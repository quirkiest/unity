9 6 40 2 ENG DES
001 ENG  DUNS
001 DES  DUNS
002 ENG  Name
002 DES  Name
003 ENG  Subsidiary
003 DES  Subsidiary
004 ENG  BusLine
004 DES  Business Line
005 ENG  Street
005 DES  Street
006 ENG  Town
006 DES  Town
007 ENG  State
007 DES  State
008 ENG  PostCode
008 DES  Post Code
009 ENG  CountryID
009 DES  Country ID
010 ENG  Phone
010 DES  Phone
011 ENG  StatusCode
011 DES  Status code
012 ENG  StatusDate
012 DES  Status date
013 ENG  ChangeDate
013 DES  Change date
014 ENG  SIC1
014 DES  SIC 1
015 ENG  SIC2
015 DES  SIC 2
016 ENG  SIC3
016 DES  SIC 3
017 ENG  SIC4
017 DES  SIC 4
018 ENG  SIC5
018 DES  SIC 5
019 ENG  PrincipalType
019 DES  Principal Type
020 ENG  PrincipalName
020 DES  Principal Name
021 ENG  Rating
021 DES  Rating
022 ENG  YearStarted
022 DES  Year Started
023 ENG  ControlYear
023 DES  Control Year
024 ENG  Import
024 DES  Import
025 ENG  Export
025 DES  Export
026 ENG  Employs
026 DES  Number of employees
027 ENG  ReportType
027 DES  Report Type
028 ENG  ReportDate
028 DES  Report Date
029 ENG  ACN
029 DES  ACN
030 ENG  History
030 DES  History
031 ENG  RegCharge
031 DES  Registered Charges Found
032 ENG  RiskScore
032 DES  Risk Score
033 ENG  IndustryNorm
033 DES  Industry Norm
034 ENG  DRSLow12Month
034 DES  DRS Low 12 Month
035 ENG  DRSHigh12Month
035 DES  DRS High 12 Mont
036 ENG  DRSAvr12Month
036 DES  DRS Average 12 Month
037 ENG  DRSIndAvr12Month
037 DES  DRS Industry Average 12 Month
038 ENG  DRSProbability
038 DES  DRS Probability
039 ENG  DDSScore
039 DES  DDS Score
040 ENG  DDSIndustryNorm
040 DES  DDS Industry Norm
041 ENG  DelinqPayProb
041 DES  DDS score - subject
042 ENG  DelinqPayProbInd
042 DES  DDS score - industry average
043 ENG  CompanyAge
043 DES  Company Age
044 ENG  ColCnt
044 DES  Total collections items
045 ENG  DirDeb
045 DES  Director debentures exist
046 ENG  CrdPet
046 DES  Creditor petition indicator
047 ENG  SIC2D
047 DES  SIC 2D
048 ENG  TotNumTrdExp
048 DES  Tot.number of trade experiences
049 ENG  PerPrmTrdExp
049 DES  Percentage trade exp within 30 days
050 ENG  Trd6090
050 DES  Trade Experiences count 60/90 days
051 ENG  Trd90120
051 DES  Trade experiences count 90/120 days
052 ENG  TrdGT120
052 DES  Trade greater than 120
053 ENG  FinStmtData.Findate
053 DES  Financial Statement date
054 ENG  FinStmtData.CurrAss
054 DES  Current assets
055 ENG  FinStmtData.CurrLiabil
055 DES  Current liabilities
056 ENG  FinStmtData.WorkCaps
056 DES  Working capital
057 ENG  FinStmtData.OthTanAss
057 DES  Other tangible assets
058 ENG  FinStmtData.OthLiabil
058 DES  Other liabilities
059 ENG  FinStmtData.NetWorth
059 DES  Net worth
060 ENG  FinStmtData.AnnSales
060 DES  Annual Sales
061 ENG  FinStmtData.Profit
061 DES  Profit
062 ENG  FinStmtData.Stock
062 DES  Stock
063 ENG  FinStmtData.Cash
063 DES  Cash
064 ENG  FinStmtData.AccRec
064 DES  Accounts receivable
065 ENG  FinStmtData.FixAss
065 DES  Fixed assets
066 ENG  FinStmtData.AccPay
066 DES  Accounts payable
067 ENG  FinProfitBATax
067 DES  Profit Before/After tax
068 ENG  FinUse1
068 DES  Financial comparative indicator 1
069 ENG  FinUse2
069 DES  Financial comparative indicator 2
070 ENG  FinUse3
070 DES  Financial comparative indicator 3
071 ENG  FinUse0
071 DES  Financial comparative indicator 0
072 ENG  FinSICGrp
072 DES  SIC Group
073 ENG  FinSizeInd
073 DES  Comparative size
074 ENG  NumPymEntries
074 DES  Number of payment entries
075 ENG  PymData.PymDate
075 DES  Payment Date
076 ENG  PymData.PayRec
076 DES  Payment Record
077 ENG  PymData.HghCrdt
077 DES  High credit
078 ENG  PymData.NowOwes
078 DES  Now Owes
079 ENG  PymData.PstDue
079 DES  Past Due
080 ENG  PymData.Terms
080 DES  Terms
081 ENG  PymData.LstSale
081 DES  Last Sale
082 ENG  PymData.IDSource
082 DES  ID Source
083 ENG  PymData.IDCode
083 DES  ID Code
084 ENG  Intangibles
084 DES  Intangibles
085 ENG  ProvisionsCur
085 DES  Current provisions
086 ENG  ProvisionsNonCur
086 DES  Non current provisions
087 ENG  ShareCapital
087 DES  Share Capital
088 ENG  Reserves
088 DES  Reserves
089 ENG  RetainedProfit
089 DES  Retained Profit
090 ENG  TotalSharehold
090 DES  Total Sharehold
091 ENG  Trd30Items
091 DES  Number of trade exp 30 days
092 ENG  Trd30AccumAmt
092 DES  Accumulated value of trade 30days
093 ENG  Trd45Items
093 DES  Number of trade exp 45 days
094 ENG  Trd45AccumAmt
094 DES  Accumulated value of trade 45 days
095 ENG  Trd60Items
095 DES  Number of trade exp 60 days
096 ENG  Trd60AccumAmt
096 DES  Accumulated value of trade 60 days
097 ENG  Trd90Items
097 DES  Number of trade exp 90 days
098 ENG  Trd90AccumAmt
098 DES  Accumulated value of trade 90 days
099 ENG  Trd120Items
099 DES  Number of trade exp 120 days
100 ENG  Trd120AccumAmt
100 DES  Accumulated value of trade 120 days
101 ENG  Trd180Items
101 DES  Number of trade exp 180 days
102 ENG  Trd180AccumAmt
102 DES  Accumulated value of trade 180 days
103 ENG  Trd999Items
103 DES  Number of trade exp 999 days
104 ENG  Trd999AccumAmt
104 DES  Accumulated value of trade 999 days
105 ENG  NumCrtAction
105 DES  Number of Court Actions
106 ENG  NumDebentures
106 DES  Number of Debentures
107 ENG  TotAmtCrtAction
107 DES  Total Amount of Court Actions
108 ENG  TotAmtCollection
108 DES  Total Amount of Collections
109 ENG  AssDeb
109 DES  Associated debenture(s) indicator
110 ENG  NumDirectors
110 DES  Number of Directors
111 ENG  NumDirCrtAction
111 DES  Number of director court actions
112 ENG  TotAmtDirCrtAct
112 DES  Total value of director court actions
113 ENG  NumFBus
113 DES  Number of Failed businesses
114 ENG  BalSheetStmtType
114 DES  Balance sheet statement type
115 ENG  BalSheet000Ind
115 DES  Balance sheet indicator 000's/millions
116 ENG  LegalStructure
116 DES  Legal Structure
117 ENG  AvgHighCredit
117 DES  Average High Credit
118 ENG  BalTotalAssets
118 DES  Balance Total Assets
119 ENG  BalTotalLiabil
119 DES  Balance Total Liability
120 ENG  BalPaidUpCapital
120 DES  Balance Paid Up Capital
121 ENG  MSCCurrencyCode
121 DES  Currency Code
122 ENG  AFLTSName
122 DES  Trading style
123 ENG  AFLImdParDuns
123 DES  Immediate Parent DUNS
124 ENG  AFLImdParName
124 DES  Immediate Parent Name
125 ENG  AFLImdParCntry
125 DES  Immediate Parent Country
126 ENG  AFLUltParDuns
126 DES  Ultimate Parent DUNS
127 ENG  AFLUltParName
127 DES  Ultimate Parent Name
128 ENG  AFLUltParCntry
128 DES  Ultimate Parent Country
129 ENG  HDRPrevName
129 DES  Previous Name
130 ENG  HDRFax
130 DES  Fax
131 ENG  HDREmpInd
131 DES  Employees Indicator
132 ENG  HDRSalesHeading
132 DES  Sales Heading
133 ENG  HDRSalesInd
133 DES  Sales indicator
134 ENG  HDRAnnualSales
134 DES  Annual Sales
135 ENG  HDRAnnualNWorth
135 DES  Annual Net Worth
136 ENG  HDRNWorthInd
136 DES  Net Worth Indicator
137 ENG  MASEmail
137 DES  Email
138 ENG  MASWebSite
138 DES  Web Site
139 ENG  MASPremises
139 DES  Premises
140 ENG  MASOfficeSize
140 DES  Office Size
141 ENG  MASDividends1
141 DES  Dividends 1
142 ENG  MASDividends2
142 DES  Dividends 2
143 ENG  MASDividends3
143 DES  Dividends 3
144 ENG  MASDlyTrxBank1
144 DES  Daily transactional bank 1
145 ENG  MASDlyTrxBank2
145 DES  Daily transactional bank 2
146 ENG  MASFinBank1
146 DES  Financing Bank 1
147 ENG  MASFinBank2
147 DES  Financing Bank 2
148 ENG  MASOSBank1
148 DES  Overseas Trading Bank 1
149 ENG  MASOSBank2
149 DES  Overseas Trading Bank 2
150 ENG  MASOtherBank1
150 DES  Other Bank 1
151 ENG  MASOtherBank2
151 DES  Other Bank 2
152 ENG  SCRArgmSch
152 DES  Scheme of arrangement - subject
153 ENG  SCRExtAdmin
153 DES  External administrator
154 ENG  SCRLiquid
154 DES  Liquidity
155 ENG  SCRMortgage
155 DES  Mortgage
156 ENG  SCRParOff
156 DES  Scheme of arrangement - parent
157 ENG  SCRRecvMgr
157 DES  Recvr/Manager appointed
158 ENG  SCRStatusCD
158 DES  Status CD
159 ENG  SCRDRSNormProb
159 DES  DRS Industry Norm Probabilty
160 ENG  SCRDRSNormRiskLvl
160 DES  DRS Norm Risk Level
161 ENG  SCRDRSRiskLvl
161 DES  DRS Risk Level
162 ENG  SCRDRSHistMth
162 DES  DRS History - subject monthly score
163 ENG  SCRDRSNormHistMth
163 DES  DRS History - industry monthly score
164 ENG  SCRDDSNormRiskLvl
164 DES  DDS Normal Risk Level
165 ENG  SCRDDSRiskLvl
165 DES  DDS Risk Level
166 ENG  SCRDDSHistMth
166 DES  DDS History - subject monthly score
167 ENG  SCRDDSNormHistMth
167 DES  DDS History - industry monthly score
168 ENG  PymHighestCredit
168 DES  Trade Payments - highest credit value
169 ENG  AnsKey
169 DES  Current answer key (answerability)
170 ENG  InDateStatus
170 DES  In-date Status
171 ENG  ACLNameStartDate
171 DES  Name Start Date
172 ENG  ACLStatusDesc
172 DES  Status Description
173 ENG  ACLTypeDesc
173 DES  Type Description
174 ENG  ACLClassDesc
174 DES  Class Description
175 ENG  ACLSubClassDesc
175 DES  Sub Class Description
176 ENG  HDRABN
176 DES  ABN
177 ENG  EntityType
177 DES  Entity Type
