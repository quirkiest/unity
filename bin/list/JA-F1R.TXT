6     6     80  2  ENG  JAP
J1    EN    favourable domestic economic conditions
J1    JA    favourable domestic economic conditions
J2    EN    increased demand
J2    JA    increased demand
J3    EN    large order(s) received
J3    JA    large order(s) received
J4    EN    increased production capacity
J4    JA    increased production capacity
J5    EN    expanding public investment
J5    JA    expanding public investment
J6    EN    active exports
J6    JA    active exports
J7    EN    new customers obtained
J7    JA    new customers obtained
J8    EN    contribution of new merchandise range
J8    JA    contribution of new merchandise range
J9    EN    contribution of new product(s)
J9    JA    contribution of new product(s)
J10   EN    increase in demand for facility investment
J10   JA    increase in demand for facility investment
J11   EN    favourable operation of parent company and/or group companies
J11   JA    favourable operation of parent company and/or group companies
J12   EN    new selling rights obtained
J12   JA    new selling rights obtained
J13   EN    economic recession in Japan
J13   JA    economic recession in Japan
J14   EN    *
J14   JA    *
J15   EN    severe competition from domestic competitors
J15   JA    severe competition from domestic competitors
J16   EN    dull demand from customers
J16   JA    dull demand from customers
J17   EN    change in accounting methods
J17   JA    change in accounting methods
J18   EN    slump in sales of main product line
J18   JA    slump in sales of main product line
J19   EN    severe competition from overseas competitors
J19   JA    severe competition from overseas competitors
