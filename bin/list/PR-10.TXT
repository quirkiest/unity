6       6       64      4       ENG     ESP     POR    COD
1       EN      Settlement
1       SP      Acuerdo
1       PO      Acuerdo
1       CO      1-Acuerdo
2       EN      Citation
2       SP      Citaci　
2       PO      Citaci　
2       CO      2-Citaci　
3       EN      Agreement
3       SP      Convenimiento
3       PO      Convenimiento
3       CO      3-Convenimiento
4       EN      Waived
4       SP      Desistimiento
4       PO      Desistimiento
4       CO      4-Desistimiento
5       EN      Embargo
5       SP      Embargo
5       PO      Embargo
5       CO      5-Embargo
6       EN      Preventive Embargo
6       SP      Embargo Preventivo
6       PO      Embargo Preventivo
6       CO      6-Embargo Preventivo
7       EN      Temporary embargo
7       SP      Embargo Provisional
7       PO      Embargo Provisional
7       CO      7-Embargo Provisional
8       EN      At the same date for
8       SP      En la misma fecha por
8       PO      En la misma fecha por
8       CO      8-En la misma fecha por
9       EN      Assembly revocation of one member against the company
9       SP      Nulidad de la Asamblea de un Socio contra la Empresa
9       PO      Nulidad de la Asamblea de un Socio contra la Empresa
9       CO      9-Nulidad de la Asamblea de un Socio contra la Empresa
10      EN      Prohibited to alienate and tax
10      SP      Prohibici　 de Enajenar y Gravar
10      PO      Prohibici　 de Enajenar y Gravar
10      CO      10-Prohibici　 de Enajenar y Gravar
11      EN      Resolution at the same date for
11      SP      Resoluci　 en la misma fecha por
11      PO      Resoluci　 en la misma fecha por
11      CO      11-Resoluci　 en la misma fecha por
12      EN      Confiscated vehicle
12      SP      Secuestro de Veh　ulo
12      PO      Secuestro de Veh　ulo
12      CO      12-Secuestro de Veh　ulo
13      EN      Soliciting an embargo
13      SP      Solicitud de Embargo
13      PO      Solicitud de Embargo
13      CO      13-Solicitud de Embargo
14      EN      Sequestration
14      SP      Secuestro
14      PO      Secuestro
14      CO      14-Secuestro
