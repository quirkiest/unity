6       6       32      2       ENG    CHI
4       EN      an industrial area
4       CH      工业区
5       EN      a commercial area
5       CH      商业区
6       EN      a residential area
6       CH      住宅区
8       EN      a rural area
8       CH      郊区
r1      EN      a prime commercial area
r1      CH      主要商业区
r2      EN      a residential area
r2      CH      住宅区
r3      EN      a residential/commercial area
r3      CH      住宅商业混合区
r4      EN      a residential/industrial area
r4      CH      住宅工业混合区
r5      EN      a free trade zone
r5      CH      保税区
r6      EN      a development area
r6      CH      开发区
