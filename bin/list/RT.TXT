6       6       64      2       ENG     CHI
OOB     EN      Out of Business
OOB     CH      歇业
FOB     EN      Favorable OB
FOB     CH      自愿停业
UOB     EN      Unfavorable OB
UOB     CH      非自愿停业
UTL     EN      Unable to Locate
UTL     CH      无法查询
NCI     EN      Non Commercial Inquiry
NCI     CH      非商业查询
RMS     EN      RMS Record
RMS     CH      RMS Record
BHR     EN      Branch Report
BHR     CH      分公司
UTC     EN      Unable To Confirm
UTC     CH      无法确认