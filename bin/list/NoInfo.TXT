9        6      50  3  ENG  CHI JAP
1        EN     Reg. Cap & Shareholders
1        CH     注册资本和股权结构
1        JA     登録資本金と資本構成
2        EN     Inv. mode and paid-up rate
2        CH     投资方式和资本到位率
2        JA     出資方式と払込率
3        EN     Inv. mode
3        CH     股东投资方式
3        JA     出資方式
4        EN     Paid-up Rate
4        CH     到位率
4        JA     払込率
5        EN     shareholding structure
5        CH     股东及股权
5        JA     株主構成及び出資比率
6        EN     shares allotment N/A
6        CH     股权不详
6        JA     出資比率は不詳です
7        EN     nationality N/A
7        CH     国别不详
7        JA     国別は不詳です
8        EN     shareholders'official English names
8        CH     股东正式英文名称
8        JA     出資者の登録英文社名
9        EN     shareholder's English name
9        CH     股东的英文名称
9        JA     出資者の英文社名
10       EN     headquarter's English name
10       CH     总部英文名称
10       JA     本店の英文社名
11       EN     Supervisor N/A
11       CH     上级主管
11       JA     上級管理機関 

