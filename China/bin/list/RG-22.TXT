6       6       64      3       ENG     ESP     POR
1       EN      together
1       SP      conjuntamente
1       PO      conjuntamente
2       EN      together or separate
2       SP      conjunta o separadamente
2       PO      conjunta o separadamente
3       EN      indistinctly
3       SP      indistintamente
3       PO      indistintamente
4       EN      who represent the firm in all dealings
4       SP      quien con su firma obliga y representa a la entidad
4       PO      quien con su firma obliga y representa a la entidad
5       EN      who represent the firm in all dealings
5       SP      quienes con su firmas obligan y representan a la entidad
5       PO      quienes con su firmas obligan y representan a la entidad
