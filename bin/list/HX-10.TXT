9        6      100  2  ENG   CHI
0        EN     The trade reference are not available throught our direct or indirect investigations. 
0        CH     已正侧面调查，未获得该主体供应商评价
1        EN     No trade reference is made due to short transaction histroy
1        CH     合作时间较短，没有对该主体做其他评价
2        EN     Excellent credit status and no delay in payments is reported
2        CH     信用状况极佳，从未有拖欠发生
3        EN     Good payment status and only sporadic delays are reported
3        CH     付款状况很好，只有个别特殊情况产生付款拖延
4        EN     Fair payment status and a number of delays is reported
4        CH     付款状况较好，出现付款拖延情况较少
5        EN     Modest payment status and a quite large number delays is reported
5        CH     付款状况一般，有时会出现付款拖延情况
6        EN     Rather poor payment status and overdue is frequently reported
6        CH     付款状况较差，经常出现拖延付款
7        EN     Very poor payment status and a large number of overdues is reported
7        CH     付款状况极差，大量出现拖延状况，且态度恶劣
8        EN     Extremely bad payment status and bad debts are reported
8        CH     付款状况最差，已产生坏帐