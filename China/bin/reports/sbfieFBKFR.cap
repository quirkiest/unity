//*********************************************************************************
//  sbeFBKFR.cap - SuperBIR Eng. HTML Finance & Banking - Key Financial Ratios
//*********************************************************************************
DBSECTION "finstmtb";
LANGUAGE "ENG";
FLOAT_FORMAT "1000.00";
LINE_LENGTH 1200;
NOTFILL;
DECLARE RESULT;
DECLARE NUMLOOPS;

DECLARE MAX_SECTIONS;       MAX_SECTIONS         := 3;
DECLARE FINANCE_KFR_EXISTS; FINANCE_KFR_EXISTS   := 0;

DECLARE TOTAL_MC;       TOTAL_MC    := 0;
DECLARE MC;             MC          := -1;
DECLARE PAD_MCOUNT;     PAD_MCOUNT  := 0;

DECLARE HASDATA;


// Check Finance Key Financial Ratios
// -------------------------------------
FINANCE_KFR_EXISTS := (ATOI([:@FIN_FH_MCOUNT:L0])>=0);

LEFT_MARGIN 9;

IF(FINANCE_KFR_EXISTS)
{
    // Key Financial Ratios
    // ----------------------

    TOTAL_MC :=  0;

    // Get TOTAL Section Count
    // -----------------------
    MC := 0;
    WHILE(MC < MAX_SECTIONS)
    {
        LCOUNT := MC;
        IF(ATOI([:@FIN_FH_MCOUNT:L?])>=0) { INC TOTAL_MC; } ELSE { MC := MAX_SECTIONS; }
        INC MC;
    }
    MC := -1;

    PAD_MCOUNT := MAX_SECTIONS - TOTAL_MC;

    // Key Financial Ratios
    // -----------------------
    `
    <H4>Key Financial Ratios</H4>
    <TABLE class="dbSummaryA" cellSpacing="0" cellPadding="0" width="630" bgColor="#dbe5f3" border="0">
    <TR>
        <TD align="middle" height="10"><SPAN class="diagramTableSpace"><IMG height="1" src="[:@WEBSERVER]images/spacer.gif" width="1"></SPAN></TD>
    </TR>
    <TR>
        <TD align="middle">
            <TABLE class="chartTable3" cellSpacing="0" cellPadding="2" width="580" border="0">
            `
            //<TR>
            //    <TD width="4" bgColor="#acbfeb">&nbsp;</TD>
            //    <TD align="left" bgColor="#acbfeb">&nbsp;</TD>
            //    `
            //
            //MC := 0;
            //WHILE(MC < MAX_SECTIONS)
            //{
            //    `<TD align="right" width="55" bgColor="#acbfeb">&nbsp;</TD>
            //    `
            //    INC MC;
            //}
            //`
            //    <TD width="4" bgColor="#acbfeb">&nbsp;</TD>
            //</TR>
            `
            <TR>
                <TD class="dbRowElabel" width="4" bgColor="#acbfeb">&nbsp;</TD>
                <TD class="dbRowElabel" align="left" bgColor="#acbfeb"><B>Ratio</B><BR></TD>
                `

            MC := 0;
            WHILE(MC < PAD_MCOUNT)
            {
                `<TD class="dbRowElabel" align="right" width="55" bgColor="#acbfeb">&nbsp;</TD>
                `
                INC MC;
            }
            MC := 0;
            WHILE(MC < TOTAL_MC)
            {
                LCOUNT:=MC; MCOUNT:=ATOI([:@FIN_FH_MCOUNT:L?]);
                `<TD class="dbRowElabel" align="right" width="55" bgColor="#acbfeb"><B>[:ib:M?:B*:FDATEYYYY]</B></TD>
                `
                INC MC;
            }
            `
                <TD class="dbRowElabel" width="4" bgColor="#acbfeb">&nbsp;</TD>
            </TR>
            `

            // Sales Growth %
            // ----------------
            HASDATA := 0; MC := 0;
            WHILE(MC < TOTAL_MC)
            {
                LCOUNT:=MC; MCOUNT:=ATOI([:@FIN_FH_MCOUNT:L?]); RESULT := RUNFORM([:@AMOUNT:L?: = ""]);
                RESULT := RUNFORM([:@AMT: = :ti:M?]);
                IF(EXISTS([:@AMT:FSTRIP])) { HASDATA := 1; RESULT := RUNFORM([:@AMOUNT:L?: = ":@AMT:"]);}
                INC MC;
            }
            IF(HASDATA)
            {
                `
                <TR>
                    <TD class="dbRowAlabel" width="4">&nbsp;</TD>
                    <TD class="dbRowAlabel" vAlign="bottom">Sales Growth %</TD>`

                // Right-Justify column data
                // --------------------------
                MC := 0; WHILE(MC < PAD_MCOUNT) { FILL; `<TD class="dbRowAresults" vAlign="bottom" align="right">&nbsp;</TD>` INC MC; }

                MC := 0;
                WHILE(MC < TOTAL_MC)
                {
                    `
                    <TD class="dbRowAresults" vAlign="bottom" align="right">`

                    LCOUNT:=MC; RESULT := RUNFORM([:@AMT: = ":@AMOUNT:L?:"]);
                    IF     (FLOATCMP([:@AMT],"0.0")== 0) {`-`}
                    ELSE IF(STRCMP([:@AMT:S1],"-")==0)
                    {
                        RESULT := RUNFORM([:@TEMP: = ":@AMT:FREMOVE-"]);
                        `([:@TEMP:FFLOAT])`
                    }
                    ELSE {`[:@AMT:FFLOAT]`}
                    `</TD>`

                    INC MC;
                }
                `
                    <TD class="dbRowAresults" width="4">&nbsp;</TD>
                </TR>
                `
            }

            // Net Profit Growth %
            // --------------------
            HASDATA := 0; MC := 0;
            WHILE(MC < TOTAL_MC)
            {
                LCOUNT:=MC; MCOUNT:=ATOI([:@FIN_FH_MCOUNT:L?]); RESULT := RUNFORM([:@AMOUNT:L?: = ""]);
                RESULT := RUNFORM([:@AMT: = :c6:M?]);
                IF(EXISTS([:@AMT:FSTRIP])) { HASDATA := 1; RESULT := RUNFORM([:@AMOUNT:L?: = ":@AMT:"]);}
                INC MC;
            }
            IF(HASDATA)
            {
                `
                <TR>
                    <TD class="dbRowAlabel" width="4">&nbsp;</TD>
                    <TD class="dbRowAlabel" vAlign="bottom">Net Profit Growth %</TD>`

                // Right-Justify column data
                // --------------------------
                MC := 0; WHILE(MC < PAD_MCOUNT) { FILL; `<TD class="dbRowAresults" vAlign="bottom" align="right">&nbsp;</TD>` INC MC; }

                MC := 0;
                WHILE(MC < TOTAL_MC)
                {
                    `
                    <TD class="dbRowAresults" vAlign="bottom" align="right">`

                    LCOUNT:=MC; RESULT := RUNFORM([:@AMT: = ":@AMOUNT:L?:"]);
                    IF     (FLOATCMP([:@AMT],"0.0")== 0) {`-`}
                    ELSE IF(STRCMP([:@AMT:S1],"-")==0)
                    {
                        RESULT := RUNFORM([:@TEMP: = ":@AMT:FREMOVE-"]);
                        `([:@TEMP:FFLOAT])`
                    }
                    ELSE {`[:@AMT:FFLOAT]`}
                    `</TD>`

                    INC MC;
                }
                `
                    <TD class="dbRowAresults" width="4">&nbsp;</TD>
                </TR>
                `
            }

            // Current Ratio
            // --------------------
            HASDATA := 0; MC := 0;
            WHILE(MC < TOTAL_MC)
            {
                LCOUNT:=MC; MCOUNT:=ATOI([:@FIN_FH_MCOUNT:L?]); RESULT := RUNFORM([:@AMOUNT:L?: = ""]);
                RESULT := RUNFORM([:@AMT: = ""]);
                IF(EXISTS([:we:M?]))
                {
                    RESULT := RUNFORM([:@AMT: = :we:M?]);
                }
                ELSE IF((ATOI([:f7:M?])!=0) + (ATOI([:cg:M?])!=0) == 2)
                {
                    RESULT := RUNFORM([:@AMT: = :pa:M?]);
                }
                IF(EXISTS([:@AMT:FSTRIP])) { HASDATA := 1; RESULT := RUNFORM([:@AMOUNT:L?: = ":@AMT:"]);}
                INC MC;
            }
            IF(HASDATA)
            {
                `
                <TR>
                    <TD class="dbRowAlabel" width="4">&nbsp;</TD>
                    <TD class="dbRowAlabel" vAlign="bottom">Current Ratio</TD>`

                // Right-Justify column data
                // --------------------------
                MC := 0; WHILE(MC < PAD_MCOUNT) { FILL; `<TD class="dbRowAresults" vAlign="bottom" align="right">&nbsp;</TD>` INC MC; }

                MC := 0;
                WHILE(MC < TOTAL_MC)
                {
                    `
                    <TD class="dbRowAresults" vAlign="bottom" align="right">`

                    LCOUNT:=MC; RESULT := RUNFORM([:@AMT: = ":@AMOUNT:L?:"]);
                    IF     (FLOATCMP([:@AMT],"0.0")== 0) {`-`}
                    ELSE IF(STRCMP([:@AMT:S1],"-")==0)
                    {
                        RESULT := RUNFORM([:@TEMP: = ":@AMT:FREMOVE-"]);
                        `([:@TEMP:FFLOAT])`
                    }
                    ELSE {`[:@AMT:FFLOAT]`}
                    `</TD>`

                    INC MC;
                }
                `
                    <TD class="dbRowAresults" width="4">&nbsp;</TD>
                </TR>
                `
            }

            // Quick Ratio
            // --------------------
            HASDATA := 0; MC := 0;
            WHILE(MC < TOTAL_MC)
            {
                LCOUNT:=MC; MCOUNT:=ATOI([:@FIN_FH_MCOUNT:L?]); RESULT := RUNFORM([:@AMOUNT:L?: = ""]);
                RESULT := RUNFORM([:@AMT: = ""]);
                IF(EXISTS([:wf:M?]))
                {
                    RESULT := RUNFORM([:@AMT: = :wf:M?]);
                }
                ELSE IF((ATOI([:f7:M?])!=0) + ((ATOI([:cg:M?])!=0)+(ATOI([:b5:M?])!=0) > 0) == 2)
                {
                    RESULT := RUNFORM([:@AMT: = :pb:M?]);
                }
                IF(EXISTS([:@AMT:FSTRIP])) { HASDATA := 1; RESULT := RUNFORM([:@AMOUNT:L?: = ":@AMT:"]);}
                INC MC;
            }
            IF(HASDATA)
            {
                `
                <TR>
                    <TD class="dbRowAlabel" width="4">&nbsp;</TD>
                    <TD class="dbRowAlabel" vAlign="bottom">Quick Ratio</TD>`

                // Right-Justify column data
                // --------------------------
                MC := 0; WHILE(MC < PAD_MCOUNT) { FILL; `<TD class="dbRowAresults" vAlign="bottom" align="right">&nbsp;</TD>` INC MC; }

                MC := 0;
                WHILE(MC < TOTAL_MC)
                {
                    `
                    <TD class="dbRowAresults" vAlign="bottom" align="right">`

                    LCOUNT:=MC; RESULT := RUNFORM([:@AMT: = ":@AMOUNT:L?:"]);
                    IF     (FLOATCMP([:@AMT],"0.0")== 0) {`-`}
                    ELSE IF(STRCMP([:@AMT:S1],"-")==0)
                    {
                        RESULT := RUNFORM([:@TEMP: = ":@AMT:FREMOVE-"]);
                        `([:@TEMP:FFLOAT])`
                    }
                    ELSE {`[:@AMT:FFLOAT]`}
                    `</TD>`

                    INC MC;
                }
                `
                    <TD class="dbRowAresults" width="4">&nbsp;</TD>
                </TR>
                `
            }

            // Total Debt/Equity Ratio
            // ------------------------
            HASDATA := 0; MC := 0;
            WHILE(MC < TOTAL_MC)
            {
                LCOUNT:=MC; MCOUNT:=ATOI([:@FIN_FH_MCOUNT:L?]); RESULT := RUNFORM([:@AMOUNT:L?: = ""]);
                RESULT := RUNFORM([:@AMT: = ""]);
                IF(STRCMP([:nt:M?:B*],"1") != 0)
                {
                    IF(EXISTS([:wk:M?]))
                    {
                        RESULT := RUNFORM([:@AMT: = :wk:M?]);
                    }
                    ELSE IF((ATOI([:h4:M?])!=0) + (ATOI([:gx:M?])!=0) == 2)
                    {
                        RESULT := RUNFORM([:@AMT: = :jx:M?]);
                    }
                }
                IF(EXISTS([:@AMT:FSTRIP])) { HASDATA := 1; RESULT := RUNFORM([:@AMOUNT:L?: = ":@AMT:"]);}
                INC MC;
            }
            IF(HASDATA)
            {
                `
                <TR>
                    <TD class="dbRowAlabel" width="4">&nbsp;</TD>
                    <TD class="dbRowAlabel" vAlign="bottom">Total Debt/Equity Ratio</TD>`

                // Right-Justify column data
                // --------------------------
                MC := 0; WHILE(MC < PAD_MCOUNT) { FILL; `<TD class="dbRowAresults" vAlign="bottom" align="right">&nbsp;</TD>` INC MC; }

                MC := 0;
                WHILE(MC < TOTAL_MC)
                {
                    `
                    <TD class="dbRowAresults" vAlign="bottom" align="right">`

                    LCOUNT:=MC; RESULT := RUNFORM([:@AMT: = ":@AMOUNT:L?:"]);
                    IF     (FLOATCMP([:@AMT],"0.0")== 0) {`-`}
                    ELSE IF(STRCMP([:@AMT:S1],"-")==0)
                    {
                        RESULT := RUNFORM([:@TEMP: = ":@AMT:FREMOVE-"]);
                        `([:@TEMP:FFLOAT])`
                    }
                    ELSE {`[:@AMT:FFLOAT]`}
                    `</TD>`

                    INC MC;
                }
                `
                    <TD class="dbRowAresults" width="4">&nbsp;</TD>
                </TR>
                `
            }

            // Debt Ratio
            // ------------------------
            HASDATA := 0; MC := 0;
            WHILE(MC < TOTAL_MC)
            {
                LCOUNT:=MC; MCOUNT:=ATOI([:@FIN_FH_MCOUNT:L?]); RESULT := RUNFORM([:@AMOUNT:L?: = ""]);
                RESULT := RUNFORM([:@AMT: = ""]);
                IF(EXISTS([:wc:M?]))
                {
                    RESULT := RUNFORM([:@AMT: = :wc:M?]);
                }
                ELSE IF((ATOI([:ec:M?])!=0) + (ATOI([:gx:M?])!=0) == 2)
                {
                    RESULT := RUNFORM([:@AMT: = :tb:M?]);
                }
                IF(EXISTS([:@AMT:FSTRIP])) { HASDATA := 1; RESULT := RUNFORM([:@AMOUNT:L?: = ":@AMT:"]);}
                INC MC;
            }
            IF(HASDATA)
            {
                `
                <TR>
                    <TD class="dbRowAlabel" width="4">&nbsp;</TD>
                    <TD class="dbRowAlabel" vAlign="bottom">Debt Ratio</TD>`

                // Right-Justify column data
                // --------------------------
                MC := 0; WHILE(MC < PAD_MCOUNT) { FILL; `<TD class="dbRowAresults" vAlign="bottom" align="right">&nbsp;</TD>` INC MC; }

                MC := 0;
                WHILE(MC < TOTAL_MC)
                {
                    `
                    <TD class="dbRowAresults" vAlign="bottom" align="right">`

                    LCOUNT:=MC; RESULT := RUNFORM([:@AMT: = ":@AMOUNT:L?:"]);
                    IF     (FLOATCMP([:@AMT],"0.0")== 0) {`-`}
                    ELSE IF(STRCMP([:@AMT:S1],"-")==0)
                    {
                        RESULT := RUNFORM([:@TEMP: = ":@AMT:FREMOVE-"]);
                        `([:@TEMP:FFLOAT])`
                    }
                    ELSE {`[:@AMT:FFLOAT]`}
                    `</TD>`

                    INC MC;
                }
                `
                    <TD class="dbRowAresults" width="4">&nbsp;</TD>
                </TR>
                `
            }

            // Collection Period (Days)
            // ------------------------
            HASDATA := 0; MC := 0;
            WHILE(MC < TOTAL_MC)
            {
                LCOUNT:=MC; MCOUNT:=ATOI([:@FIN_FH_MCOUNT:L?]); RESULT := RUNFORM([:@AMOUNT:L?: = ""]);
                RESULT := RUNFORM([:@AMT: = ""]);
                IF(EXISTS([:wh:M?]))
                {
                    RESULT := RUNFORM([:@AMT: = :wh:M?]);
                }
                ELSE IF((ATOI([:t2:M?])!=0) + (ATOI([:br:M?])!=0) == 2)
                {
                  IF(STRCMP([::finstmtb:ib:M?:E0:B*],"12")==0)
                    {RESULT := RUNFORM([:@AMT: = :tf:M?]);}
                    ELSE IF(STRCMP([::finstmtb:ib:M?:E0:B*],"3")==0)
                    {RESULT := RUNFORM([:@AMTTEMP: = :tf:M?]);
                     RESULT := RUNFORM([:@AMT: = :@AMTTEMP*0.25]);}
                    ELSE IF(STRCMP([::finstmtb:ib:M?:E0:B*],"6")==0)
                    {RESULT := RUNFORM([:@AMTTEMP: = :tf:M?]);
                     RESULT := RUNFORM([:@AMT: = :@AMTTEMP*0.5]);}
                    ELSE IF(STRCMP([::finstmtb:ib:M?:E0:B*],"9")==0)
                    {RESULT := RUNFORM([:@AMTTEMP: = :tf:M?]);
                     RESULT := RUNFORM([:@AMT: = :@AMTTEMP*0.75]);}
                }
                IF(EXISTS([:@AMT:FSTRIP])) { HASDATA := 1; RESULT := RUNFORM([:@AMOUNT:L?: = ":@AMT:"]);}
                INC MC;
            }
            IF(HASDATA)
            {
                `
                <TR>
                    <TD class="dbRowAlabel" width="4">&nbsp;</TD>
                    <TD class="dbRowAlabel" vAlign="bottom">Collection Period (Days)</TD>`

                // Right-Justify column data
                // --------------------------
                MC := 0; WHILE(MC < PAD_MCOUNT) { FILL; `<TD class="dbRowAresults" vAlign="bottom" align="right">&nbsp;</TD>` INC MC; }

                FLOAT_FORMAT "1000";

                MC := 0;
                WHILE(MC < TOTAL_MC)
                {
                    `
                    <TD class="dbRowAresults" vAlign="bottom" align="right">`

                    LCOUNT:=MC; RESULT := RUNFORM([:@AMT: = ":@AMOUNT:L?:"]);
                    IF     (FLOATCMP([:@AMT],"0.0")== 0) {`-`}
                    ELSE IF(STRCMP([:@AMT:S1],"-")==0)
                    {
                        RESULT := RUNFORM([:@TEMP: = ":@AMT:FREMOVE-"]);
                        `([:@TEMP:FFLOAT])`
                    }
                    ELSE {`[:@AMT:FFLOAT]`}
                    `</TD>`

                    INC MC;
                }
                FLOAT_FORMAT "1000.00";

                `
                    <TD class="dbRowAresults" width="4">&nbsp;</TD>
                </TR>
                `
            }

            // Payment Period (Days)
            // ------------------------
            HASDATA := 0; MC := 0;
            WHILE(MC < TOTAL_MC)
            {
                LCOUNT:=MC; MCOUNT:=ATOI([:@FIN_FH_MCOUNT:L?]); RESULT := RUNFORM([:@AMOUNT:L?: = ""]);
                RESULT := RUNFORM([:@AMT: = ""]);
                IF(ATOI([:pg:M?])!=0)
                {
                   IF(STRCMP([::finstmtb:ib:M?:E0:B*],"12")==0)
                    {RESULT := RUNFORM([:@AMT: = :pg:M?]);}
                    ELSE IF(STRCMP([::finstmtb:ib:M?:E0:B*],"3")==0)
                    {RESULT := RUNFORM([:@AMTTEMP: = :pg:M?]);
                     RESULT := RUNFORM([:@AMT: = :@AMTTEMP*0.25]);}
                    ELSE IF(STRCMP([::finstmtb:ib:M?:E0:B*],"6")==0)
                    {RESULT := RUNFORM([:@AMTTEMP: = :pg:M?]);
                     RESULT := RUNFORM([:@AMT: = :@AMTTEMP*0.5]);}
                    ELSE IF(STRCMP([::finstmtb:ib:M?:E0:B*],"9")==0)
                    {RESULT := RUNFORM([:@AMTTEMP: = :pg:M?]);
                     RESULT := RUNFORM([:@AMT: = :@AMTTEMP*0.75]);} 
                }
                ELSE IF(((ATOI([:is:M?])!=0)+(ATOI([:ct:M?])!=0)==2) + (ATOI([:fg:M?])!=0) == 2)
                {
                    RESULT := RUNFORM([:@AMT: = :wm:M?]);
                }
                IF(EXISTS([:@AMT:FSTRIP])) { HASDATA := 1; RESULT := RUNFORM([:@AMOUNT:L?: = ":@AMT:"]);}
                INC MC;
            }
            IF(HASDATA)
            {
                `
                <TR>
                    <TD class="dbRowAlabel" width="4">&nbsp;</TD>
                    <TD class="dbRowAlabel" vAlign="bottom">Payment Period (Days)</TD>`

                // Right-Justify column data
                // --------------------------
                MC := 0; WHILE(MC < PAD_MCOUNT) { FILL; `<TD class="dbRowAresults" vAlign="bottom" align="right">&nbsp;</TD>` INC MC; }

                FLOAT_FORMAT "1000";

                MC := 0;
                WHILE(MC < TOTAL_MC)
                {
                    `
                    <TD class="dbRowAresults" vAlign="bottom" align="right">`

                    LCOUNT:=MC; RESULT := RUNFORM([:@AMT: = ":@AMOUNT:L?:"]);
                    IF     (FLOATCMP([:@AMT],"0.0")== 0) {`-`}
                    ELSE IF(STRCMP([:@AMT:S1],"-")==0)
                    {
                        RESULT := RUNFORM([:@TEMP: = ":@AMT:FREMOVE-"]);
                        `([:@TEMP:FFLOAT])`
                    }
                    ELSE {`[:@AMT:FFLOAT]`}
                    `</TD>`

                    INC MC;
                }
                FLOAT_FORMAT "1000.00";

                `
                    <TD class="dbRowAresults" width="4">&nbsp;</TD>
                </TR>
                `
            }

            // Inventory Turnover (Days)
            // ----------------------------
            HASDATA := 0; MC := 0;
            WHILE(MC < TOTAL_MC)
            {
                LCOUNT:=MC; MCOUNT:=ATOI([:@FIN_FH_MCOUNT:L?]); RESULT := RUNFORM([:@AMOUNT:L?: = ""]);
                RESULT := RUNFORM([:@AMT: = ""]);
                IF(ATOI([:tg:M?])!=0)
                {
                   IF(STRCMP([::finstmtb:ib:M?:E0:B*],"12")==0)
                    {RESULT := RUNFORM([:@AMT: = :tg:M?]);}
                    ELSE IF(STRCMP([::finstmtb:ib:M?:E0:B*],"3")==0)
                    {RESULT := RUNFORM([:@AMTTEMP: = :tg:M?]);
                     RESULT := RUNFORM([:@AMT: = :@AMTTEMP*0.25]);}
                    ELSE IF(STRCMP([::finstmtb:ib:M?:E0:B*],"6")==0)
                    {RESULT := RUNFORM([:@AMTTEMP: = :tg:M?]);
                     RESULT := RUNFORM([:@AMT: = :@AMTTEMP*0.5]);}
                    ELSE IF(STRCMP([::finstmtb:ib:M?:E0:B*],"9")==0)
                    {RESULT := RUNFORM([:@AMTTEMP: = :tg:M?]);
                     RESULT := RUNFORM([:@AMT: = :@AMTTEMP*0.75]);}
                }
                IF(EXISTS([:@AMT:FSTRIP])) { HASDATA := 1; RESULT := RUNFORM([:@AMOUNT:L?: = ":@AMT:"]);}
                INC MC;
            }
            IF(HASDATA)
            {
                `
                <TR>
                    <TD class="dbRowAlabel" width="4">&nbsp;</TD>
                    <TD class="dbRowAlabel" vAlign="bottom">Inventory Turnover (Days)</TD>`

                // Right-Justify column data
                // --------------------------
                MC := 0; WHILE(MC < PAD_MCOUNT) { FILL; `<TD class="dbRowAresults" vAlign="bottom" align="right">&nbsp;</TD>` INC MC; }

                FLOAT_FORMAT "1000";

                MC := 0;
                WHILE(MC < TOTAL_MC)
                {
                    `
                    <TD class="dbRowAresults" vAlign="bottom" align="right">`

                    LCOUNT:=MC; RESULT := RUNFORM([:@AMT: = ":@AMOUNT:L?:"]);
                    IF     (FLOATCMP([:@AMT],"0.0")== 0) {`-`}
                    ELSE IF(STRCMP([:@AMT:S1],"-")==0)
                    {
                        RESULT := RUNFORM([:@TEMP: = ":@AMT:FREMOVE-"]);
                        `([:@TEMP:FFLOAT])`
                    }
                    ELSE {`[:@AMT:FFLOAT]`}
                    `</TD>`

                    INC MC;
                }
                FLOAT_FORMAT "1000.00";

                `
                    <TD class="dbRowAresults" width="4">&nbsp;</TD>
                </TR>
                `
            }

            // Asset Turnover
            // ----------------------------
            HASDATA := 0; MC := 0;
            WHILE(MC < TOTAL_MC)
            {
                LCOUNT:=MC; MCOUNT:=ATOI([:@FIN_FH_MCOUNT:L?]); RESULT := RUNFORM([:@AMOUNT:L?: = ""]);
                RESULT := RUNFORM([:@AMT: = ""]);
                IF(EXISTS([:AT:M?]))
                {
                    RESULT := RUNFORM([:@AMT: = :AT:M?]);
                }
                ELSE IF((ATOI([:ec:M?])!=0)+(ATOI([:t2:M?])!=0)==2)
                {
                    RESULT := RUNFORM([:@AMT: = :at:M?]);
                }
                IF(EXISTS([:@AMT:FSTRIP])) { HASDATA := 1; RESULT := RUNFORM([:@AMOUNT:L?: = ":@AMT:"]);}
                INC MC;
            }
            IF(HASDATA)
            {
                `
                <TR>
                    <TD class="dbRowAlabel" width="4">&nbsp;</TD>
                    <TD class="dbRowAlabel" vAlign="bottom">Asset Turnover</TD>`

                // Right-Justify column data
                // --------------------------
                MC := 0; WHILE(MC < PAD_MCOUNT) { FILL; `<TD class="dbRowAresults" vAlign="bottom" align="right">&nbsp;</TD>` INC MC; }

                MC := 0;
                WHILE(MC < TOTAL_MC)
                {
                    `
                    <TD class="dbRowAresults" vAlign="bottom" align="right">`

                    LCOUNT:=MC; RESULT := RUNFORM([:@AMT: = ":@AMOUNT:L?:"]);
                    IF     (FLOATCMP([:@AMT],"0.0")== 0) {`-`}
                    ELSE IF(STRCMP([:@AMT:S1],"-")==0)
                    {
                        RESULT := RUNFORM([:@TEMP: = ":@AMT:FREMOVE-"]);
                        `([:@TEMP:FFLOAT])`
                    }
                    ELSE {`[:@AMT:FFLOAT]`}
                    `</TD>`

                    INC MC;
                }
                `
                    <TD class="dbRowAresults" width="4">&nbsp;</TD>
                </TR>
                `
            }

           

            // Net Profit Margin %
            // ----------------------------
            HASDATA := 0; MC := 0;
            WHILE(MC < TOTAL_MC)
            {
                LCOUNT:=MC; MCOUNT:=ATOI([:@FIN_FH_MCOUNT:L?]); RESULT := RUNFORM([:@AMOUNT:L?: = ""]);
                RESULT := RUNFORM([:@AMT: = ""]);
                IF(EXISTS([:wg:M?]))
                {
                    RESULT := RUNFORM([:@AMT: = :AT:M?]);
                }
                ELSE IF((ATOI([:t2:M?])!=0)+(ATOI([:jj:M?])!=0)==2)
                {
                    RESULT := RUNFORM([:@AMT: = :pd:M?]);
                }
                IF(EXISTS([:@AMT:FSTRIP])) { HASDATA := 1; RESULT := RUNFORM([:@AMOUNT:L?: = ":@AMT:"]);}
                INC MC;
            }
            IF(HASDATA)
            {
                `
                <TR>
                    <TD class="dbRowAlabel" width="4">&nbsp;</TD>
                    <TD class="dbRowAlabel" vAlign="bottom">Net Profit Margin %</TD>`

                // Right-Justify column data
                // --------------------------
                MC := 0; WHILE(MC < PAD_MCOUNT) { FILL; `<TD class="dbRowAresults" vAlign="bottom" align="right">&nbsp;</TD>` INC MC; }

                MC := 0;
                WHILE(MC < TOTAL_MC)
                {
                    `
                    <TD class="dbRowAresults" vAlign="bottom" align="right">`

                    LCOUNT:=MC; RESULT := RUNFORM([:@AMT: = ":@AMOUNT:L?:"]);
                    IF     (FLOATCMP([:@AMT],"0.0")== 0) {`-`}
                    ELSE IF(STRCMP([:@AMT:S1],"-")==0)
                    {
                        RESULT := RUNFORM([:@TEMP: = ":@AMT:FREMOVE-"]);
                        `([:@TEMP:FFLOAT])`
                    }
                    ELSE {`[:@AMT:FFLOAT]`}
                    `</TD>`

                    INC MC;
                }
                `
                    <TD class="dbRowAresults" width="4">&nbsp;</TD>
                </TR>
                `
            }

            // Return on Equity %
            // ----------------------------
            HASDATA := 0; MC := 0;
            WHILE(MC < TOTAL_MC)
            {
                LCOUNT:=MC; MCOUNT:=ATOI([:@FIN_FH_MCOUNT:L?]); RESULT := RUNFORM([:@AMOUNT:L?: = ""]);
                RESULT := RUNFORM([:@AMT: = ""]);

                IF(STRCMP([:nt:M?:B*],"1") != 0)
                {
                    IF(EXISTS([:wd:M?]))
                    {
                        RESULT := RUNFORM([:@AMT: = :wd:M?]);
                    }
                    ELSE IF((ATOI([:dd:M?])!=0) + (ATOI([:h4:M?])!=0) == 2)
                    {
                        RESULT := RUNFORM([:@AMT: = :de:M?]);
                    }
                }
                IF(EXISTS([:@AMT:FSTRIP])) { HASDATA := 1; RESULT := RUNFORM([:@AMOUNT:L?: = ":@AMT:"]);}
                INC MC;
            }
            IF(HASDATA)
            {
                `
                <TR>
                    <TD class="dbRowAlabel" width="4">&nbsp;</TD>
                    <TD class="dbRowAlabel" vAlign="bottom">Return on Equity %</TD>`

                // Right-Justify column data
                // --------------------------
                MC := 0; WHILE(MC < PAD_MCOUNT) { FILL; `<TD class="dbRowAresults" vAlign="bottom" align="right">&nbsp;</TD>` INC MC; }

                MC := 0;
                WHILE(MC < TOTAL_MC)
                {
                    `
                    <TD class="dbRowAresults" vAlign="bottom" align="right">`

                    LCOUNT:=MC; RESULT := RUNFORM([:@AMT: = ":@AMOUNT:L?:"]);
                    IF     (FLOATCMP([:@AMT],"0.0")== 0) {`-`}
                    ELSE IF(STRCMP([:@AMT:S1],"-")==0)
                    {
                        RESULT := RUNFORM([:@TEMP: = ":@AMT:FREMOVE-"]);
                        `([:@TEMP:FFLOAT])`
                    }
                    ELSE {`[:@AMT:FFLOAT]`}
                    `</TD>`

                    INC MC;
                }
                `
                    <TD class="dbRowAresults" width="4">&nbsp;</TD>
                </TR>
                `
            }

            // Return on Assets %
            // ----------------------------
            HASDATA := 0; MC := 0;
            WHILE(MC < TOTAL_MC)
            {
                LCOUNT:=MC; MCOUNT:=ATOI([:@FIN_FH_MCOUNT:L?]); RESULT := RUNFORM([:@AMOUNT:L?: = ""]);
                RESULT := RUNFORM([:@AMT: = ""]);
                IF(EXISTS([:wb:M?]))
                {
                    RESULT := RUNFORM([:@AMT: = :wb:M?]);
                }
                ELSE IF((ATOI([:ec:M?])!=0) + (ATOI([:jj:M?])!=0) == 2)
                {
                    RESULT := RUNFORM([:@AMT: = :tp:M?]);
                }
                IF(EXISTS([:@AMT:FSTRIP])) { HASDATA := 1; RESULT := RUNFORM([:@AMOUNT:L?: = ":@AMT:"]);}
                INC MC;
            }
            IF(HASDATA)
            {
                `
                <TR>
                    <TD class="dbRowAlabel" width="4">&nbsp;</TD>
                    <TD class="dbRowAlabel" vAlign="bottom">Return on Assets %</TD>`

                // Right-Justify column data
                // --------------------------
                MC := 0; WHILE(MC < PAD_MCOUNT) { FILL; `<TD class="dbRowAresults" vAlign="bottom" align="right">&nbsp;</TD>` INC MC; }

                MC := 0;
                WHILE(MC < TOTAL_MC)
                {
                    `
                    <TD class="dbRowAresults" vAlign="bottom" align="right">`

                    LCOUNT:=MC; RESULT := RUNFORM([:@AMT: = ":@AMOUNT:L?:"]);
                    IF     (FLOATCMP([:@AMT],"0.0")== 0) {`-`}
                    ELSE IF(STRCMP([:@AMT:S1],"-")==0)
                    {
                        RESULT := RUNFORM([:@TEMP: = ":@AMT:FREMOVE-"]);
                        `([:@TEMP:FFLOAT])`
                    }
                    ELSE {`[:@AMT:FFLOAT]`}
                    `</TD>`

                    INC MC;
                }
                `
                    <TD class="dbRowAresults" width="4">&nbsp;</TD>
                </TR>
                `
            }
            `
            </TABLE>
        </TD>
    </TR>
    <TR>
        <TD height="10"><IMG height="1" src="[:@WEBSERVER]images/spacer.gif" width="1"></TD>
    </TR>
    </TABLE>
    `
}

