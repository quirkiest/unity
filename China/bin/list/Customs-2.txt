9     6    60  3  ENG  CHI JAP
AA    EN   Enterprise with outstanding credit
AA    CH   信用突出企业
AA    JA   信用が非常に良い企業
A     EN   Enterprise with good credit
A     CH   信用良好企业
A     JA   信用が良好な企業
B     EN   Enterprise with average credit
B     CH   信用一般企业
B     JA   信用が普通な企業
C     EN   Enterprise with poor credit
C     CH   信用较差企业
C     JA   信用がやや不良な企業
D     EN   Enterprise with rather poor credit
D     CH   信用很差企业
D     JA   信用が非常に不良な企業


