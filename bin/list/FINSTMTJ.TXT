6 6 32 1 ENG
name  1 Financial Statement
scr1  1 Financial Statement
scr2  1 Results
scr3  1 Other Measures
scr4  1 Current Term Expectation
scr5  1 Key Ratios & Trend 1
scr6  1 Key Ratios & Trend 2
scr7  1 Condition
scr8  1 English Comment
scr9  1 Japanese Comment
scr10 1 Balance Sheet
scr11 1 Current Assets 1
scr12 1 Current Assets 2
scr13 1 Current Assets 3
scr14 1 Fixed Assets
scr15 1 Intangible Assets
scr16 1 Investment
scr17 1 Other Assets
scr18 1 Current Liabilities 1
scr19 1 Current Liabilities 2
scr20 1 Current Liabilities 3
scr21 1 Non Current Liabilities 1
scr22 1 Non Current Liabilities 2
scr23 1 Non Current Liabilities 3
scr24 1 Equity 1
scr25 1 Equity 2
scr26 1 Equity 3
scr27 1 Profit & Loss 1
scr28 1 Profit & Loss 2
scr29 1 Profit & Loss 3
scr30 1 Profit & Loss 4
scr31 1 Profit & Loss 5
scr32 1 Appropriations-1
scr33 1 Appropriations-2
scr34 1 Auditor
ac    1 Update Date
ai    1 Millions/Thousands
ad    1 Lack of financial info...
ae    1 Due to recent inception...
af    1 Lack of exec. info...
b2    1 Inventory - type
b4    1 Amount
b5    1 Total Inventory
b7    1 Prepayments - type
b9    1 Amount
bc    1 Cash & Bank - type
be    1 Amount
bf    1 Total Cash & Bank
bh    1 Investments - type
bk    1 Amount
bl    1 Total Investments
bp    1 Accounts Receivable - type
br    1 Amount
bu    1 Total Accounts Receivable
ca    1 Total Prepayments
cc    1 Other Current Assets - type
ce    1 Amount
cf    1 Total Other Current Assets
cg    1 TOTAL CURRENT ASSETS
d2    1 Type
d4    1 Amount
d5    1 Total Other Assets
d6    1 Estimated Total Other Assets
d7    1 Type
d9    1 Amount
dn    1 Fixed Assets - type
dp    1 Amount
dr    1 Accumulated Depreciation
du    1 Total Fixed Assets
dw    1 Investment - type
dy    1 Amount
dz    1 Total Investment
ea    1 Total Intangibles
xx    1 Estimated Total Intangibles
eb    1 TOTAL NON CURRENT ASSETS
ec    1 TOTAL ASSETS
f3    1 Other Current Liabilities -
f5    1 Amount
f6    1 Total Other Current Liab
f7    1 TOTAL CURRENT LIABILITIES
fd    1 Trade Creditors - type
ff    1 Amount
fg    1 Total Current Liabilities
fi    1 Other Creditors - type
fk    1 Amount
fl    1 Total Other Creditors
fn    1 Bonds & Debentures - type
fp    1 Amount
fq    1 Total Bonds & Debentures
fs    1 Taxes - type
fu    1 Amount
fv    1 Total Taxes
gd    1 Creditors & Borrowings - type
gf    1 Amount
gg    1 Total Creditors & Borrowings
gn    1 Other Non-Current Liabilities
gp    1 Amount
gq    1 Total Other Liabilities
gs    1 Bonds & Debentures - type
gu    1 Amount
gv    1 Total Bonds & Debentures
gw    1 TOTAL NON CURRENT LIABILITIES
gx    1 TOTAL LIABILITIES
h1    1 TOTAL EQUITY
h2    1 Total Liabilities & Equity
hc    1 Capital - type
he    1 Amount
hf    1 Total Capital
hi    1 Reserves - type
hk    1 Amount
hl    1 Total Reserves
ho    1 Surplus - type
hq    1 Amount
hr    1 Total Surplus
ht    1 Other Equity - type
hv    1 Amount
hw    1 Total Other Equity
i2    1 Non Operating Income - type
i4    1 Amount
i5    1 Tot Non Op Income/Expenses
i6    1 ORDINARY PROFIT
i8    1 Taxes - type
ia    1 Statement Date
ib    1 Year End date
ie    1 Sales - type
ig    1 Amount
ih    1 Total Sales
ip    1 COGS - type
ir    1 Amount
is    1 Total COGS
it    1 GROSS PROFIT ON SALES
iv    1 Operating Income - type
ix    1 Amount
iy    1 Total Income/Expenses
iz    1 OPERATING PROFIT/LOSS
ja    1 Amount
jb    1 Total Taxes
jc    1 PROFIT/LOSS BEFORE TAX
jf    1 Special Income - type
jh    1 Amount
ji    1 Total Special Income/Expenses
jj    1 NET PROFIT/LOSS
jk    1 Contingent Liabilities
jl    1 Comment
JL    1 Comment (J)
jm    1 Comment
JM    1 Comment (J)
jn    1 Condition
jo    1 Reason
jp    1 Reason (Text)
JP    1 Reason (Text-j)
jr    1 Address 1
JR    1 Address 1 (J)
js    1 Address 2
JS    1 Address 2 (J)
jt    1 Audited/prepared?
ju    1 Prefecture
jv    1 Telephone
jw    1 Fax
jx    1 Net Worth
jy    1 Working Capital
kb    1 Profit Result
kc    1 Reason
kd    1 Reason (Text)
KD    1 Reason (Text-j)
ke    1 Title
kf    1 Reason (Text)
KF    1 Reason (Text-j)
kg    1 Contact Name
KG    1 Contact Name (J)
kh    1 Dept/Title (J)
kp    1 Auditor Name
KP    1 Auditor Name (J)
la    1 Statement Type
lb    1 Period Ending
lc    1 Unapp. Ret'd Earnings - type
ld    1 Amount
le    1 Total Unapp. Ret'd Earnings
lf    1 Vol Earned Surplus Rev - type
lg    1 Amount
lh    1 Total V.E.S.R.
li    1 TOTAL RET'D EARNINGS FOR APP.
lj    1 Appropriations - type
lk    1 Amount
ll    1 Total Appropriations
lm    1 Vol Earned Surplus - type
ln    1 Amount
lo    1 Total Vol Earned Surplus
lp    1 Brought Forward - type
lq    1 TOTAL AMOUNT
lr    1 Comment
ls    1 Comment
LR    1 Comment (J)
LS    1 Comment (J)
ma    1 Operating Expenses - type
mb    1 Amount
mc    1 Non-Operating Expenses-type
md    1 Amount
me    1 Special Expenses - type
mf    1 Amount
na    1 Manual Amount
nb    1 Manual Amount
nc    1 Manual Amount
nd    1 Manual Amount
ne    1 Manual Amount
nf    1 Manual Amount
ng    1 Manual Amount
nh    1 Manual Amount
ni    1 Manual Amount
nj    1 Manual Amount
nk    1 Manual Amount
nl    1 Manual Amount
nm    1 Manual Amount
nn    1 Manual Amount
no    1 Manual Amount
np    1 Manual Amount
nq    1 Manual Amount
pa    1 Current Ratio
pb    1 Quick Ratio
pc    1 Debt Ratio
pd    1 Net Profit/Sales
pe    1 Cost of Goods Sold
pf    1 Invested Capital Ratio (%)
pg    1 Trade Payable Turnover
pw    1 Balance Carried Forward
py    1 Dividends
pz    1 Unappropriated Profit
r1    1 Amount
r2    1 Trend
r4    1 Comment
R4    1 Comment (J)
r5    1 Discounted Notes
r6    1 Endorsed Notes
r8    1 Major Current Assets
r9    1 %
ra    1 Interview Date
rb    1 Unsuccessful Contact?
rc    1 Declined/Submitted
rd    1 Type of Information
rf    1 # Companies consolidated
rg    1 Investigation provided...
rh    1 Statement Date
ri    1 Comment
RI    1 Comment (J)
rj    1 Period To
rm    1 Updated from Mainframe?
rn    1 Statement Source
ro    1 Total
rp    1 Total
rq    1 Total
rr    1 Total
ru    1 Sales Result
rx    1 Reason
ry    1 Comment
RY    1 Comment (J)
rz    1 Projection Type
s1    1 Comment
s2    1 Comment
s3    1 Comment
s4    1 Comment
S1    1 Comment (J)
S2    1 Comment (J)
S3    1 Comment (J)
S4    1 Comment (J)
sc    1 Reductions
sd    1 Amount
se    1 Total Reductions
sk    1 TOTAL INVESTMENT & OTHER ASSETS
sn    1 Other Liabilities
so    1 Amount
sp    1 Total Other Liabilities
sz    1 Industry Type
tb    1 Total Asset Turnover
tf    1 Receivable Turnover
tg    1 Collection Period
ti    1 Sales Growth rate
to    1 Net Worth/Total Assets
tp    1 Ordinary Profit to Sales
tq    1 Inventory Turnover
tr    1 Financial Analysis (Eng.)
TR    1 Financial Analysis (Jap.)
vi    1 Reason (Text)
VI    1 Reason (Text-j)
vj    1 Trend
vk    1 Reason
vm    1 Trend
vn    1 Reason
vv    1 Reason (Text)
VV    1 Reason (Text-j)
vw    1 Overall Trend
vx    1 Other measure
vy    1 Description
vz    1 Market size trend
wa    1 -
wb    1 -
wc    1 -
wd    1 -
we    1 -
wf    1 -
wg    1 -
wh    1 -
wi    1 -
wj    1 -
wk    1 -
wl    1 -
wm    1 -
wx    1 Statement?
wy    1 PL?
xa    1 Mainframe
xb    1 Amount
xc    1 Total mainframe
xd    1 Mainframe
xe    1 Amount
xf    1 Total mainframe
xg    1 Mainframe
xh    1 Amount
xi    1 Total mainframe
xj    1 Mainframe
xk    1 Amount
xl    1 Total mainframe
xm    1 Mainframe
xn    1 Amount
xo    1 Total mainframe
xp    1 Mainframe
xq    1 Amount
xr    1 Total mainframe
xs    1 Mainframe
xt    1 Amount
xu    1 Total mainframe
xv    1 Period From
xw    1 Period To
y1    1 Financial Analysis (Eng.)
Y1    1 Financial Analysis (Jap.)
y2    1 Financial Analysis (Eng.)
Y2    1 Financial Analysis (Jap.)
y3    1 Financial Analysis (Eng.)
Y3    1 Financial Analysis (Jap.)
y4    1 Financial Analysis (Eng.)
Y4    1 Financial Analysis (Jap.)
y5    1 Financial Analysis (Eng.)
Y5    1 Financial Analysis (Jap.)
ya    1 Financial Analysis (Eng.)
YA    1 Financial Analysis (Jap.)
yb    1 Financial Analysis (Eng.)
YB    1 Financial Analysis (Jap.)
yc    1 Financial Analysis (Eng.)
YC    1 Financial Analysis (Jap.)
yd    1 Financial Analysis (Eng.)
YD    1 Financial Analysis (Jap.)
ye    1 Financial Analysis (Eng.)
YE    1 Financial Analysis (Jap.)
yf    1 Financial Analysis (Eng.)
YF    1 Financial Analysis (Jap.)
yg    1 Financial Analysis (Eng.)
YG    1 Financial Analysis (Jap.)
yh    1 Financial Analysis (Eng.)
YH    1 Financial Analysis (Jap.)
yi    1 Financial Analysis (Eng.)
YI    1 Financial Analysis (Jap.)
yj    1 Financial Analysis (Eng.)
YJ    1 Financial Analysis (Jap.)
yk    1 Financial Analysis (Eng.)
YK    1 Financial Analysis (Jap.)
yl    1 Financial Analysis (Eng.)
YL    1 Financial Analysis (Jap.)
ym    1 Financial Analysis (Eng.)
YM    1 Financial Analysis (Jap.)
yn    1 Financial Analysis (Eng.)
YN    1 Financial Analysis (Jap.)
yo    1 Financial Analysis (Eng.)
YO    1 Financial Analysis (Jap.)
yp    1 Financial Analysis (Eng.)
YP    1 Financial Analysis (Jap.)
yq    1 Financial Analysis (Eng.)
YQ    1 Financial Analysis (Jap.)
yr    1 Financial Analysis (Eng.)
YR    1 Financial Analysis (Jap.)
ys    1 Financial Analysis (Eng.)
YS    1 Financial Analysis (Jap.)
yt    1 Financial Analysis (Eng.)
YT    1 Financial Analysis (Jap.)
yu    1 Financial Analysis (Eng.)
YU    1 Financial Analysis (Jap.)
yv    1 Financial Analysis (Eng.)
YV    1 Financial Analysis (Jap.)
yw    1 Financial Analysis (Eng.)
YW    1 Financial Analysis (Jap.)
yx    1 Financial Analysis (Eng.)
YX    1 Financial Analysis (Jap.)
yy    1 Financial Analysis (Eng.)
YY    1 Financial Analysis (Jap.)
yz    1 Financial Analysis (Eng.)
YZ    1 Financial Analysis (Jap.)
