9   6  120  4   SCR  ENG  CHI JAP

0   SCR   N/A
0   ENG   Assessment not available due to insufficient data
0   CHI   没有足够数据导致无法提供评估
0   JAP   情報不足の為、リスク評価を付与することができない」ことを表しています 

1   SCR   1
1   ENG   Much Lower Risk
1   CHN   风险非常低
1   JAP   リスクが非常に低い

2   SCR   2
2   ENG   Low Risk
2   CHN   低风险
2   JAP   低リスク

3   SCR   3
3   ENG   Moderate Low Risk 
3   CHN   较低风险
3   JAP   比較的低いリスク

4   SCR   4
4   ENG   Below Average Risk 
4   CHN   低于平均风险
4   JAP   平均リスクより低い

5   SCR   5
5   ENG   Slight Below Average Risk
5   CHN   略低于平均风险
5   JAP   平均リスクよりやや低い

6   SCR   6
6   ENG   Average Risk 
6   CHN   平均风险
6   JAP   平均リスク

7   SCR   7
7   ENG   Higher Than Average Risk
7   CHN   高于平均风险
7   JAP   平均リスクより高い

8   SCR   8
8   ENG   Moderate High Risk 
8   CHN   较高风险
8   JAP   比較的高いリスク

9   SCR   9
9   ENG   High Risk
9   CHN   高风险
9   JAP   高リスク

10  SCR   10
10  ENG   Much Higher Risk 
10  CHN   风险非常高
10  JAP   リスクが非常に高い

