6       6       40      3       ENG     ESP     POR
A.      EN      Argentine Austral
A.      SP      Austral Argentino
A.      PO      Austral Argentino
ADH     EN      Durham
ADH     SP      Durham
ADH     PO      Durham
AFG     EN      Afghanistan Afghani
AFG     SP      Afghanistan Afghani
AFG     PO      Afghanistan Afghani
NAF     EN      Netherlands Antilles Guilder
NAF     SP      Netherlands Antilles Guilder
NAF     PO      Netherlands Antilles Guilder
AFR     EN      CFA Franc
AFR     SP      CFA Franc
AFR     PO      CFA Franc
AKZ     EN      Kwanza
AKZ     SP      Kwanza
AKZ     PO      Kwanza
ALD     EN      Algerian Dinar
ALD     SP      Algerian Dinar
ALD     PO      Algerian Dinar
AF      EN      Aruba Guilder
AF      SP      Guilder de Aruba
AF      PO      Guilder de Aruba
ARI     EN      Saudi Riyal
ARI     SP      Saudi Riyal
ARI     PO      Saudi Riyal
AUD     EN      Australian Dollar
AUD     SP      D�lar Australiano
AUD     PO      D�lar Australiano
AUS     EN      Austrian Schilling
AUS     SP      Austrian Schilling
AUS     PO      Austrian Schilling
B/.     EN      Balboa
B/.     SP      Balboa
B/.     PO      Balboa
Bds.D.  EN      Barbados Dollar
Bds.D.  SP      D�lar de Barbados
Bds.D.  PO      D�lar de Barbados
BDT     EN      Bangladesh Taka
BDT     SP      Bangladesh Taka
BDT     PO      Bangladesh Taka
Ber.D.  EN      Bermuda Dollar
Ber.D.  SP      D�lar de Bermuda
Ber.D.  PO      D�lar de Bermuda
BFR     EN      Belium Franc
BFR     SP      Belium Franc
BFR     PO      Belium Franc
BHD     EN      Bahrain Dinar
BHD     SP      Bahrain Dinar
BHD     PO      Bahrain Dinar
BHT     EN      Baht
BHT     SP      Baht
BHT     PO      Baht
BIR     EN      Ethiopia BIR
BIR     SP      Ethiopia BIR
BIR     PO      Ethiopia BIR
Bah.D.  EN      Bahamian Dollar
Bah.D.  SP      D�lar de Bahamas
Bah.D.  PO      D�lar de Bahamas
Bze.D.  EN      Belize Dollar
Bze.D.  SP      D�lar de Belice
Bze.D.  PO      D�lar de Belice
Bol.    EN      Boliviano
Bol.    SP      Boliviano
Bol.    PO      Boliviano
BRD     EN      Brunei Dollar
BRD     SP      Brunei Dollar
BRD     PO      Brunei Dollar
Bs.     EN      Bs.
Bs.     SP      Bs.
Bs.     PO      Bs.
BTP     EN      Botswana Pula
BTP     SP      Botswana Pula
BTP     PO      Botswana Pula
BUR     EN      Burmese Kyat
BUR     SP      Burmese Kyat
BUR     PO      Burmese Kyat
CAD     EN      Canadian Dollar
CAD     SP      D�lar Canadiense
CAD     PO      D�lar Canadiense
CER     EN      Sri Lanka Rupee
CER     SP      Sri Lanka Rupee
CER     PO      Sri Lanka Rupee
CHP     EN      Chilean Peso
CHP     SP      Peso Chileno
CHP     PO      Peso Chileno
CID     EN      Cayman Island Dollar
CID     SP      D�lar de Islas caimanes
CID     PO      D�lar de Islas caimanes
CKR     EN      Czechoslovkia Kruna
CKR     SP      Czechoslovkia Kruna
CKR     PO      Czechoslovkia Kruna
P.      EN      Colombian Peso
P.      SP      Peso Colombiano
P.      PO      Peso Colombiano
C.      EN      Nicaragua Cordoba
C.      SP      C�rdoba Nicarag�ense
C.      PO      C�rdoba Nicarag�ense
C.R.C.  EN      Costa Rica Colon
C.R.C.  SP      Col�n de Costa Rica
C.R.C.  PO      Col�n de Costa Rica
CUP     EN      Cuban Peso
CUP     SP      Peso Cubano
CUP     PO      Peso Cubano
CVE     EN      Cape Verde Escudo
CVE     SP      Cape Verde Escudo
CVE     PO      Cape Verde Escudo
CYL     EN      Cyprus Pound
CYL     SP      Cyprus Pound
CYL     PO      Cyprus Pound
DFL     EN      Dutch Guilder
DFL     SP      Dutch Guilder
DFL     PO      Dutch Guilder
DFR     EN      Djibouti Franc
DFR     SP      Djibouti Franc
DFR     PO      Djibouti Franc
DKR     EN      Danish Krone
DKR     SP      Danish Krone
DKR     PO      Danish Krone
DMK     EN      Deutsch Mark
DMK     SP      Deutsch Mark
DMK     PO      Deutsch Mark
R.D.P.  EN      Dominican Republic Peso
R.D.P.  SP      Peso Dominicano
R.D.P.  PO      Peso Dominicano
DRA     EN      Drachma
DRA     SP      Drachma
DRA     PO      Drachma
DYD     EN      Yemen Dinar
DYD     SP      Yemen Dinar
DYD     PO      Yemen Dinar
E.C.D.  EN      Eastern Caribbean Dollar
E.C.D.  SP      Eastern Caribbean Dollar
E.C.D.  PO      Eastern Caribbean Dollar
EGL     EN      Egyptian Pound
EGL     SP      Egyptian Pound
EGL     PO      Egyptian Pound
ESP     EN      Portuguese Escudo
ESP     SP      Escudos Portugueses
ESP     PO      Escudos Portugueses
FFR     EN      French Franc
FFR     SP      French Franc
FFR     PO      French Franc
FID     EN      Fiji Dollar
FID     SP      Fiji Dollar
FID     PO      Fiji Dollar
FMG     EN      Madagascar Franc
FMG     SP      Madagascar Franc
FMG     PO      Madagascar Franc
FMK     EN      Finland Markka
FMK     SP      Finland Markka
FMK     PO      Finland Markka
FOR     EN      Forint
FOR     SP      Forint
FOR     PO      Forint
FRB     EN      Burundi Franc
FRB     SP      Burundi Franc
FRB     PO      Burundi Franc
FRR     EN      Rwanda Franc
FRR     SP      Rwanda Franc
FRR     PO      Rwanda Franc
GAD     EN      Dalasi
GAD     SP      Dalasi
GAD     PO      Dalasi
GBL     EN      Gibralter Pund
GBL     SP      Gibralter Pund
GBL     PO      Gibralter Pund
GHC     EN      Ceci
GHC     SP      Ceci
GHC     PO      Ceci
Gdes.   EN      Gourde
Gdes.   SP      Gourde
Gdes.   PO      Gourde
GQE     EN      Ekuele
GQE     SP      Ekuele
GQE     PO      Ekuele
GSI     EN      Sily
GSI     SP      Sily
GSI     PO      Sily
G.      EN      Guarani
G.      SP      Guarani
G.      PO      Guarani
Q.      EN      Quetzal
Q.      SP      Quetzal
Q.      PO      Quetzal
GWE     EN      Guinea-Bissau Peso
GWE     SP      Guinea-Bissau Peso
GWE     PO      Guinea-Bissau Peso
G.D.    EN      Guyana Dollar
G.D.    SP      Guyana D�lar
G.D.    PO      Guyana D�lar
HKD     EN      Hong Kong Dollar
HKD     SP      Hong Kong Dollar
HKD     PO      Hong Kong Dollar
IKR     EN      Icelandic Krona
IKR     SP      Icelandic Krona
IKR     PO      Icelandic Krona
ILS     EN      Sheqel
ILS     SP      Sheqel
ILS     PO      Sheqel
INR     EN      Indian Rupee
INR     SP      Indian Rupee
INR     PO      Indian Rupee
IRD     EN      Iraq Dinar
IRD     SP      Iraq Dinar
IRD     PO      Iraq Dinar
IRI     EN      Iranian Rial
IRI     SP      Iranian Rial
IRI     PO      Iranian Rial
IRL     EN      Irish Pound
IRL     SP      Irish Pound
IRL     PO      Irish Pound
J.D.    EN      Jamaican Dollar
J.D.    SP      D�lar Jamaiquino
J.D.    PO      D�lar Jamaiquino
JOD     EN      Jordanian Dinar
JOD     SP      Jordanian Dinar
JOD     PO      Jordanian Dinar
JYE     EN      Japanese Yen
JYE     SP      Japanese Yen
JYE     PO      Japanese Yen
KES     EN      Kenyan Shilling
KES     SP      Kenyan Shilling
KES     PO      Kenyan Shilling
KIP     EN      KIP
KIP     SP      KIP
KIP     PO      KIP
KUD     EN      Kuwait Dinar
KUD     SP      Kuwait Dinar
KUD     PO      Kuwait Dinar
LBD     EN      Libyan Dinar
LBD     SP      Libyan Dinar
LBD     PO      Libyan Dinar
LEI     EN      Lei
LEI     SP      Lei
LEI     PO      Lei
LEL     EN      Lebanon Pound
LEL     SP      Lebanon Pound
LEL     PO      Lebanon Pound
L.      EN      Lempira
L.      SP      Lempira
L.      PO      Lempira
LEV     EN      Bulgaria Lev
LEV     SP      Bulgaria Lev
LEV     PO      Bulgaria Lev
LFR     EN      Luxembourg Franc
LFR     SP      Luxembourg Franc
LFR     PO      Luxembourg Franc
LID     EN      Liberian Dollar
LID     SP      Liberian Dollar
LID     PO      Liberian Dollar
LIT     EN      Italian Lira
LIT     SP      Lira Italiana
LIT     PO      Lira Italiana
LSM     EN      Maloti
LSM     SP      Maloti
LSM     PO      Maloti
MAL     EN      Malta Pound
MAL     SP      Malta Pound
MAL     PO      Malta Pound
MAR     EN      Mauritius Rupee
MAR     SP      Mauritius Rupee
MAR     PO      Mauritius Rupee
MDH     EN      Dirham
MDH     SP      Dirham
MDH     PO      Dirham
Ps.     EN      Mexican Peso
Ps.     SP      Peso Mejicano
Ps.     PO      Peso Mejicano
MLF     EN      Mali France
MLF     SP      Mali France
MLF     PO      Mali France
MOG     EN      Ouguiya
MOG     SP      Ouguiya
MOG     PO      Ouguiya
MWK     EN      Kwacha
MWK     SP      Kwacha
MWK     PO      Kwacha
MZM     EN      Metical
MZM     SP      Metical
MZM     PO      Metical
NER     EN      Nepalese Rupee
NER     SP      Nepalese Rupee
NER     PO      Nepalese Rupee
NGK     EN      Kina
NGK     SP      Kina
NGK     PO      Kina
NGR     EN      Naira
NGR     SP      Naira
NGR     PO      Naira
NKR     EN      Norwegian Krone
NKR     SP      Norwegian Krone
NKR     PO      Norwegian Krone
NTD     EN      Taiwanese Dollar
NTD     SP      Taiwanese Dollar
NTD     PO      Taiwanese Dollar
NUP     EN      Uruguay Peso
NUP     SP      Peso Uruguayo
NUP     PO      Peso Uruguayo
NZD     EN      New Zealand Dollar
NZD     SP      New Zealand Dollar
NZD     PO      New Zealand Dollar
PAR     EN      Pakistan Rupee
PAR     SP      Pakistan Rupee
PAR     PO      Pakistan Rupee
PFR     EN      French Polynesia Franc
PFR     SP      French Polynesia Franc
PFR     PO      French Polynesia Franc
PHP     EN      Philippines Peso
PHP     SP      Philippines Peso
PHP     PO      Philippines Peso
PTS     EN      Pesata
PTS     SP      Peseta
PTS     PO      Peseta
QRI     EN      Qatar Riyal
QRI     SP      Qatar Riyal
QRI     PO      Qatar Riyal
RGT     EN      Ringgit
RGT     SP      Ringgit
RGT     PO      Ringgit
RIO     EN      Rial Omani
RIO     SP      Rial Omani
RIO     PO      Rial Omani
RMB     EN      Chiaese Ren Min Bi
RMB     SP      Chiaese Ren Min Bi
RMB     PO      Chiaese Ren Min Bi
ROU     EN      Rouble
ROU     SP      Rouble
ROU     PO      Rouble
RPA     EN      Indonesia Rupiah
RPA     SP      Indonesia Rupiah
RPA     PO      Indonesia Rupiah
E.S.C.  EN      El Salvador Colon
E.S.C.  SP      El Salvador Colon
E.S.C.  PO      El Salvador Colon
SAR     EN      South African Rand
SAR     SP      South African Rand
SAR     PO      South African Rand
SAT     EN      Tala
SAT     SP      Tala
SAT     PO      Tala
SBD     EN      Solomon Is Dollar
SBD     SP      Solomon Is Dollar
SBD     PO      Solomon Is Dollar
SER     EN      Seychelles Rupee
SER     SP      Seychelles Rupee
SER     PO      Seychelles Rupee
Sf      EN      Suriname Guilder
Sf      SP      Suriname Guilder
Sf      PO      Suriname Guilder
SFR     EN      Swiss Franc
SFR     SP      Swiss Franc
SFR     PO      Swiss Franc
SID     EN      Singapore Dollar
SID     SP      Singapore Dollar
SID     PO      Singapore Dollar
SKR     EN      Swedish Krona
SKR     SP      Swedish Krona
SKR     PO      Swedish Krona
SLE     EN      Leone
SLE     SP      Leone
SLE     PO      Leone
L/.     EN      Sol
L/.     SP      Sol
L/.     PO      Sol
SOM     EN      Somalia Shilling
SOM     SP      Somalia Shilling
SOM     PO      Somalia Shilling
S/.     EN      Dobra
S/.     SP      Dobra
S/.     PO      Dobra
SUC     EN      Sucre
SUC     SP      Sucre
SUC     PO      Sucre
SUL     EN      Sudanese Pound
SUL     SP      Sudanese Pound
SUL     PO      Sudanese Pound
SYL     EN      Syrian Pound
SYL     SP      Syrian Pound
SYL     PO      Syrian Pound
SZL     EN      Ulangeni
SZL     SP      Ulangeni
SZL     PO      Ulangeni
TAS     EN      Tanzanian Shilling
TAS     SP      Tanzanian Shilling
TAS     PO      Tanzanian Shilling
TOP     EN      Pa,Anga
TOP     SP      Pa,Anga
TOP     PO      Pa,Anga
TTD     EN      Trinidad & Tobago Dollar
TTD     SP      D�lar de Trinidad
TTD     PO      D�lar de Trinidad
TUD     EN      Tunisian Dinar
TUD     SP      Tunisian Dinar
TUD     PO      Tunisian Dinar
TUL     EN      Turkish Lira
TUL     SP      Turkish Lira
TUL     PO      Turkish Lira
UGS     EN      Uganda Shilling
UGS     SP      Uganda Shilling
UGS     PO      Uganda Shilling
UKL     EN      British Pound
UKL     SP      British Pound
UKL     PO      British Pound
USD     EN      USD
USD     SP      USD
USD     PO      USD
VDD     EN      Dong
VDD     SP      Dong
VDD     PO      Dong
VUV     EN      Vanuatu Franc
VUV     SP      Vanuatu Franc
VUV     PO      Vanuatu Franc
WON     EN      WON
WON     SP      WON
WON     PO      WON
YEM     EN      Yemen Riyal
YEM     SP      Yemen Riyal
YEM     PO      Yemen Riyal
YUD     EN      Yugoslavia Dinar
YUD     SP      Yugoslavia Dinar
YUD     PO      Yugoslavia Dinar
ZAI     EN      Zaire
ZAI     SP      Zaire
ZAI     PO      Zaire
ZLO     EN      Zloty
ZLO     SP      Zloty
ZLO     PO      Zloty
ZMK     EN      Zambia Kwacha
ZMK     SP      Zambia Kwacha
ZMK     PO      Zambia Kwacha
ZWD     EN      Zimbabwe Dollar
ZWD     SP      Zimbabwe Dollar
ZWD     PO      Zimbabwe Dollar
AMD     EN      American Dollar
AMD     SP      American Dollar
AMD     PO      American Dollar
