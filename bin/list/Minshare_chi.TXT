9 6 32 1 CHI
name  1 Minor Shareholders
Scr1  1 1/2 Minor Shareholders
ac    1 Update Date
am    1 Controlling Shareholder?
ai    1 D-U-N-S
ad    1 Name
AD    1 Chinese Name
bd    1 Address 1
jc    1 Address 2
db    1 District
ah    1 City
BD    1 Chinese Address 1
JC    1 Chinese Address 2
DB    1 Chinese District
AH    1 Chiness City
af    1 County
ae    1 Country
wc    1 Telephone
p1    1 Post Code
ag    1 Foreign State
Scr2  1 2/2 Minor Shareholders Info
AL    1 LOB
al    1 LOB(chi)
AO    1 LOB(chi)
AP    1 LOB(chi)
bc    1 Number of Employees
au    1 % of Subject Owned
cu    1 Currency
c1    1 Capitalization
es    1 Established Date
bu    1 Biz Reg #
xa    1 Group Name (e)
XA    1 Group Name (c)
xb    1 No. of Companies 
z1    1 1/2 Minor Shareholders
z2    1 2/2 Minor Shareholders Info
re    1 Recommendation (eng)
RE    1 Recommendation (chi)
hc    1 Comments-HX(Eng)	
HC    1 Comments-HX(Chi)
fx    1 Fax
le    1 Legal Status
lc    1	Litigation Comments (eng)
LC    1 Litigation Comments(chi)
mc    1	Media Comments (eng)
MC    1	Media Comments(chi)
sc    1 Sanction Comments(eng)
SC    1 Sanction Comments(chi)