6       6       256     3       ENG     ESP     POR     JAP
1       EN      a financial statement, local sources indicate that operations are being conducted along normal lines for this industry
1       SP      un estado fianciero, fuentes locales consultadas reportaro que las peraciones son efectuadas de acuerdo a los lineamientos propios de la industria.
1       PO      un estado fianciero, fuentes locales consultadas reportaro que las peraciones son efectuadas de acuerdo a los lineamientos propios de la industria.
1       JA
2       EN      financial figures, local sources indicate that operations are being conducted along normal lines for this industry
2       SP      cifras financieras, fuentes locales consultadas reportaro que las operaciones son efectuadas de acuerdo a los lineamientos propios de la industria.
2       PO      cifras financieras, fuentes locales consultadas reportaro que las operaciones son efectuadas de acuerdo a los lineamientos propios de la industria.
2       JA
3       EN      additional financial figures, local sources indicate that operations are being conducted along normal lines for this industry
3       SP      cifras financieras adicionales, fuentes locales consultadas reportaro que las operaciones son efectuadas de acuerdo a los lineamientos propios de la  industria
3       PO      cifras financieras adicionales, fuentes locales consultadas reportaro que las operaciones son efectuadas de acuerdo a los lineamientos propios de la  industria
3       JA