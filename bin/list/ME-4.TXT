6       6       32      3       ENG     ESP     POR
1       EN      articles
1       SP      art�culos
1       PO      art�culos
2       EN      barrels
2       SP      barriles
2       PO      barriles
3       EN      gallons
3       SP      galones
3       PO      galones
4       EN      kilograms
4       SP      kilogramos
4       PO      kilogramos
5       EN      pounds
5       SP      libras
5       PO      libras
6       EN      liters
6       SP      litros
6       PO      litros
7       EN      cubic meters
7       SP      metros c�bicos
7       PO      metros c�bicos
8       EN      ounces
8       SP      onzas
8       PO      onzas
9       EN      cubic feet
9       SP      pies c�bicos
9       PO      pies c�bicos
10      EN      board feet
10      SP      piezas
10      PO      piezas
11      EN      sacks
11      SP      sacos
11      PO      sacos
12      EN      tons
12      SP      toneladas
12      PO      toneladas
13      EN      metric tons
13      SP      toneladas m�tricas
13      PO      toneladas m�tricas
14      EN      units
14      SP      unidades
14      PO      unidades
15      EN      square feet
15      SP      pies cuadrados
15      PO      pies cuadrados
16      EN      square meters
16      SP      metros cuadrados
16      PO      metros cuadrados
17      EN      meters
17      SP      metros
17      PO      metros
