6        6      64   1   ENG
1        EN     increased tangible fixed assets caused by capital investment
1	 CH	资本投资导至无形固定资产增加
2        EN     decreased assets caused by sale of tangible fixed assets
2	 CH	销售无形资产导至资产下降
3        EN     increased investment in marketable securities
3	 CH	增加对可买卖债券的投资
4        EN     decreased investments caused by sale of marketable securities
4	 CH	卖出债券导至投资降低
5        EN     increased investment in subsidiaries/affiliated companies
5	 CH	增加对子公司/相关企业的投资
6        EN     decreased investments caused by sale of the company's interest on subsidiaries/affiliated companies
6	 CH	卖出公司对子公司/相关企业的控制权导至投资降低
7        EN     increased investments caused by a newly established subsidiary/affiliate
7	 CH	新成立的子公司/相关企业导至投资增加
8        EN     increased current assets
8	 CH	增加流动资产
9        EN     decreased current assets
9	 CH	降低流动资产
10       EN     increased borrowings
10	 CH	增加借款
11       EN     decreased borrowings
11	 CH	降低借款
12       EN     issue of corporate bond
12	 CH	发行公司债
13       EN     redemption of corporate bond
13	 CH	买回公司债
14       EN     increased share capital
14	 CH	增加股本
15       EN     decreased share capital
15	 CH	减少股本
16       EN     increased retained earnings
16	 CH	增加累积盈馀
17       EN     decreased equity caused by current loss
17	 CH	亏损导至降低股本权益
18       EN     decreased equity caused by reserval of surplus
18	 CH	保留盈馀导至股东权益下降