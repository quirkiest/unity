 6  6  50   2   ENG  CHI
14   EN Manager
14   CH 经理
15   EN Chief
15   CH 总裁
82   EN Assistant Manager
82   CH 经理助理
83   EN Assistant General Manager
83   CH 助理总经理
96   EN Division Manager
96   CH 区经理
97   EN Executive Officer
97   CH 行政长官
98   EN Finance Manager
98   CH 财务经理
99   EN Financial Controller
99   CH 财务长
100  EN Finance Director
100  CH 财务董事
105  EN Marketing Manager
105  CH 行销经理
108  EN Operations Manager
108  CH 营运经理
117  EN Sales Manager
117  CH 业务经理
118  EN Country Manager
118  CH 海外分公司经理
M1   EN Administrator
M1   CH 总务
M8   EN Accountant
M8   CH 会计
M11  EN Director
M11  CH 董事
M17  EN President
M17  CH 总裁
M18  EN Proprietor
M18  CH 经营人
M26  EN Vice President
M26  CH 副总裁
M27  EN Senior Vice President
M27  CH 资深副总裁
M70  EN Executive Vice President
M70  CH 执行副总裁
M28  EN Chairman
M28  CH 董事长
M29  EN Vice Chairman
M29  CH 副董事长
M58  EN Auditor
M58  CH 审计员
M61  EN Branch Manager
M61  CH 分公司经理
M67  EN General Manager
M67  CH 总经理
M68  EN Managing Director
M68  CH 常务董事
M69  EN Owner
M69  CH 业主
C2   EN Assistant President
C2   CH 协理
C10  EN Specialist
C10  CH 专员
C12  EN Factory Manager
C12  CH 厂长
C13  EN Advisor
C13  CH 顾问
C14  EN Others
C14  CH 其他
C15  EN Supervisor
C15  CH 主管
C16  EN Engineer
C16  CH 工程师
C17  EN Chairman/President
C17  CH 董事长兼总经理
C18  EN Principal
C18  CH 经理人
C19  EN Assistant Principal
C19  CH 助理经理人
C20  EN Secretary
C20  CH 秘书
C29  EN Partner
C29  CH 合夥人
C30  EN Production Manager
C30  CH 生产经理
C31  EN Personal Assistant
C31  CH 特别助理
C32  EN Personnel Manager
C32  CH 人事经理
C33  EN Human Resource Manager
C33  CH 人力资源部经理/人事经理
C34  EN Quantity Surveyor
C34  CH 品管人员
C35  EN Executive Secretary
C35  CH 执行秘书
C36  EN Analyst
C36  CH 分析师
C38  EN Vice Manager
C38  CH 副经理
C39  EN Executive Assistant
C39  CH 执行助理
C40  EN Chief Representative
C40  CH 首席代表
C41  EN Officer
C41  CH 公务员
C42  EN Store Manager
C42  CH 店长
C43  EN Administration Manager
C43  CH 行政经理
C44  EN Senior Manager
C44  CH 高级经理
C45  EN Chairwoman
C45  CH (女性)董事长
C46  EN Public Affairs Manager
C46  CH 公关部经理
C47  EN Quality Control Manager
C47  CH 品管经理
C48  EN Vice General Manager
C48  CH 副总经理
C49  EN R & D Manager
C49  CH 研发经理
C50  EN Executive
C50  CH 执行长
C51  EN Vice Factory Manager
C51  CH 副厂长
C52  EN Professor
C52  CH 教授
C53  EN Lecturer
C53  CH 讲师
C54  EN Operations Assistant
C54  CH 业务助理
C55  EN Captain
C55  CH 船长
C56  EN Associate Manager
C56  CH 副理
C57  EN Vice Associate Manager
C57  CH 副理
C58  EN Associate Professor
C58  CH 副教授
C59  EN Teacher
C59  CH 老师
C60  EN Purchaser
C60  CH 购专员
C61  EN Representative
C61  CH 代表
C62  EN Special Assistant
C62  CH 特别助理
C63  EN Executive Director
C63  CH 执行董事
C64  EN Management Officer
C64  CH 管理人员
C65  EN Director General
C65  CH 总经理
C66  EN Deputy Director
C66  CH 副厂长
C67  EN Financial Supervisor
C67  CH 财务专员
C68  EN Chief Engineer
C68  CH 主要工程师
C69  EN Board Member
C69  CH 理事会员
C70  EN Dean
C70  CH 院长
C71  EN General Affairs Manager
C71  CH 总务经理
C72  EN Finance Assistant
C72  CH 财务助理
C73  EN Medical Doctor
C73  CH 内科医生
C74  EN Credit Officer
C74  CH 徵信人员
C75  EN Auditing Manager
C75  CH 稽核经理
C76  EN Senior Position
C76  CH 资深人员
C77  EN Legislator
C77  CH 立法委员
C78  EN Accounts Assistant
C78  CH 会计助理
C79  EN Speaker
C79  CH 广播员
C80  EN Marketing Consultant
C80  CH 行销顾问
C81  EN Senior Executive
C81  CH 资深执行
C82  EN Council Member
C82  CH 会议委员
C83  EN Secretary General
C83  CH 经理秘书
C84  EN Business Section Chief
C84  CH 业务部主管
C85  EN Administration Chief
C85  CH 行政部主管
C86  EN Councillor
C86  CH 议员
C87  EN Minister
C87  CH 部长
C88  EN Ambassador to Saudi Arabia
C88  CH 沙乌地阿拉大使
C89  EN Ambassador to South Africa
C89  CH 南非大使
C90  EN Ambassador to South Korea
C90  CH 南韩大使
C91  EN Political Vice Minister
C91  CH 政治部副部长
C92  EN Publisher
C92  CH 发行人
C93  EN Public Relations Manager
C93  CH 发行部经理
C94  EN Vice Minister
C94  CH 副部长
C95 EN Management Executive
C95 CH 负责人
C96 EN Admiral
C96 CH 海军上将
C97 EN Jeweler
C97 CH 珠宝员
C98 EN Designer
C98 CH 绘图员
C99 EN Bus Driver
C99 CH 公车驾驶员
C100 EN Pilot
C100 CH 飞行员
C101 EN Technician
C101 CH 技术人员
C102 EN Salesman
C102 CH 销售员
C103 EN Loans Officer
C103 CH 贷款
C104 EN Product Chief
C104 CH 生产总裁
C105 EN Assistant Lecturer
C105 CH 助教
C106 EN Sales Engineer
C106 CH 业务工程师
C107 EN Controller
C107 CH 监察人
C108 EN Foreman
C108 CH 领班
C109 EN Senior Programmer
C109 CH 高级程式员
C110 EN Commander-in-Chief
C110 CH 总司令
C111 EN Executive Manager
C111 CH 执行经理
C112 EN Personnel Assistant
C112 CH 人事助理
C113 EN Engineering Manager
C113 CH 工程部经理
C114 EN Marketing Specialist
C114 CH 市场专员
C115 EN Governor
C115 CH 理事
C116 EN Research Assistant
C116 CH 研究助理
C117 EN Executive Vice General Manager
C117 CH 执行副总经理
C118 EN Account Executive
C118 CH 执行会计
C119 EN Sales Engineer
C119 CH 销售工程师
C120 EN Executive Vice Chairman
C120 CH 执行副总裁
C121 EN Project Manager
C121 CH 专案经理
C122 EN Design Manager
C122 CH 设计经理
C123 EN Product Engineering Manager
C123 CH 产品工程部经理
C124 EN Vice Plant Manager
C124 CH 副厂长
C125 EN Honorary Chairman
C125 CH 名誉董事长
C126 EN Section Chief
C126 CH 部门主管
C127 EN Purchasing Manager
C127 CH 购经理
C128 EN Air Stewardess
C128 CH 空中小姐
C129 EN Assistant Finance Manager
C129 CH 财务副理
C130 EN Accounting Supervisor
C130 CH 会计主任
C131 EN Vice Finance Manager
C131 CH 财务副理
C132 EN Vice Sales Manager
C132 CH 业务副理
C133 EN Chief Accountant
C133 CH 主办会计
C134 EN President/General Manager
C134 CH 董事长/总经理
C135 EN Librarian
C135 CH 图书管理人员
C136 EN Assistant
C136 CH 助理
C137 EN Quality Controller
C137 CH 品管人员
C138 EN Staff
C138 CH 职员
C139 EN Gardener
C139 CH 园丁
C140 EN Operator
C140 CH 操作员
C141 EN Repairman
C141 CH 修理员
C142 EN Accounting Manager
C142 CH 会计部经理
C143 EN Agent
C143 CH 代理商
C144 EN Assistant Sales Manager
C144 CH 业务副理
C145 EN Authorized Representative
C145 CH 发言人
C146 EN Business Manager
C146 CH 营业部经理
C147 EN CPA
C147 CH 会计师
C148 EN Chief CPA
C148 CH 主办会计师
C149 EN Chief Manager
C149 CH 主要经理
C150 EN Chief Representative
C150 CH 主要代表人
C151 EN Commercial Manager
C151 CH 商业经理
C152 EN Country General Manager
C152 CH 海外分公司总经理
C153 EN Development Manager
C153 CH 开发部经理
C154 EN Director of R & D
C154 CH 研发部主管
C155 EN Director of Sales & Marketing
C155 CH 行销部主管
C156 EN Division Chief
C156 CH 部门主管
C157 EN Executive Vice General Manager
C157 CH 执行副总
C158 EN Export Manager
C158 CH 出口部经理
C159 EN Financial & Adm. Manager
C159 CH 财务及行政经理
C160 EN Foreign Dept. Manager
C160 CH 国外部经理
C161 EN General Manager/President
C161 CH 总经理兼董事长
C162 EN Import & Export Manager
C162 CH 进出口部经理
C163 EN Import Manager
C163 CH 进口部经理
C164 EN Office Manager
C164 CH 经理人
C165 EN Overseas Manager
C165 CH 海外经理
C167 EN Plant Manager
C167 CH 厂长
C168 EN Plant Supervisor
C168 CH 工厂主任
C169 EN Product Manager
C169 CH 生产部经理
C170 EN Senior Representative
C170 CH 资深代表
C171 EN System Manager
C171 CH 系统经理
C172 EN Trading Manager
C172 CH 贸易经理
C173 EN Sales Executive
C173 CH 执行业务员
C174 EN Lawyer
C174 CH 律师
C175 EN Pharmacist
C175 CH 药剂师
C176 EN Architect
C176 CH 建筑师
C177 EN Scrivener
C177 CH 代书
C178 EN Technical Manager
C178 CH 技术部经理
C179 EN CEO
C179 CH 主要管理人
C180 EN Saleswoman
C180 CH 女业务员
C181 EN Counselor
C181 CH 参事临时代办
C182 EN Ambassador
C182 CH 大使
C183 EN Honorary
C183 CH 名誉领事
C184 EN General Plant Manager
C184 CH 总厂长
C185 EN Superintendent
C185 CH 校长
C186 EN Trade Commissioner
C186 CH 处长
C187 EN Doctor
C187 CH 医生
C188 EN Chairman/General Manager
C188 CH 董事长兼总经理
C189 EN Correspondent
C189 CH 记者
C190 EN Doctor of the Urology Division
C190 CH Doctor of the Urology Disision
C191 EN Accoucheur
C191 CH Accoucheur
C192 EN Dermatologist & Doctor of Urology
C192 CH Dermatologist & Doctor of Urology
C193 EN Assistant Divison Manager
C193 CH Assistant Divison Manager
C194 EN Owner/Dermatologist
C194 CH 拥有者/皮肤科医生
C195 EN Chief Druggist
C195 CH 药剂师
C196 EN Owner/Chinese Medical Physician
C196 CH 拥有者/中医师
C197 EN Associate Chief
C197 CH 会长
C198 EN Veterinarian
C198 CH 兽医师
C199 EN Examiner
C199 CH 检验师
C200 EN Alternate Director
C200 CH Alternate Director
C201 EN Chairman
C201 CH 董事长
C202 EN Chairman & Managing Director
C202 CH Chairman & Managing Director
C203 EN Chief Executive
C203 CH Chief Executive
C204 EN Committee Member
C204 CH Committee Member
C205 EN Company Secretary
C205 CH Company Secretary
C206 EN Credit Manager
C206 CH Credit Manager
C207 EN Credit Controller
C207 CH Credit Controller
C208 EN Director & Company Secretary
C208 CH Director & Company Secretary
C209 EN Divisional General Manager
C209 CH Divisional General Manager
C210 EN  Divisional Mananger
C210 CH  Divisional Mananger
C211 EN  General Manager & Company Secretary
C211 CH  General Manager & Company Secretary
C212 EN  Group Controller
C212 CH  Group Controller
C213 EN  Manager
C213 CH  Manager
C214 EN  Marketing Director
C214 CH  Marketing Director
C215 EN  Personnel Manager
C215 CH  Personnel Manager
C216 EN  Purchasing Director
C216 CH  Purchasing Director
C217 EN  Receive Manager
C217 CH  Receive Manager
C218 EN  Senior Partner
C218 CH  Senior Partner
C219 EN  State Manager
C219 CH  State Manager
C220 EN  Technical Director
C220 CH  Technical Director
C221 EN  Textils Manager
C221 CH  Textils Manager
C222 EN  Treasurer
C222 CH  Treasurer
C223 EN  Vice Chairman
C223 CH  Vice Chairman
C224 EN  Vice President
C224 CH  Vice President
C225 EN  Branch Manager
C225 CH  Branch Manager
C226 EN  EDP Manager
C226 CH  EDP Manager
C227 EN  Production Director
C227 CH  Production Director
22   EN  Unknown
22   CH  Unknown
23   EN  Secretary of the Communist Party
23   CH  党委书记
24   EN  Vice Chief Representative
24   CH  首席副代表
25   EN  Vice Department Manager
25   CH  副部门经理
26   EN  student
26   CH  学生
27   EN  Head
27   CH  Head
28   EN  Vice Head
28   CH  Vice Head
29   EN  Deputy Chief
29   CH  Deputy Chief
30   EN  Vice Secretary of the Communist Party
30   CH  Vice Secretary of the Communist Party
31   EN  Deputy Header
31   CH  Deputy Header
32   EN  Senior Engineer
32   CH  高级工程理师
33   EN  Deputy Executive Director
33   CH  执行副董事
34   EN  Deputy Section Chief
34   CH  Deputy Section Chief
35   EN  Secretary of the Communist Youth League
35   CH  团支部书记
36   EN  Editor
36   CH  编辑
37   EN  Deputy Sectory
37   CH  Deputy Sectory
38   EN  Senior Consultant
38   CH  高级顾问
39   EN  Deputy Secretary
39   CH  副秘书
40   EN  Investigator
40   CH  调查员
41   EN  Legal Representative
41   CH  法人代表
42   EN  Vice Chairman & Executive Vice President
42   CH  副董事长兼副执行行长
43   EN  Vice chairman & Vice President
43   CH  副董事长兼副行长
44   EN  Executive Director & Vice President
44   CH  执行董事兼副行长
45   EN  Chairman & President
45   CH  董事长兼行长
46   EN  Executive Vice Manager
46   CH  副执行经理
47   EN  Executive Vice President
47   CH  常务副行长
48   EN  Senior Vice President
48   CH  副行长
49   EN  Headmaster
49   CH  校长
50   EN  Vice General Manager & Sceretary of the Communist Party of China
50   CH  副总经理和党支部书记
51   EN  General Accountant
51   CH  总会计师
52   EN  Marketing Assistant
52   CH  行销助理
53   EN  Vice Factory Manager
53   CH  副厂长
54   EN  Legal Person
54   CH  法人代表
55   EN  Vice Managing Director
55   CH  副董事总经理
56   EN  General Engineer
56   CH  总工程师
57   EN  Assitant Minister
57   CH  部长助理
58   EN  Senior Engineer
58   CH 高级工程师	
59   EN  Project Supervisor
59   CH  项目主管
60   EN  Vice Mayor
60   CH  副市长
61   EN  Assistant Factory Manager
61   CH  厂长助理
62   EN  Vice Chairman/Vice General Manager
62   CH  副董事长/副总经理
63   EN  Director/Vice General Manager
63   CH  董事/副总经理
64   EN  Honorary Professor
64   CH  名誉教授
65   EN  Vice Managing General Manager
65   CH  Vice Managing General Manager
66   EN  Assistant Mayor
66   CH  市长助理
67   EN  Vice Dean
67   CH  Vice Dean
68   EN  Vice Representative
68   CH  副代表
69   EN  Member
69   CH  成员
70   EN  Executive Member
70   CH  执行成员
71   EN  Vice Chief Manager
71   CH  副主要经理
72   EN  Statistician
72   CH  统计员
73   EN  Vice Chairman/General Manager
73   CH	 副董事长兼总经理
74   EN  General Manager Assistant
74   CH  总经理助理
75   EN  Vice Professer
75   CH  副教授	
76   EN  Mayor
76   CH  市长
77   EN  Business Representative
77   CH  业务代表
78   EN  Executive Vice Director
78   CH  常务付所长
79   EN  Nurse
79   CH  护士
80   EN  Vice Regimental Commander
80   CH  副团长
81   EN  Vice Premier
81   CH  副总理
82   EN  Consul
82   CH  领事
