6       6       50      4       ENG     ESP     POR   COD
01      EN      aircraft
01      SP      aeronave
01      PO      aeronave
01      CO      01-aeronave
02      EN      aircrafts
02      SP      aeronaves
02      PO      aeronaves
02      CO      02-aeronaves
03      EN      ambulance
03      SP      ambulancia
03      PO      ambulancia
03      CO      03-ambulancia
04      EN      ambulances
04      SP      ambulancias
04      PO      ambulancias
04      CO      04-ambulancias
05      EN      bus
05      SP      autobus
05      PO      autobus
05      CO      05-autobus
06      EN      buses
06      SP      autobuses
06      PO      autobuses
06      CO      06-autobuses
07      EN      automobile
07      SP      autom�vil
07      PO      autom�vil
07      CO      07-autom�vil
08      EN      automobiles
08      SP      autom�viles
08      PO      autom�viles
08      CO      08-autom�viles
09      EN      tank truck
09      SP      autotanque
09      PO      autotanque
09      CO      09-autotanque
10      EN      tank trucks
10      SP      autotanques
10      PO      autotanques
10      CO      10-autotanques
11      EN      ship
11      SP      barco
11      PO      barco
11      CO      11-barco
12      EN      ships
12      SP      barcos
12      PO      barcos
12      CO      12-barcos
13      EN      boat
13      SP      bote
13      PO      bote
13      CO      13-bote
14      EN      boats
14      SP      botes
14      PO      botes
14      CO      14-botes
15      EN      truck
15      SP      camion
15      PO      camion
15      CO      15-camion
16      EN      trucks
16      SP      camiones
16      PO      camiones
16      CO      16-camiones
17      EN      cement truck
17      SP      camion de cemento
17      PO      camion de cemento
17      CO      17-camion de cemento
18      EN      cement trucks
18      SP      camiones de cemento
18      PO      camiones de cemento
18      CO      18-camiones de cemento
19      EN      delivery truck
19      SP      camion de distribuci�n
19      PO      camion de distribuci�n
19      CO      19-camion de distribuci�n
20      EN      delivery trucks
20      SP      camiones de distribuci�n
20      PO      camiones de distribuci�n
20      CO      20-camiones de distribuci�n
21      EN      double wheel truck
21      SP      camion de doble rodada
21      PO      camion de doble rodada
21      CO      21-camion de doble rodada
22      EN      double wheel trucks
22      SP      camiones de doble rodada
22      PO      camiones de doble rodada
22      CO      22-camiones de doble rodada
23      EN      service truck
23      SP      camion de servicio
23      PO      camion de servicio
23      PO      23-camion de servicio
24      EN      service trucks
24      SP      camiones de servicio
24      PO      camiones de servicio
24      PO      24-camiones de servicio
25      EN      pick up truck
25      SP      camioneta "pick up"
25      PO      camioneta "pick up"
25      CO      25-camioneta "pick up"
26      EN      pick up trucks
26      SP      camionetas "pick up"
26      PO      camionetas "pick up"
26      CO      26-camionetas "pick up"
27      EN      van
27      SP      camioneta "Van"
27      PO      camioneta "Van"
27      PO      27-camioneta "Van"
28      EN      vans
28      SP      camionetas "Van"
28      PO      camionetas "Van"
28      PO      28-camionetas "Van"
29      EN      freight container
29      SP      contenedor
29      PO      contenedor
29      CO      29-contenedor
30      EN      freight containers
30      SP      contene ores
30      PO      contenedores
30      CO      30-contenedores
31      EN      barge
31      SP      lanchon
31      PO      lanchon
31      CO      31-lanchon
32      EN      barges
32      SP      lanchones
32      PO      lanchones
32      CO      32-lanchones
33      EN      motorcycle
33      SP      motocicleta
33      PO      motocicleta
33      CO      33-motocicleta
34      EN      motorcycles
34      SP      motocicletas
34      PO      motocicletas
34      CO      34-motocicletas
35      EN      truck tractor
35      SP      tractocamion
35      PO      tractocamion
35      CO      35-tractocamion
36      EN      truck tractors
36      SP      tractocamiones
36      PO      tractocamiones
36      CO      36-tractocamiones
37      EN      tractor
37      SP      tractor
37      PO      tractor
37      CO      37-tractor
38      EN      tractors
38      SP      tractores
38      PO      tractores
38      CO      38-tractores
39      EN      freight train car
39      SP      vagon de ferrocarril
39      PO      vagon de ferrocarril
39      CO      39-vagon de ferrocarril
40      EN      freight train cars
40      SP      vagones de ferrocarril
40      PO      vagones de ferrocarril
40      CO      40-vagones de ferrocarril
41      EN      transport fleet
41      SP      flotilla de transporte
41      PO      flotilla de transporte
41      CO      41-flotilla de transporte
42      EN      vehicles of various models & trademarks
42      SP      veh�culos de diferentes marcas y modelos
42      PO      veh�culos de diferentes marcas y modelos
42      CO      42-veh�culos de diferentes marcas y modelos
