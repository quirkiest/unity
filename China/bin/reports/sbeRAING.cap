//****************************************************************************
//  sbeRAING.cap - SuperBIR Chinese. HTML Risk Analysis - Industry Norm Grouping
//****************************************************************************
DBSECTION "general";
LANGUAGE "CHI";
LINE_LENGTH 1200;
NOTFILL;
DECLARE RESULT;
DECLARE WIDTH; DECLARE HEIGHT;

// :@RISK:L?  variables
// ---------------------------
DECLARE RISKINDVALUELC;             RISKINDVALUELC              := 0;
DECLARE RISKINDDISPLAYVALUELC;      RISKINDDISPLAYVALUELC       := 1;

DECLARE RISKINDMINLC;               RISKINDMINLC                :=20;
DECLARE RISKINDMAXLC;               RISKINDMAXLC                :=21;

DECLARE RISKCOUNT;
DECLARE RISKIND;

DECLARE RISKINDMIN; DECLARE RISKINDMAX;
LCOUNT := RISKINDMINLC;  RISKINDMIN := ATOI([:@RISK:L?]);
LCOUNT := RISKINDMAXLC;  RISKINDMAX := ATOI([:@RISK:L?]);


// :@INDNORM:L?  variables
// ---------------------------
DECLARE INDGROUP_SIC_LC;            INDGROUP_SIC_LC             := 0;
DECLARE INDGROUP_DESC_LC;           INDGROUP_DESC_LC            := 1;
DECLARE INDGROUP_RI_MEDIAN_LC;      INDGROUP_RI_MEDIAN_LC       := 2;
DECLARE INDGROUP_RI_LOWERQ_LC;      INDGROUP_RI_LOWERQ_LC       := 3;
DECLARE INDGROUP_RI_UPPERQ_LC;      INDGROUP_RI_UPPERQ_LC       := 4;

DECLARE INDGROUP_RI_PERC_TOTAL; INDGROUP_RI_PERC_TOTAL := 0;
DECLARE INDGROUP_RI_PERC_LC;    INDGROUP_RI_PERC_LC    := 0;


// :@FIELD:L?  variables
// ---------------------------
DECLARE COMPNAMELC;     COMPNAMELC      := 0;


LCOUNT := RISKINDDISPLAYVALUELC;
IF(((EXISTS([:@RISK:L?]))+(STRCMPI([:@RISK:L?],"N/A")!=0))==2)
{
LEFT_MARGIN 16;
            LCOUNT := INDGROUP_RI_MEDIAN_LC;
            IF(EXISTS([:@INDNORM:L?]))
            {
        // Industry Norm Grouping
        // ---------------------------------------------
        `
        <H5 class=tableHead>Industry Norm Grouping</H5>
         <TABLE class="chartTable2" cellSpacing="0" cellPadding="2" width="630" bgColor="#eff3f7" border="0">
            <TR>
                <TD class="dbRowB" width="4" bgColor="#c8d7f0">&nbsp;</TD>
                <TD class="dbRowB" align="left" bgColor="#c8d7f0">&nbsp;</TD>
                <TD class="dbRowB" align="middle" width="250" bgColor="#c8d7f0">&nbsp;</TD>
                <TD class="dbRowB" width="4" bgColor="#c8d7f0">&nbsp;</TD>
            </TR>
            <TR>
                <TD class="dbRowClabel" width="4">&nbsp;</TD>
                <TD class="dbRowClabel">Industry Group SIC</TD>
                <TD class="dbRowCresults" align="left" width="250">`
                    LCOUNT := INDGROUP_SIC_LC; `[:@INDNORM:L?] </TD>
                <TD class="dbRowCresults" width="4">&nbsp;</TD>
            </TR>
            <TR>
                <TD class="dbRowClabel" width="4">&nbsp;</TD>
                <TD class="dbRowClabel">Industry Group Description</TD>
                <TD class="dbRowCresults" align="left" width="250">`
                    LCOUNT := INDGROUP_DESC_LC; `[:@INDNORM:L?] </TD>
                <TD class="dbRowCresults" width="4">&nbsp;</TD>
            </TR>

                <TR>
                    <TD class="dbRowClabel" width="4">&nbsp;</TD>
                    <TD class="dbRowClabel" valign=top>Industry Group EMMA Score Quartile Values</TD>
               			<TD class="dbRowCresults" align="left" width="250"><STRONG>
                <TABLE>
                <TR>
                    <TD width="250">Lower Quartile</TD>
                    <TD align="right">` LCOUNT := INDGROUP_RI_LOWERQ_LC; `[:@INDNORM:L?]</TD>
                </TR>
                <TR>
                    <TD width="250">Median</TD>
                    <TD align="right">` LCOUNT := INDGROUP_RI_MEDIAN_LC; `[:@INDNORM:L?]</TD>
                </TR>
                <TR>
                    <TD width="250">Upper Quartile</TD>
                    <TD align="right">` LCOUNT := INDGROUP_RI_UPPERQ_LC; `[:@INDNORM:L?]</TD>
                </TR>
                </TABLE>
                </STRONG>
            </TD>
                    <TD class="dbRowCresults" width="4">&nbsp;</TD>
                </TR>
        </TABLE>`
            }

        `
        <BR>
        `
				LEFT_MARGIN 9;
				
        LCOUNT := INDGROUP_RI_MEDIAN_LC;
        IF(EXISTS([:@INDNORM:L?]))
        {
            // Company Risk Vs Industry Norm chart
            // --------------------------------------
            `
            <H5>Company Risk Vs Industry Norm Chart</H5>
            <TABLE class=graphTable cellSpacing=0 cellPadding=0 width=630 border=0>
                <TR>
                    <TD>
                        <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
                            <TR>
                                <TD class=graphLabel width=95>EMMA Score</TD>
                                <TD class=graphAxisRightBorder width=1>
                                    <IMG height=1 src="[:@WEBSERVER]images/spacer.gif" width=1>
                                </TD>
                                <TD vAlign=center align=left background=[:@WEBSERVER]/images/graph_grid_bg_3_50px.gif bgColor=#eff3f7 height=30>`
                                    LCOUNT := RISKINDDISPLAYVALUELC; WIDTH := 50*ATOI([:@RISK:L?]);
                                    VCOUNT := WIDTH;
                                    `
                                    <IMG height=15 src="[:@WEBSERVER]images/dbGraphYellow_Horiz.gif" width="[:#:VAR:V?:FSTRIP]">
                                </TD>
                            </TR>
                            <TR>
                                <TD class=graphLabel align=right width=63>
                                    <IMG height=1 src="[:@WEBSERVER]images/spacer.gif" width=1>
                                </TD>
                                <TD class=graphAxisRightBorder width=1>
                                    <IMG height=1 src="[:@WEBSERVER]images/spacer.gif" width=1>
                                </TD>
                                <TD background=[:@WEBSERVER]/images/graph_grid_bg_3_50px.gif>
                                    <IMG height=1 src="[:@WEBSERVER]images/spacer.gif" width=1>
                                </TD>
                            </TR>
                            <TR>
                                <TD class=graphLabel width=95>Lower Quartile</TD>
                                <TD class=graphAxisRightBorder width=5>
                                    <IMG height=1 src="[:@WEBSERVER]images/spacer.gif" width=1>
                                </TD>
                                <TD vAlign=center align=left background=[:@WEBSERVER]/images/graph_grid_bg_3_50px.gif bgColor=#eff3f7 height=30>`

                                    LCOUNT := INDGROUP_RI_LOWERQ_LC; WIDTH := 50*ATOI([:@INDNORM:L?]);
                                    VCOUNT := WIDTH;
                                    `
                                    <IMG height=15 src="[:@WEBSERVER]images/dbGraphLBlue_Horiz.gif" width="[:#:VAR:V?:FSTRIP]">
                                    <!--<IMG height=15 src="[:@WEBSERVER]images/dbGraphLBlue_Horiz.gif" width="[:#:VAR:V?:FSTRIP]">-->
                                </TD>
                            </TR>
                            <TR>
                                <TD class=graphLabel width=95>Median</TD>
                                <TD class=graphAxisRightBorder width=5>
                                    <IMG height=1 src="[:@WEBSERVER]images/spacer.gif" width=1>
                                </TD>
                                <TD vAlign=center align=left background=[:@WEBSERVER]/images/graph_grid_bg_3_50px.gif bgColor=#eff3f7 height=30>`
                                    LCOUNT := INDGROUP_RI_MEDIAN_LC; WIDTH := 50*ATOI([:@INDNORM:L?]);
                                    VCOUNT := WIDTH;
                                    `
                                    <IMG height=15 src="[:@WEBSERVER]images/dbGraphLBlue_Horiz.gif" width="[:#:VAR:V?:FSTRIP]">
                                </TD>
                            </TR>
                            <TR>
                                <TD class=graphLabel width=95>Upper Quartile</TD>
                                <TD class=graphAxisRightBorder width=5>
                                    <IMG height=1 src="[:@WEBSERVER]images/spacer.gif" width=1>
                                </TD>
                                <TD vAlign=center align=left background=[:@WEBSERVER]/images/graph_grid_bg_3_50px.gif bgColor=#eff3f7 height=30>`
                                    LCOUNT := INDGROUP_RI_UPPERQ_LC; WIDTH := 50*ATOI([:@INDNORM:L?]);
                                    VCOUNT := WIDTH;
                                    `
                                    <IMG height=15 src="[:@WEBSERVER]images/dbGraphLBlue_Horiz.gif" width="[:#:VAR:V?:FSTRIP]">
                                    <!--<IMG height=15 src="[:@WEBSERVER]images/dbGraphLBlue_Horiz.gif" width="[:#:VAR:V?:FSTRIP]"><IMG height=15 src="[:@WEBSERVER]images/dbGraphLBlue_Horiz_End.gif" width=1>-->
                                </TD>
                            </TR>
                        </TABLE>
                    </TD>
                </TR>
           <TR>
        <TD>
            <TABLE align="left" cellSpacing=0 cellPadding=0 width=630>
            <TR>
                 <TD align="left" width=97>&nbsp;</TD>          

                 `

                    RISKCOUNT := RISKINDMIN-1;
                    WHILE(RISKCOUNT <= RISKINDMAX)
                    {
                        `
                       <TD class="graphGridTop" align="left" width=47>` VCOUNT := RISKCOUNT; `[:#:VAR:V?:FSTRIP]</TD>`
                        INC RISKCOUNT;
                    }
                `
                  
                </TR>
                

                
                <TR>
                     <TD class=graphLabel align=right colSpan=11>EMMA Score</TD>
                </TR>
                <TR>
                    <TD align=right colSpan=11>
                        <TABLE class=graphKey cellSpacing=0 cellPadding=5 align=right border=0>
                            <TR>
                                <TD><IMG height=9 src="[:@WEBSERVER]images/yellow.gif" width=9></TD>
                                <TD class=graphLabel>` LCOUNT := COMPNAMELC; `[:@FIELD:L?]</TD>
                                <TD><IMG height=9 src="[:@WEBSERVER]images/midblue.gif" width=9></TD>
                                <TD class=graphLabel>Industry</TD>
                            </TR>
                        </TABLE>
                    </TD>
                </TR>
            </TABLE>
               </TD>
    </TR>
    </TABLE>
    `

            IF(EXISTS([:@INDGROUP_RI_PERC]))
            {
                INDGROUP_RI_PERC_TOTAL := COUNTSPEC([:@INDGROUP_RI_PERC]);

                // Distribution of Risk Index
                // ----------------------------
                `
                <H5 id="RAnDistributionRiskGraph">Distribution of EMMA Score in Huaxia D&amp;B China's Database</H5>
                <TABLE cellSpacing="0" cellPadding="0" width="630" border="0">
                    <TR>
                        <TD width="380">
                             <!--<TABLE class="graphTable" style="BORDER-BOTTOM: #000000 1px solid" cellSpacing="0" cellPadding="0" width="450" border="0">-->
                    					<TABLE class="graphTable"  cellSpacing="0" cellPadding="0" width="450" border="0">
                                <TR>
                                    <TD>
                                        <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
                                            <TR>
                                                <TD valign="bottom" class="graphLabel" align="right" width="80">
                                                    <TABLE height="180" cellSpacing="0" cellPadding="0" width="80" border="0">
                                                        <TR align="right"><TD>40%</TD></TR>
                                                        <TR align="right"><TD>35%</TD></TR>
                                                        <TR align="right"><TD>30%</TD></TR>
                                                        <TR align="right"><TD>25%</TD></TR>
                                                        <TR align="right"><TD>20%</TD></TR>
                                                        <TR align="right"><TD>15%</TD></TR>
                                                        <TR align="right"><TD>10%</TD></TR>
                                                        <TR align="right"><TD> 5%</TD></TR>
                                                        <TR align="right"><TD> 0%</TD></TR>
                                                    </TABLE>
                                                </TD>
                                                <TD class="graphAxisRightBorder" width="5"><IMG height="1" src="[:@WEBSERVER]images/spacer.gif" width="1">
                                                </TD>`

                                        INDGROUP_RI_PERC_LC := 0;
                                        WHILE(INDGROUP_RI_PERC_LC < INDGROUP_RI_PERC_TOTAL)
                                        {
                                            `
                                            <TD vAlign="bottom" align="middle" background="[:@WEBSERVER]images/graph_VERT_bg.gif" bgColor="#eff3f7" height="170">
                                                 <!--IMG height="1" src="[:@WEBSERVER]images/dbGraphLBlue_Vert_End.gif" width="20"-->
                                								`

                                                LCOUNT := INDGROUP_RI_PERC_LC; HEIGHT := 5*ATOI([:@INDGROUP_RI_PERC:L?]);
                                                VCOUNT := HEIGHT*8/10;
                                                `
                                                <IMG height="[:#:VAR:V?]" src="[:@WEBSERVER]images/dbGraphLBlue_Vert.gif" width="20">
                                            </TD>`

                                            INC INDGROUP_RI_PERC_LC;
                                        }
                                        `
                                            </TR>
                                        </TABLE>
                                    </TD>
                                </TR>
                            </TABLE>
                            <TABLE cellSpacing="0" cellPadding="0" width="450" border="0">
                                <TR>
                                    <TD align="right" width="48">&nbsp;</TD>`

                                    INDGROUP_RI_PERC_LC := 0;
                                    WHILE(INDGROUP_RI_PERC_LC < INDGROUP_RI_PERC_TOTAL)
                                    {
                                        `
                                        <TD align="right" width="34">` VCOUNT := INDGROUP_RI_PERC_LC+1; `[:#:VAR:V?]</TD>`
                                        INC INDGROUP_RI_PERC_LC;
                                    }
                                `
                                </TR>
                            </TABLE>
                        </TD>
                        <TD>&nbsp;</TD>
                        <TD vAlign="top" align="right" width="140">
                            <TABLE  style="BORDER-RIGHT: #000066 1px solid; BORDER-TOP: #000066 1px solid; BORDER-LEFT: #000066 1px solid; BORDER-BOTTOM: #000066 1px solid"
                                    height="170" cellSpacing="0" cellPadding="2" width="140" bgColor="#eff3f7" border="0">
                                <TR>
                                    <TD class="dbRowB" bgColor="#c8d7f0" Align="center"><B>Score</B></TD>
                                    <TD class="dbRowB" bgColor="#c8d7f0" Align="center"><B>Distribution</B></TD>
                                </TR>`

                            INDGROUP_RI_PERC_LC := 0;
                            WHILE(INDGROUP_RI_PERC_LC < INDGROUP_RI_PERC_TOTAL)
                            {
                                `
                                <TR>
                                    <TD class="dbRowClabel" align="middle">`    VCOUNT := INDGROUP_RI_PERC_LC+1; `[:#:VAR:V?]</TD>
                                    <TD class="dbRowCresults" align="middle">`  LCOUNT := INDGROUP_RI_PERC_LC;   `[:@INDGROUP_RI_PERC:L?]%</TD>
                                </TR>`
                                INC INDGROUP_RI_PERC_LC;
                            }
                            `
                            </TABLE>
                        </TD>
                    </TR>
                </TABLE>
                `
            }
            `
            <P><I>Note: please refer to the report appendix for the principle and major factors of Huaxia D&amp;B China EMMA Score.</I></P>
            `
        }
       }    
