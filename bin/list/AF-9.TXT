6       6       64      3       ENG     ESP     POR     JAP
1       EN      parent
1       SP      la casa matriz
1       PO      la casa matriz
1       JA      親会社
2       EN      partners
2       SP      los socios
2       PO      los socios
2       JA	ﾊﾟ-ﾄﾅ-
4       EN      affiliate
4       SP      la afiliada
4       PO      la afiliada
4       JA　　　関連会社
5       EN      family member of the partners
5       SP      un familiar de los socios
5       PO      un familiar de los socios
5       JA	ﾊﾟ-ﾄﾅ-の家族
6       EN      subsidiary
6       SP      la subsidiaria
6       PO      la subsidiaria
6       JA	子会社
8       EN      attorney
8       SP      el abogado
8       PO      el abogado
8       JA	弁護士
F3      EN      one partner
F3      SP      una socia
F3      PO      una socia
F3      JA	ﾊﾟ-ﾄﾅ-
F7      EN      owner
F7      SP      due､a
F7      PO      due､a
F7      JA	所有者
M3      EN      one partner
M3      SP      un socio
M3      PO      un socio
M3      JA	ﾊﾟ-ﾄﾅ-
M7      EN      owner
M7      SP      due､o
M7      PO      due､o
M7      JA	所有者　
