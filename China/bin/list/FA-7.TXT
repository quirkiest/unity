6       6       64      3       ENG     ESP     POR
1       EN      Office
1       SP      edificio de oficinas
1       PO      edificio de oficinas
2       EN      Industrial
2       SP      industrial
2       PO      industrial
3       EN      Commercial
3       SP      comercial
3       PO      comercial
4       EN      Residential
4       SP      residencial
4       PO      residencial
5       EN      Shopping mall
5       SP      centro comercial
5       PO      centro comercial
6       EN      Industrial warehouse
6       SP      deposito industrial
6       PO      deposito industrial
7       EN      Airport terminal
7       SP      aeropuerto
7       PO      aeropuerto
8       EN      Hangar
8       SP      hangar
8       PO      hangar
9       EN      Pier
9       SP      muelle
9       PO      muelle
10      EN      Train station
10      SP      estaci�n ferroviaria
10      PO      estaci�n ferroviaria
11      EN      Bus station
11      SP      estaci�n de autobuses
11      SP      estaci�n de autobuses
12      EN      Sidewalk stand
12      SP      puesto ambulante
12      PO      puesto ambulante
13      EN      Museum
13      SP      museo
13      PO      museo
14      EN      Farm
14      SP      hacienda
14      PO      hacienda
15      EN      Church
15      SP      iglesia
15      PO      iglesia
16      EN      Adapted for the company's purposes
16      SP      adaptado para las operaciones de la firma
16      PO      adaptado para las operaciones de la firma
17      EN      Subway station
17      SP      estaci�n del metro
17      PO      estaci�n del metro
