//*********************************************************************************
//  sbeFBIFN.cap - SuperBIR Eng. HTML Finance & Banking - Industry Financial Norms
//*********************************************************************************
DBSECTION "operatio";
LANGUAGE "ENG";
LINE_LENGTH 800;
NOTFILL;
DECLARE RESULT;
DECLARE NUMSECTIONS;
DECLARE NUMLOOPS;

DECLARE SICLCOUNT;

DECLARE INHNUMRECS;         INHNUMRECS  := 0;
DECLARE INHYCOUNT;          INHYCOUNT   := -1;
DECLARE INHLCOUNT;          INHLCOUNT   := 0;
DECLARE FINANCE_IFN_EXISTS; FINANCE_IFN_EXISTS := 0;

DECLARE MAX_COLUMNS;    MAX_COLUMNS  := 3;
DECLARE COLUMN_COUNT;   COLUMN_COUNT := 0;

// Initialise
// ---------------------
RESULT := RUNFORM([:@SIC: = ""]);

// Check the SIC codes
// ---------------------
IF(EXISTS([:s1:L0]))
{
    SICLCOUNT  := 0;
    NUMLOOPS   := LOOPS([:s1]);
    WHILE(SICLCOUNT < NUMLOOPS)
    {
        // Look-up the SIC codes
        // ---------------------
        LCOUNT := SICLCOUNT;
        IF(EXISTS([:s1:L?:B*]))
        {
            RESULT := RUNFORM([:@SIC: = "::operatio:s1:L?:B*:S4:FSTRIP"]);

            // See if this SIC code exists on Industry_Norm_Hist
            // --------------------------------------------------
            INHYCOUNT  := LOADREL([:+DCSD>SP>usp_GetIndustryFinancialNorms:K~SIC = :@SIC:~:CRatioType]);
            YCOUNT     := INHYCOUNT; INHNUMRECS := COUNTREL([:Y?]);
            IF(INHNUMRECS == 0)
            {
                YCOUNT := INHYCOUNT; INHYCOUNT := -1; YCOUNT := UNLOADREL([:Y?]);
            }
            ELSE
            {
                FINANCE_IFN_EXISTS  := 1;
                SICLCOUNT           := NUMLOOPS;
            }
        }
        INC SICLCOUNT;
    }
}

LEFT_MARGIN 9;

// Do Industry Norms exist?
// ---------------------------
IF(FINANCE_IFN_EXISTS)
{
    //**************************************************************
    // Sample Output
    //**************************************************************
    // RatioType MedianForYear0 MedianForYear1 MedianForYear2
    // --------- -------------- -------------- --------------
    // YEARS     2006           2005           2004
    // NUMENT    10101          7129           8135
    // ASTN      0.36892        0              0.36392
    // CLPR      5.45           5.44           5.43
    // CRRT      0.75205        0.74205        0.73205
    // DTRT      0.561805       0              0.531805
    // IVTN      96.4598        96.4498        96.4398
    // NPMG      5.3565         5.3545         0
    // PYPR      16.9664        16.9464        16.9364
    // QKRT      0.35626        0.34626        0
    // RTAS      6.13           6.43           6.33
    // RTEQ      6.13           6.43           6.33
    // TERT      1.25272        1.24272        1.23272
    //**************************************************************

    // Industry Financial Norms
    // ---------------------------
    `
    <H4>Industry Financial Norms</H4>
    <TABLE class="dbSummaryA" cellSpacing="0" cellPadding="0" width="630" bgColor="#dbe5f3" border="0">
    <TR>
        <TD align="middle" height="10"><SPAN class="diagramTableSpace"><IMG height="1" src="[:@WEBSERVER]images/spacer.gif" width="1"></SPAN></TD>
    </TR>
    <TR>
        <TD align="middle">
            <TABLE class="chartTable2" cellSpacing="0" cellPadding="2" width="580" border="0">
            <!--<TR>
                <TD width="4" bgColor="#acbfeb">&nbsp;</TD>
                <TD align="left" bgColor="#acbfeb">&nbsp;</TD>
                <TD align="right" width="55" bgColor="#acbfeb">&nbsp;</TD>
                <TD align="right" width="55" bgColor="#acbfeb">&nbsp;</TD>
                <TD align="right" width="55" bgColor="#acbfeb">&nbsp;</TD>
                <TD width="4" bgColor="#acbfeb">&nbsp;</TD>
            </TR>-->
            `

            // Ratio Heading
            // ---------------
            FLOAT_FORMAT "1000";
            INHLCOUNT := 0;
            WHILE(INHLCOUNT < INHNUMRECS)
            {
                YCOUNT := INHYCOUNT; LCOUNT := INHLCOUNT;
                IF(STRCMPI([:+CRatioType:Y?:L?],"YEARS")==0)
                {
                    `
                    <TR>
                        <TD class="dbRowElabel" width="4" bgColor="#acbfeb">&nbsp;</TD>
                        <TD class="dbRowElabel" align="left" bgColor="#acbfeb"><B>Ratio</B><BR></TD>
                        `
                        COLUMN_COUNT := 0;
                        WHILE(COLUMN_COUNT < MAX_COLUMNS)
                        {
                            IF     (COLUMN_COUNT==0){RESULT := RUNFORM([:@TEMP: = ":+CMedianForYear0:Y?:L?:"]);}
                            ELSE IF(COLUMN_COUNT==1){RESULT := RUNFORM([:@TEMP: = ":+CMedianForYear1:Y?:L?:"]);}
                            ELSE IF(COLUMN_COUNT==2){RESULT := RUNFORM([:@TEMP: = ":+CMedianForYear2:Y?:L?:"]);}
                            `
                            <TD class="dbRowElabel" align="right" width="55" bgColor="#acbfeb"><B>`
                                IF(FLOATCMP([:@TEMP],"0")!=0) { `[:@TEMP:FFLOAT]`} ELSE { `-` } `</B></TD>`
                            INC COLUMN_COUNT;
                        }
                        `
                        <TD class="dbRowElabel" width="4" bgColor="#acbfeb">&nbsp;</TD>
                    </TR>
                    `
                }
                INC INHLCOUNT;
            }
            FLOAT_FORMAT "1000.00";

            // No. of enterprises covered
            // --------------------------
            FLOAT_FORMAT "1000";
            INHLCOUNT := 0;
            WHILE(INHLCOUNT < INHNUMRECS)
            {
                YCOUNT := INHYCOUNT; LCOUNT := INHLCOUNT;
                IF(STRCMPI([:+CRatioType:Y?:L?],"NUMENT")==0)
                {
                    `
                    <TR>
                        <TD class="dbRowAlabel" width="4">&nbsp;</TD>
                        <TD class="dbRowAlabel" align="left"><B>No. of enterprises covered</B></TD>
                        `
                        COLUMN_COUNT := 0;
                        WHILE(COLUMN_COUNT < MAX_COLUMNS)
                        {
                            IF     (COLUMN_COUNT==0){RESULT := RUNFORM([:@TEMP: = ":+CMedianForYear0:Y?:L?:"]);}
                            ELSE IF(COLUMN_COUNT==1){RESULT := RUNFORM([:@TEMP: = ":+CMedianForYear1:Y?:L?:"]);}
                            ELSE IF(COLUMN_COUNT==2){RESULT := RUNFORM([:@TEMP: = ":+CMedianForYear2:Y?:L?:"]);}
                            `
                            <TD class="dbRowAresults" align="right"><B>`
                                IF(FLOATCMP([:@TEMP],"0")!=0) { `[:@TEMP:FFLOAT]`} ELSE { `-` } `</B></TD>`
                            INC COLUMN_COUNT;
                        }
                        `
                        <TD class="dbRowAresults" width="4">&nbsp;</TD>
                    </TR>
                    `
                }
                INC INHLCOUNT;
            }
            FLOAT_FORMAT "1000.00";

            // Current Ratio
            // --------------------------
            INHLCOUNT := 0;
            WHILE(INHLCOUNT < INHNUMRECS)
            {
                YCOUNT := INHYCOUNT; LCOUNT := INHLCOUNT;
                IF(STRCMPI([:+CRatioType:Y?:L?],"CRRT")==0)
                {
                    `
                    <TR>
                        <TD class="dbRowAlabel" width="4">&nbsp;</TD>
                        <TD class="dbRowAlabel" align="left"><B>Current Ratio</B><BR>Total Current Assets/Total Current Liabilities</TD>
                        `
                        COLUMN_COUNT := 0;
                        WHILE(COLUMN_COUNT < MAX_COLUMNS)
                        {
                            IF     (COLUMN_COUNT==0){RESULT := RUNFORM([:@TEMP: = ":+CMedianForYear0:Y?:L?:"]);}
                            ELSE IF(COLUMN_COUNT==1){RESULT := RUNFORM([:@TEMP: = ":+CMedianForYear1:Y?:L?:"]);}
                            ELSE IF(COLUMN_COUNT==2){RESULT := RUNFORM([:@TEMP: = ":+CMedianForYear2:Y?:L?:"]);}
                            `
                            <TD class="dbRowAresults" align="right"><B>`
                                IF(FLOATCMP([:@TEMP],"0")!=0) { `[:@TEMP:FFLOAT]`} ELSE { `-` } `</B></TD>`
                            INC COLUMN_COUNT;
                        }
                        `
                        <TD class="dbRowAresults" width="4">&nbsp;</TD>
                    </TR>
                    `
                }
                INC INHLCOUNT;
            }

            // Quick Ratio
            // --------------------------
            INHLCOUNT := 0;
            WHILE(INHLCOUNT < INHNUMRECS)
            {
                YCOUNT := INHYCOUNT; LCOUNT := INHLCOUNT;
                IF(STRCMPI([:+CRatioType:Y?:L?],"QKRT")==0)
                {
                    `
                    <TR>
                        <TD class="dbRowAlabel" width="4">&nbsp;</TD>
                        <TD class="dbRowAlabel" align="left"><B>Quick Ratio</B><BR>(Total Current Assets - Total Inventory)/Total Current Liabilities</TD>
                        `
                        COLUMN_COUNT := 0;
                        WHILE(COLUMN_COUNT < MAX_COLUMNS)
                        {
                            IF     (COLUMN_COUNT==0){RESULT := RUNFORM([:@TEMP: = ":+CMedianForYear0:Y?:L?:"]);}
                            ELSE IF(COLUMN_COUNT==1){RESULT := RUNFORM([:@TEMP: = ":+CMedianForYear1:Y?:L?:"]);}
                            ELSE IF(COLUMN_COUNT==2){RESULT := RUNFORM([:@TEMP: = ":+CMedianForYear2:Y?:L?:"]);}
                            `
                            <TD class="dbRowAresults" align="right"><B>`
                                IF(FLOATCMP([:@TEMP],"0")!=0) { `[:@TEMP:FFLOAT]`} ELSE { `-` } `</B></TD>`
                            INC COLUMN_COUNT;
                        }
                        `
                        <TD class="dbRowAresults" width="4">&nbsp;</TD>
                    </TR>
                    `
                }
                INC INHLCOUNT;
            }

            // Debt/Equity Ratio
            // --------------------------
            INHLCOUNT := 0;
            WHILE(INHLCOUNT < INHNUMRECS)
            {
                YCOUNT := INHYCOUNT; LCOUNT := INHLCOUNT;
                IF(STRCMPI([:+CRatioType:Y?:L?],"TERT")==0)
                {
                    `
                    <TR>
                        <TD class="dbRowAlabel" width="4">&nbsp;</TD>
                        <TD class="dbRowAlabel" align="left"><B>Debt/Equity Ratio</B><BR>Total Liabilities/Total Equity</TD>
                        `
                        COLUMN_COUNT := 0;
                        WHILE(COLUMN_COUNT < MAX_COLUMNS)
                        {
                            IF     (COLUMN_COUNT==0){RESULT := RUNFORM([:@TEMP: = ":+CMedianForYear0:Y?:L?:"]);}
                            ELSE IF(COLUMN_COUNT==1){RESULT := RUNFORM([:@TEMP: = ":+CMedianForYear1:Y?:L?:"]);}
                            ELSE IF(COLUMN_COUNT==2){RESULT := RUNFORM([:@TEMP: = ":+CMedianForYear2:Y?:L?:"]);}
                            `
                            <TD class="dbRowAresults" align="right"><B>`
                                IF(FLOATCMP([:@TEMP],"0")!=0) { `[:@TEMP:FFLOAT]`} ELSE { `-` } `</B></TD>`
                            INC COLUMN_COUNT;
                        }
                        `
                        <TD class="dbRowAresults" width="4">&nbsp;</TD>
                    </TR>
                    `
                }
                INC INHLCOUNT;
            }
            // Debt Ratio
            // --------------------------
            INHLCOUNT := 0;
            WHILE(INHLCOUNT < INHNUMRECS)
            {
                YCOUNT := INHYCOUNT; LCOUNT := INHLCOUNT;
                IF(STRCMPI([:+CRatioType:Y?:L?],"DTRT")==0)
                {
                    `
                    <TR>
                        <TD class="dbRowAlabel" width="4">&nbsp;</TD>
                        <TD class="dbRowAlabel" align="left"><B>Debt Ratio</B><BR>Total Liabilities/Total Assets</TD>
                        `
                        COLUMN_COUNT := 0;
                        WHILE(COLUMN_COUNT < MAX_COLUMNS)
                        {
                            IF     (COLUMN_COUNT==0){RESULT := RUNFORM([:@TEMP: = ":+CMedianForYear0:Y?:L?:"]);}
                            ELSE IF(COLUMN_COUNT==1){RESULT := RUNFORM([:@TEMP: = ":+CMedianForYear1:Y?:L?:"]);}
                            ELSE IF(COLUMN_COUNT==2){RESULT := RUNFORM([:@TEMP: = ":+CMedianForYear2:Y?:L?:"]);}
                            `
                            <TD class="dbRowAresults" align="right"><B>`
                                IF(FLOATCMP([:@TEMP],"0")!=0) { `[:@TEMP:FFLOAT]`} ELSE { `-` } `</B></TD>`
                            INC COLUMN_COUNT;
                        }
                        `
                        <TD class="dbRowAresults" width="4">&nbsp;</TD>
                    </TR>
                    `
                }
                INC INHLCOUNT;
            }
            // Collection Period (Days)
            // --------------------------
            INHLCOUNT := 0;
            WHILE(INHLCOUNT < INHNUMRECS)
            {
                YCOUNT := INHYCOUNT; LCOUNT := INHLCOUNT;
                IF(STRCMPI([:+CRatioType:Y?:L?],"CLPR")==0)
                {
                    `
                    <TR>
                        <TD class="dbRowAlabel" width="4">&nbsp;</TD>
                        <TD class="dbRowAlabel" align="left"><B>Collection Period (Days)</B><BR>Accounts Receivable/Sales x 360</TD>
                        `
                        COLUMN_COUNT := 0;
                        WHILE(COLUMN_COUNT < MAX_COLUMNS)
                        {
                            IF     (COLUMN_COUNT==0){RESULT := RUNFORM([:@TEMP: = ":+CMedianForYear0:Y?:L?:"]);}
                            ELSE IF(COLUMN_COUNT==1){RESULT := RUNFORM([:@TEMP: = ":+CMedianForYear1:Y?:L?:"]);}
                            ELSE IF(COLUMN_COUNT==2){RESULT := RUNFORM([:@TEMP: = ":+CMedianForYear2:Y?:L?:"]);}
                            `
                            <TD class="dbRowAresults" align="right"><B>`
                                IF(FLOATCMP([:@TEMP],"0")!=0) { `[:@TEMP:FFLOAT]`} ELSE { `-` } `</B></TD>`
                            INC COLUMN_COUNT;
                        }
                        `
                        <TD class="dbRowAresults" width="4">&nbsp;</TD>
                    </TR>
                    `
                }
                INC INHLCOUNT;
            }
            // Payment Period (Days)
            // --------------------------
            INHLCOUNT := 0;
            WHILE(INHLCOUNT < INHNUMRECS)
            {
                YCOUNT := INHYCOUNT; LCOUNT := INHLCOUNT;
                IF(STRCMPI([:+CRatioType:Y?:L?],"PYPR")==0)
                {
                    `
                    <TR>
                        <TD class="dbRowAlabel" width="4">&nbsp;</TD>
                        <TD class="dbRowAlabel" align="left"><B>Payment Period (Days)</B><BR>Accounts Payable/Cost of Sales x 360</TD>
                        `
                        COLUMN_COUNT := 0;
                        WHILE(COLUMN_COUNT < MAX_COLUMNS)
                        {
                            IF     (COLUMN_COUNT==0){RESULT := RUNFORM([:@TEMP: = ":+CMedianForYear0:Y?:L?:"]);}
                            ELSE IF(COLUMN_COUNT==1){RESULT := RUNFORM([:@TEMP: = ":+CMedianForYear1:Y?:L?:"]);}
                            ELSE IF(COLUMN_COUNT==2){RESULT := RUNFORM([:@TEMP: = ":+CMedianForYear2:Y?:L?:"]);}
                            `
                            <TD class="dbRowAresults" align="right"><B>`
                                IF(FLOATCMP([:@TEMP],"0")!=0) { `[:@TEMP:FFLOAT]`} ELSE { `-` } `</B></TD>`
                            INC COLUMN_COUNT;
                        }
                        `
                        <TD class="dbRowAresults" width="4">&nbsp;</TD>
                    </TR>
                    `
                }
                INC INHLCOUNT;
            }
            // Inventory Turnover (Days)
            // --------------------------
            INHLCOUNT := 0;
            WHILE(INHLCOUNT < INHNUMRECS)
            {
                YCOUNT := INHYCOUNT; LCOUNT := INHLCOUNT;
                IF(STRCMPI([:+CRatioType:Y?:L?],"IVTN")==0)
                {
                    `
                    <TR>
                        <TD class="dbRowAlabel" width="4">&nbsp;</TD>
                        <TD class="dbRowAlabel" align="left"><B>Inventory Turnover (Days)</B><BR>Total Inventory/Cost of Sales x 360</TD>
                        `
                        COLUMN_COUNT := 0;
                        WHILE(COLUMN_COUNT < MAX_COLUMNS)
                        {
                            IF     (COLUMN_COUNT==0){RESULT := RUNFORM([:@TEMP: = ":+CMedianForYear0:Y?:L?:"]);}
                            ELSE IF(COLUMN_COUNT==1){RESULT := RUNFORM([:@TEMP: = ":+CMedianForYear1:Y?:L?:"]);}
                            ELSE IF(COLUMN_COUNT==2){RESULT := RUNFORM([:@TEMP: = ":+CMedianForYear2:Y?:L?:"]);}
                            `
                            <TD class="dbRowAresults" align="right"><B>`
                                IF(FLOATCMP([:@TEMP],"0")!=0) { `[:@TEMP:FFLOAT]`} ELSE { `-` } `</B></TD>`
                            INC COLUMN_COUNT;
                        }
                        `
                        <TD class="dbRowAresults" width="4">&nbsp;</TD>
                    </TR>
                    `
                }
                INC INHLCOUNT;
            }
            // Asset Turnover
            // --------------------------
            INHLCOUNT := 0;
            WHILE(INHLCOUNT < INHNUMRECS)
            {
                YCOUNT := INHYCOUNT; LCOUNT := INHLCOUNT;
                IF(STRCMPI([:+CRatioType:Y?:L?],"ASTN")==0)
                {
                    `
                    <TR>
                        <TD class="dbRowAlabel" width="4">&nbsp;</TD>
                        <TD class="dbRowAlabel" align="left"><B>Asset Turnover</B><BR>Net Sales/Total Assets</TD>
                        `
                        COLUMN_COUNT := 0;
                        WHILE(COLUMN_COUNT < MAX_COLUMNS)
                        {
                            IF     (COLUMN_COUNT==0){RESULT := RUNFORM([:@TEMP: = ":+CMedianForYear0:Y?:L?:"]);}
                            ELSE IF(COLUMN_COUNT==1){RESULT := RUNFORM([:@TEMP: = ":+CMedianForYear1:Y?:L?:"]);}
                            ELSE IF(COLUMN_COUNT==2){RESULT := RUNFORM([:@TEMP: = ":+CMedianForYear2:Y?:L?:"]);}
                            `
                            <TD class="dbRowAresults" align="right"><B>`
                                IF(FLOATCMP([:@TEMP],"0")!=0) { `[:@TEMP:FFLOAT]`} ELSE { `-` } `</B></TD>`
                            INC COLUMN_COUNT;
                        }
                        `
                        <TD class="dbRowAresults" width="4">&nbsp;</TD>
                    </TR>
                    `
                }
                INC INHLCOUNT;
            }
            // Gross Margin %
            // --------------------------
            //INHLCOUNT := 0;
            //WHILE(INHLCOUNT < INHNUMRECS)
            //{
            //    YCOUNT := INHYCOUNT; LCOUNT := INHLCOUNT;
            //    IF(STRCMPI([:+CRatioType:Y?:L?],"GRMG")==0)
            //    {
            //        `
            //        <TR>
            //            <TD class="dbRowAlabel" width="4">&nbsp;</TD>
            //            <TD class="dbRowAlabel" align="left"><B>Gross Margin %</B><BR>Gross Profit on Sales/Sales x 100%</TD>
            //            `
            //            COLUMN_COUNT := 0;
            //            WHILE(COLUMN_COUNT < MAX_COLUMNS)
            //            {
            //                IF     (COLUMN_COUNT==0){RESULT := RUNFORM([:@TEMP: = ":+CMedianForYear0:Y?:L?:"]);}
            //                ELSE IF(COLUMN_COUNT==1){RESULT := RUNFORM([:@TEMP: = ":+CMedianForYear1:Y?:L?:"]);}
            //                ELSE IF(COLUMN_COUNT==2){RESULT := RUNFORM([:@TEMP: = ":+CMedianForYear2:Y?:L?:"]);}
            //                `
            //                <TD class="dbRowAresults" align="right"><B>`
            //                    IF(FLOATCMP([:@TEMP],"0")!=0) { `[:@TEMP:FFLOAT]`} ELSE { `-` } `</B></TD>`
            //                INC COLUMN_COUNT;
            //            }
            //            `
            //            <TD class="dbRowAresults" width="4">&nbsp;</TD>
            //        </TR>
            //        `
            //    }
            //    INC INHLCOUNT;
            //}
            // Net Profit Margin %
            // --------------------------
            INHLCOUNT := 0;
            WHILE(INHLCOUNT < INHNUMRECS)
            {
                YCOUNT := INHYCOUNT; LCOUNT := INHLCOUNT;
                IF(STRCMPI([:+CRatioType:Y?:L?],"NPMG")==0)
                {
                    `
                    <TR>
                        <TD class="dbRowAlabel" width="4">&nbsp;</TD>
                        <TD class="dbRowAlabel" align="left"><B>Net Profit Margin %</B><BR>Net Profit(Loss)/Sales x 100%</TD>
                        `
                        COLUMN_COUNT := 0;
                        WHILE(COLUMN_COUNT < MAX_COLUMNS)
                        {
                            IF     (COLUMN_COUNT==0){RESULT := RUNFORM([:@TEMP: = ":+CMedianForYear0:Y?:L?:"]);}
                            ELSE IF(COLUMN_COUNT==1){RESULT := RUNFORM([:@TEMP: = ":+CMedianForYear1:Y?:L?:"]);}
                            ELSE IF(COLUMN_COUNT==2){RESULT := RUNFORM([:@TEMP: = ":+CMedianForYear2:Y?:L?:"]);}
                            `
                            <TD class="dbRowAresults" align="right"><B>`
                                IF(FLOATCMP([:@TEMP],"0")!=0) { `[:@TEMP:FFLOAT]`} ELSE { `-` } `</B></TD>`
                            INC COLUMN_COUNT;
                        }
                        `
                        <TD class="dbRowAresults" width="4">&nbsp;</TD>
                    </TR>
                    `
                }
                INC INHLCOUNT;
            }
            // Return On Equity %
            // --------------------------
            INHLCOUNT := 0;
            WHILE(INHLCOUNT < INHNUMRECS)
            {
                YCOUNT := INHYCOUNT; LCOUNT := INHLCOUNT;
                IF(STRCMPI([:+CRatioType:Y?:L?],"RTEQ")==0)
                {
                    `
                    <TR>
                        <TD class="dbRowAlabel" width="4">&nbsp;</TD>
                        <TD class="dbRowAlabel" align="left"><B>Return On Equity %</B><BR>Net Profit(Loss)/Total Equity x 100%</TD>
                        `
                        COLUMN_COUNT := 0;
                        WHILE(COLUMN_COUNT < MAX_COLUMNS)
                        {
                            IF     (COLUMN_COUNT==0){RESULT := RUNFORM([:@TEMP: = ":+CMedianForYear0:Y?:L?:"]);}
                            ELSE IF(COLUMN_COUNT==1){RESULT := RUNFORM([:@TEMP: = ":+CMedianForYear1:Y?:L?:"]);}
                            ELSE IF(COLUMN_COUNT==2){RESULT := RUNFORM([:@TEMP: = ":+CMedianForYear2:Y?:L?:"]);}
                            `
                            <TD class="dbRowAresults" align="right"><B>`
                                IF(FLOATCMP([:@TEMP],"0")!=0) { `[:@TEMP:FFLOAT]`} ELSE { `-` } `</B></TD>`
                            INC COLUMN_COUNT;
                        }
                        `
                        <TD class="dbRowAresults" width="4">&nbsp;</TD>
                    </TR>
                    `
                }
                INC INHLCOUNT;
            }
            // Return On Assets %
            // --------------------------
            INHLCOUNT := 0;
            WHILE(INHLCOUNT < INHNUMRECS)
            {
                YCOUNT := INHYCOUNT; LCOUNT := INHLCOUNT;
                IF(STRCMPI([:+CRatioType:Y?:L?],"RTAS")==0)
                {
                    `
                    <TR>
                        <TD class="dbRowAlabel" width="4">&nbsp;</TD>
                        <TD class="dbRowAlabel" align="left"><B>Return On Assets %</B><BR>Net Profit(Loss)/Total Assets x 100%</TD>
                        `
                        COLUMN_COUNT := 0;
                        WHILE(COLUMN_COUNT < MAX_COLUMNS)
                        {
                            IF     (COLUMN_COUNT==0){RESULT := RUNFORM([:@TEMP: = ":+CMedianForYear0:Y?:L?:"]);}
                            ELSE IF(COLUMN_COUNT==1){RESULT := RUNFORM([:@TEMP: = ":+CMedianForYear1:Y?:L?:"]);}
                            ELSE IF(COLUMN_COUNT==2){RESULT := RUNFORM([:@TEMP: = ":+CMedianForYear2:Y?:L?:"]);}
                            `
                            <TD class="dbRowAresults" align="right"><B>`
                                IF(FLOATCMP([:@TEMP],"0")!=0) { `[:@TEMP:FFLOAT]`} ELSE { `-` } `</B></TD>`
                            INC COLUMN_COUNT;
                        }
                        `
                        <TD class="dbRowAresults" width="4">&nbsp;</TD>
                    </TR>
                    `
                }
                INC INHLCOUNT;
            }
            `
            </TABLE>
        </TD>
    </TR>
    <TR>
        <TD height="10"><IMG height="1" src="[:@WEBSERVER]images/spacer.gif" width="1"></TD>
    </TR>
    </TABLE>
    <P></P>
    <LI>Above industry financial norms were derived by calculating the data in HDBC's database.
    `
}

IF(INHYCOUNT > -1) { YCOUNT := INHYCOUNT; INHYCOUNT := -1; YCOUNT := UNLOADREL([:Y?]);}

