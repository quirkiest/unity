11        6      32         2  ENG   CHI
0        EN     Net
0        CH     记帐
1        EN     COD
1        CH     货到收现
2        EN     Open A/c
2        CH     记帐
3        EN     L/C
3        CH     信用状
4        EN     T/T
4        CH     电汇
5        EN     D/A
5        CH     银行承兑交单
6        EN     D/P
6        CH     银行付款交单
7        EN     Special
7        CH     特殊
8        EN     % Deposit
8        CH     部份预付
9        EN     Consignm.
9        CH     寄售
10       EN     Inter-co.
10       CH     关人交易
11       EN     CR Card
11       CH     信用卡
12       EN     Advance
12       CH     预付
13       EN     EOM
13       CH     月结
14       EN     Credit 
14       CH     Credit 
15       EN     Partial
15       CH     部份付款
16       EN     Upon Completion
16       CH     完工时付款
17       EN     Cheque
17       CH     支票
18       EN     Draft
18       CH     Draft
19       EN     Cash
19       CH     现金
20       EN     Check
20       CH     Check
