6     6    60  2  ENG   CHI
1     EN    10) a state-owned enterprise
1     CH    国营企业
2     EN    7) a jointly owned enterprise
2     CH    联营企业
4     EN    3) a collectively owned enterprise
4     CH    集体企业
5     EN    8) a limited company
5     CH    股份制企业
6     EN    9) a private owned enterprise
6     CH    私营企业
7     EN    2) a Chinese foreign joint venture
7     CH    中外合资经营企业
8     EN    1) a Chinese foreign cooperative venture
8     CH    中外合作经营企业
9     EN    4) a government dept
9     CH    国家机关
11    EN    13) an enterprise processing provided samples
11    CH    三来一补企业
12    EN    11) a wholly foreign-owned enterprise
12    CH    外国独资企业
13    EN    5) a holding limited company
13    CH    股份有限公司
14    EN    6) a joint stock
14    CH    联营公司
15    EN    12) an administrative organization
15    CH    管理部门
16    EN    15) Not registered
16    CH    Not registered
17    EN    14) Missing
17    CH    没有资料
18    EN    16) State Owned Institution
18    CH    事业单位  