//****************************************************************************
//  sbcFB_T.cap - SuperBIR CHinese. HTML Finance & Banking - Title
//****************************************************************************
DBSECTION "finstmtb";
LANGUAGE "CHI";
LINE_LENGTH 1200;
NOTFILL;
DECLARE RESULT;
DECLARE NUMSECTIONS;
DECLARE NUMLOOPS;
DECLARE THOUSANDS; THOUSANDS := 0;
DECLARE MILLIONS;  MILLIONS  := 0;

DECLARE MAX_SECTIONS;      MAX_SECTIONS    := 3;
DECLARE FINANCE_FH_EXISTS; FINANCE_FH_EXISTS  := 0;
DECLARE FINANCE_BS_EXISTS; FINANCE_BS_EXISTS  := 0;
DECLARE FINANCE_FAN_EXISTS;FINANCE_FAN_EXISTS := 0;
DECLARE BANKING_EXISTS;    BANKING_EXISTS  := 0;

DECLARE FIN_STATEMENT_EXISTS;
FIN_STATEMENT_EXISTS := (ATOI([:wx:M0:B*])==1);

DECLARE JULIAN_THREE_YEARSAGO;
JULIAN_THREE_YEARSAGO := ATOI([::general:TD:FJULIANDATE]) - 1095;
DECLARE JULIAN_FOUR_YEARSAGO;
JULIAN_FOUR_YEARSAGO := ATOI([::general:TD:FJULIANDATE]) - 1460;

DECLARE NUMSECTIONS;    NUMSECTIONS := 0;
DECLARE TOTAL_MC;       TOTAL_MC    := 0;
DECLARE MC;             MC          := -1;
DECLARE PAD_MCOUNT;     PAD_MCOUNT  := 0;

DECLARE VALIDSECTION;   VALIDSECTION := 0;
DECLARE PREV_STATEMENT_YEAR;
DECLARE THIS_STATEMENT_YEAR;
DECLARE HASDATA;

DECLARE SICLCOUNT;

DECLARE INHNUMRECS;             INHNUMRECS  := 0;
DECLARE INHYCOUNT;              INHYCOUNT   := -1;
DECLARE INHLCOUNT;              INHLCOUNT   := 0;
DECLARE FINANCE_IFN_EXISTS;     FINANCE_IFN_EXISTS := 0;


// Get Financial Highlights MCOUNT Array
// ----------------------------------------

// Initialise [:@FIN_FH_MCOUNT]
// -----------------------------------------
MC := 0;
WHILE(MC < MAX_SECTIONS)
{
    LCOUNT := MC; RESULT := RUNFORM([:@FIN_FH_MCOUNT:L?: = "-1"]);
    INC MC;
}

IF(FIN_STATEMENT_EXISTS)
{
    // Check Fin.Statements are "In-Date"
    // -----------------------------------------
    DBSECTION "Finstmtb";
    NUMSECTIONS := SUPER([::finstmtb]);
    TOTAL_MC := 0;
    MC       := 0;
    WHILE(MC < NUMSECTIONS)
    {
        // Is the YearEnd date is within 4 years ?
        // -----------------------------------------
        MCOUNT := MC;
        VALIDSECTION := (ATOI([:ib:M?:B*:FJULIANDATE]) > JULIAN_FOUR_YEARSAGO);
        IF(VALIDSECTION)
        {
            IF(MC > 0)
            {
                // Is the Prev.YearEnd date 1 Year ago ?
                // -----------------------------------------
                MCOUNT := MC-1; PREV_STATEMENT_YEAR := ATOI([:ib:M?:B*:FDATEYYYY]);
                MCOUNT := MC;   THIS_STATEMENT_YEAR := ATOI([:ib:M?:B*:FDATEYYYY]);
                VALIDSECTION := ((PREV_STATEMENT_YEAR - THIS_STATEMENT_YEAR)==1);
            }
        }
        IF(VALIDSECTION)
        {
            // Check current Section count
            // ---------------------------
            IF(TOTAL_MC < MAX_SECTIONS)
            {
                // Store this MC
                // ----------------------
                VCOUNT := MC; LCOUNT := TOTAL_MC; INC TOTAL_MC;
                RESULT := RUNFORM([:@FIN_FH_MCOUNT:L?: = ":#:VAR:V?:FSTRIP"]);

                // Store Mult.Factor
                // -----------------------
                THOUSANDS := (ATOI([:ai:M?:B*])==1); MILLIONS := (ATOI([:ai:M?:B*])==2);
                IF     (THOUSANDS)  { RESULT := RUNFORM([:@FIN_FH_FACTOR:L?: = "1000"]);    }
                ELSE IF(MILLIONS)   { RESULT := RUNFORM([:@FIN_FH_FACTOR:L?: = "1000000"]); }
                ELSE                { RESULT := RUNFORM([:@FIN_FH_FACTOR:L?: = "1"]);       }
            }
            ELSE
            {
                // Exit
                // ----------------------
                MC := NUMSECTIONS;
            }
        }
        INC MC;
    }
}

// Get Financial Balance Sheet MCOUNT Array
// ----------------------------------------

// Initialise [:@FIN_BS_MCOUNT]
// -----------------------------------------
MC := 0;
WHILE(MC < MAX_SECTIONS)
{
    LCOUNT := MC; RESULT := RUNFORM([:@FIN_BS_MCOUNT:L?: = "-1"]);
    INC MC;
}

IF(FIN_STATEMENT_EXISTS)
{
    // Check Fin.Statements are "In-Date"
    // -----------------------------------------
    DBSECTION "Finstmtb";
    NUMSECTIONS := SUPER([::finstmtb]);
    TOTAL_MC := 0;
    MC       := 0;
    WHILE(MC < NUMSECTIONS)
    {
        // Is the YearEnd date is within 4 years ?
        // -----------------------------------------
        MCOUNT := MC;
        VALIDSECTION := (ATOI([:xw:M?:B*:FJULIANDATE]) > JULIAN_FOUR_YEARSAGO);
        IF(VALIDSECTION)
        {
            IF(MC > 0)
            {
                // Is the Prev.YearEnd date 1 Year ago ?
                // -----------------------------------------
                MCOUNT := MC-1; PREV_STATEMENT_YEAR := ATOI([:xw:M?:B*:FDATEYYYY]);
                MCOUNT := MC;   THIS_STATEMENT_YEAR := ATOI([:xw:M?:B*:FDATEYYYY]);
                VALIDSECTION := ((PREV_STATEMENT_YEAR - THIS_STATEMENT_YEAR)==1);
            }
        }
        IF(VALIDSECTION)
        {
            // Check current Section count
            // ---------------------------
            IF(TOTAL_MC < MAX_SECTIONS)
            {
                // Store this MC
                // ----------------------
                VCOUNT := MC; LCOUNT := TOTAL_MC; INC TOTAL_MC;
                RESULT := RUNFORM([:@FIN_BS_MCOUNT:L?: = ":#:VAR:V?:FSTRIP"]);

                // Store Mult.Factor
                // -----------------------
                THOUSANDS := (ATOI([:ai:M?:B*])==1); MILLIONS := (ATOI([:ai:M?:B*])==2);
                IF     (THOUSANDS)  { RESULT := RUNFORM([:@FIN_BS_FACTOR:L?: = "1000"]);    }
                ELSE IF(MILLIONS)   { RESULT := RUNFORM([:@FIN_BS_FACTOR:L?: = "1000000"]); }
                ELSE                { RESULT := RUNFORM([:@FIN_BS_FACTOR:L?: = "1"]);       }
            }
            ELSE
            {
                // Exit
                // ----------------------
                MC := NUMSECTIONS;
            }
        }
        INC MC;
    }
}

// Check Industry Financial Norms
// ----------------------------------------

// Initialise
// ---------------------
RESULT := RUNFORM([:@SIC: = ""]);

// Check the SIC codes
// ---------------------
IF(EXISTS([::operatio:s1:L0]))
{
    SICLCOUNT  := 0;
    NUMLOOPS   := LOOPS([::operatio:s1]);
    WHILE(SICLCOUNT < NUMLOOPS)
    {
        // Look-up the SIC codes
        // ---------------------
        LCOUNT := SICLCOUNT;
        IF(EXISTS([::operatio:s1:L?:B*]))
        {
            RESULT := RUNFORM([:@SIC: = "::operatio:s1:L?:B*:S4:FSTRIP"]);

            // See if this SIC code exists on Industry_Norm_Hist
            // --------------------------------------------------
            INHYCOUNT  := LOADREL([:+DCSD>SP>usp_GetIndustryFinancialNorms:K~SIC = :@SIC:~:CRatioType]);
            YCOUNT     := INHYCOUNT; INHNUMRECS := COUNTREL([:Y?]);
            IF(INHNUMRECS > 0)
            {
                FINANCE_IFN_EXISTS  := 1;
                SICLCOUNT           := NUMLOOPS;
            }
            YCOUNT := INHYCOUNT; INHYCOUNT := -1; YCOUNT := UNLOADREL([:Y?]);
        }
        INC SICLCOUNT;
    }
}


// Check Finance Highlights
// -------------------------------------
FINANCE_FH_EXISTS := (ATOI([:@FIN_FH_MCOUNT:L0])>=0);

// Check Finance Balance Sheet
// -------------------------------------
FINANCE_BS_EXISTS := (ATOI([:@FIN_BS_MCOUNT:L0])>=0);


// Check Financial Analysis
// -------------------------------------
FINANCE_FAN_EXISTS := ((EXISTS([:ac:M0:B*]))+(EXISTS([:a0:M0:B*]))==2);


// Check Banking
// ---------------
BANKING_EXISTS  := (((EXISTS([::banks:ac:M0:B*])) +
                    ((EXISTS([::banks:ra:M0:B*])) + (EXISTS([::banks:BA:M0:B*])) > 0) == 2) > 0);


IF((FINANCE_FH_EXISTS)+(FINANCE_BS_EXISTS)+(FINANCE_FAN_EXISTS)+(FINANCE_IFN_EXISTS)+(BANKING_EXISTS))
{
    // Finance And Banking
    // ----------------------

    // Finish Off the Previous Section Before starting This ONE
    // -----------------------------------------------------------
    `
        <DIV class="toTop"><A class="arrowNav" href="#top"><IMG height="6" alt="top" src="[:@WEBSERVER]images/arrow-up-single.gif" width="9" border="0"></A></DIV>
        </TD>
        <TD class=rightMargin align=left>&nbsp;</TD>
    </TR>
    </TABLE>
    </DIV>

    <!-- 08 Finance And Banking-->
    <DIV align=center id="8">
    <DIV class="pagebreakhere"></DIV>

    <DIV class="NOPRINT">
        <TABLE border="0" cellpadding="0" cellspacing="0" class="mainTableNew">
        <TR>
            <TD height=35  class="mainMargin1">&nbsp;</TD>
            <TD bgcolor="#C8D7F0" class="mainCol2newBlue">&nbsp;</TD>
            <TD  width="300" align="left" valign="top" bgcolor="#C8D7F0"><H2>财务与往来银行</H2></TD>
            <TD align="right" valign="top" bgcolor="#C8D7F0">
                <TABLE border="0" cellspacing="0" cellpadding="0">
                <TR>
                    <TD width="22" height=24 align="left" valign="bottom">
                         <!-- **************************************************
                             DO NOT BREAK the lines "<A . . .><IMG" OR "></A>"
                             ************************************************** -->

                        <A class="h2nav" href="javascript:doExpand('Finance');"><IMG
                            id="butFinance" onclick="" height="11" alt="Summarise"
                            src="[:@WEBSERVER]images/negative-single-box-11px.gif" width="11" border="0"></A>
                    </TD>
                </TR>
                </TABLE>
            </TD>
            <TD class="rightMargin">&nbsp;</TD>
        </TR>
        <TR>
            <TD height=4><IMG src="[:@WEBSERVER]images/spacer.gif" width="1" height=1></TD>
            <TD align="left" bgcolor="#F5F5F5" class="mainCol2new">
                <IMG src="images/spacer.gif" width="1" height=1></TD>
            <TD align="left" bgcolor="#F5F5F5"><IMG src="[:@WEBSERVER]images/spacer.gif" width="1" height=1></TD>
            <TD align="left" bgcolor="#F5F5F5"><IMG src="[:@WEBSERVER]images/spacer.gif" width="1" height=1></TD>
            <TD><IMG src="images/spacer.gif" width="1" height=1></TD>
        </TR>
        </TABLE>
    </DIV>
    <DIV class="PRINTONLY">
        <TABLE class="mainTableNew" cellSpacing="0" cellPadding="0" border="0">
        <!--<TR>
            <TD class="mainMargin1" width="35" height="20"><IMG height="1" src="[:@WEBSERVER]images/spacer.gif" width="1"></TD>
            <TD class="mainCol2new" align="left"><IMG height="1" src="[:@WEBSERVER]images/spacer.gif" width="1"></TD>
            <TD class="h2startSectionHeightPrint" align="left">&nbsp;</TD>
        </TR>-->
        </TABLE>
        <TABLE class="mainTableNew" cellSpacing="0" cellPadding="0" border="0">
        <TR>
            <TD width="35"  height="35"><IMG height="35" src="[:@WEBSERVER]images/yellow_replace.gif" width="35"></TD>
            <TD align="left"><IMG class="h2ImagePrintSize" src="[:@WEBSERVER]images/head8j.gif"></TD>
        </TR>
        <TR>
            <TD height="4"><IMG height="1" src="[:@WEBSERVER]images/spacer.gif" width="1"></TD>
            <TD class="mainCol2masthead" align="left"><IMG height="4" src="[:@WEBSERVER]images/lightgrey.gif" width="665"></TD>
        </TR>
        </TABLE>
    </DIV>
    <DIV id="Finance" style="DISPLAY: block">

   <TABLE class="mainTableNew" cellSpacing="0" cellPadding="0" border="0">
    <TR>
        <TD class="mainMargin1">&nbsp;</TD>
        <TD class="mainCol2new">&nbsp;</TD>
        <TD vAlign="top" align="left">
        `
}
