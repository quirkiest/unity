//**********************************************************************************
//  sbcFBCFH.cap - SuperBIR Chinese. HTML Finance & Banking - Cash Flow Highlights
//**********************************************************************************
DBSECTION "finstmtb";
LANGUAGE "CHI";
LINE_LENGTH 1200;
NOTFILL;
DECLARE RESULT;
DECLARE NUMSECTIONS;
DECLARE NUMLOOPS;
DECLARE LC;
DECLARE FOUND;
DECLARE THOUSANDS; THOUSANDS := 0;
DECLARE MILLIONS;  MILLIONS  := 0;

DECLARE MAX_SECTIONS;       MAX_SECTIONS        := 3;
DECLARE FINANCE_BS_EXISTS;  FINANCE_BS_EXISTS   := 0;
DECLARE CASHFLOW_EXISTS;    CASHFLOW_EXISTS     := 0;

DECLARE TOTAL_MC;       TOTAL_MC    := 0;
DECLARE MC;             MC          := -1;
DECLARE PAD_MCOUNT;     PAD_MCOUNT  := 0;
DECLARE HASDATA;

DECLARE OUTPUT_SUBTOTAL; OUTPUT_SUBTOTAL := 0;

DECLARE FIN_KEY_LC;
DECLARE FIN_KEY_TOTAL;

// Check Cash Flow Highlights exists
// -------------------------------------
// Get Current Finance Section Numbers
// -------------------------------------
FINANCE_BS_EXISTS := (ATOI([:@FIN_BS_MCOUNT:L0])>=0);

LEFT_MARGIN 9;

IF(FINANCE_BS_EXISTS)
{
    // Finance And Banking
    // ----------------------
    TOTAL_MC :=  0;

    // Get TOTAL Section Count
    // -----------------------
    MC := 0;
    WHILE(MC < MAX_SECTIONS)
    {
        LCOUNT := MC;
        IF(ATOI([:@FIN_BS_MCOUNT:L?])>=0) { INC TOTAL_MC; } ELSE { MC := MAX_SECTIONS; }
        INC MC;
    }
    MC := -1;

    PAD_MCOUNT := MAX_SECTIONS - TOTAL_MC;

    // Cash Flow Highlights
    // -----------------------
    CASHFLOW_EXISTS := 0; MC := 0;
    WHILE(MC < TOTAL_MC)
    {
        LCOUNT:=MC; MCOUNT:=ATOI([:@FIN_BS_MCOUNT:L?]);
        RESULT := RUNFORM([:@AMT: = :aj:M?]); IF(ATOI([:@AMT]) != 0) { CASHFLOW_EXISTS := 1;}
        RESULT := RUNFORM([:@AMT: = :ak:M?]); IF(ATOI([:@AMT]) != 0) { CASHFLOW_EXISTS := 1;}
        RESULT := RUNFORM([:@AMT: = :al:M?]); IF(ATOI([:@AMT]) != 0) { CASHFLOW_EXISTS := 1;}
        RESULT := RUNFORM([:@AMT: = :an:M?]); IF(ATOI([:@AMT]) != 0) { CASHFLOW_EXISTS := 1;}
        IF(CASHFLOW_EXISTS) {MC := TOTAL_MC;}
        INC MC;
    }
}


IF(CASHFLOW_EXISTS)
{
    `<H4>`

    IF(STRCMP([::General:rt:B*],"BHR")==0) { `BRANCH `}
    `现金流量概要`

    `</H4>

    <TABLE class="chartTable2" cellSpacing="0" cellPadding="2" width="630" bgColor="#eff3f7" border="0">
    <TR>
        <TD class="dbRowB" width="4" bgColor="#c8d7f0">&nbsp;</TD>
        <TD class="dbRowB" vAlign="bottom" bgColor="#c8d7f0">&nbsp;</TD>
        `
        MC := 0;
        WHILE(MC < PAD_MCOUNT)
        {
            FILL; `<TD class="dbRowB" align="right" width="86" bgColor="#c8d7f0">&nbsp;<BR>&nbsp;</TD>`
            IF(MC < MAX_SECTIONS-1)
            {
                FILL; `<TD class="dbRowB" align="right" width="86" bgColor="#c8d7f0">&nbsp;<BR>&nbsp;</TD>`
            }
            FILL;
            INC MC;
        }
        MC := 0;
        WHILE(MC < TOTAL_MC)
        {
            LCOUNT:=MC; MCOUNT:=ATOI([:@FIN_BS_MCOUNT:L?]);
            `<TD class="dbRowB" align="right" width="86" bgColor="#c8d7f0"><B>[:xw:M?:B*:FDATEDD/MM/YYYY]</B>
                <BR> [:ps:M?] `
                THOUSANDS := (ATOI([:c8:M?:B*])==1); MILLIONS := (ATOI([:c8:M?:B*])==2);
                IF(THOUSANDS) { `'000` } ELSE IF(MILLIONS){ `'000000` }
                IF(EXISTS([:cs:M?:B*])){ `<BR>[:cs:M?]` }
            `</TD>
            `
            IF(MC < TOTAL_MC-1)
            {
                `<TD class="dbRowB" valign="top" align="right" width="60" bgColor="#c8d7f0"><B>变化<BR>%</B></TD>
                `
            }
            INC MC;
        }
        `
        <TD class="dbRowB" width="4" bgColor="#c8d7f0" height="30">&nbsp;</TD>
    </TR>
    `

    // Net Cash Flow from Operational Activities
    // -----------------------------------------
    HASDATA := 0; MC := 0;
    WHILE(MC < TOTAL_MC)
    {
        LCOUNT:=MC; MCOUNT:=ATOI([:@FIN_BS_MCOUNT:L?]); RESULT := RUNFORM([:@AMOUNT:L?: = ""]);
        RESULT := RUNFORM([:@AMT: = :aj:M?]);
        IF(EXISTS([:@AMT:FSTRIP])) { HASDATA := 1; RESULT := RUNFORM([:@AMOUNT:L?: = ":@AMT:"]);}

        // START Calculate %Change
        // ------------------------
        LCOUNT := MC; RESULT := RUNFORM([:@CHANGE:L?: = "0"]);
        IF(MC > 0)
        {
            FLOAT_FORMAT "1000";
            LCOUNT := MC-1;   RESULT := RUNFORM([:@AMT0: = ":@AMOUNT:L?:" * ":@FIN_BS_FACTOR:L?:"]);
            LCOUNT := MC;     RESULT := RUNFORM([:@AMT1: = ":@AMOUNT:L?:" * ":@FIN_BS_FACTOR:L?:"]);
            IF((EXISTS([:@AMT0]) + (EXISTS([:@AMT1])) + (ATOI([:@AMT1]) != 0))==3)
            {
                RESULT := RUNFORM([:@CHG:  = (:@AMT0 - :@AMT1)/(ABS(:@AMT1)) * "100"]);
                RESULT := RUNFORM([:@TEMP: = (ABS(:@CHG:) <=999)*(ABS(:@CHG:) + 0.005) + "0"]);
                IF(ATOI([:@TEMP]) > 0)
                {
                    IF(STRCMP([:@CHG:S1],"-")==0) {RESULT := RUNFORM([:@CHG: = ":@TEMP:" * "-1"]); }
                    ELSE                          {RESULT := RUNFORM([:@CHG: = ":@TEMP:"]);        }
                    LCOUNT := MC-1; RESULT := RUNFORM([:@CHANGE:L?:  = ":@CHG"]);
                }
            }
            FLOAT_FORMAT "1,000";
        }
        // END Calculate %Change
        // -----------------------

        INC MC;
    }
    IF(HASDATA)
    {
        `
        <TR>
            <TD class="dbRowClabel" width="4">&nbsp;</TD>
            <TD class="dbRowClabel" vAlign="bottom">营运活动产生的净现金流</TD>`

        // Right-Justify column data
        // --------------------------
        MC := 0; WHILE(MC < PAD_MCOUNT) { FILL; `<TD class="dbRowCresults" vAlign="bottom" align="right">&nbsp;</TD>` IF(MC < MAX_SECTIONS-1) { FILL; `<TD class="dbRowCresults" vAlign="bottom" align="right">&nbsp;</TD>` } INC MC; }

        MC := 0;
        WHILE(MC < TOTAL_MC)
        {
            `
            <TD class="dbRowCresults" vAlign="bottom" align="right">`

            LCOUNT:=MC; RESULT := RUNFORM([:@AMT: = ":@AMOUNT:L?:"]);
            IF     (FLOATCMP([:@AMT],"0.0")== 0) {`-`}
            ELSE IF(STRCMP([:@AMT:S1],"-")==0)
            {
                RESULT := RUNFORM([:@TEMP: = ":@AMT:FREMOVE-"]);
                `([:@TEMP:FFLOAT])`
            }
            ELSE {`[:@AMT:FFLOAT]`}
            `</TD>`


            IF(MC < TOTAL_MC-1)
            {
                `
                <TD class="dbRowCresults" vAlign="bottom" align="right">`
                    FLOAT_FORMAT "1,000.00"; IF(FLOATCMP([:@CHANGE:FFLOAT],"0")==0) {`-`} ELSE {`[:@CHANGE:L?:FFLOAT]%`} FLOAT_FORMAT "1,000";
                `</TD>`
            }
            INC MC;
        }
        `
            <TD class="dbRowCresults" width="4">&nbsp;</TD>
        </TR>
        `
        OUTPUT_SUBTOTAL := 0;
    }

    // Net Cash Flow from Investment Activities
    // ------------------------------------------
    HASDATA := 0; MC := 0;
    WHILE(MC < TOTAL_MC)
    {
        LCOUNT:=MC; MCOUNT:=ATOI([:@FIN_BS_MCOUNT:L?]); RESULT := RUNFORM([:@AMOUNT:L?: = ""]);
        RESULT := RUNFORM([:@AMT: = :ak:M?]);
        IF(EXISTS([:@AMT:FSTRIP])) { HASDATA := 1; RESULT := RUNFORM([:@AMOUNT:L?: = ":@AMT:"]);}

        // START Calculate %Change
        // ------------------------
        LCOUNT := MC; RESULT := RUNFORM([:@CHANGE:L?: = "0"]);
        IF(MC > 0)
        {
            FLOAT_FORMAT "1000";
            LCOUNT := MC-1;   RESULT := RUNFORM([:@AMT0: = ":@AMOUNT:L?:" * ":@FIN_BS_FACTOR:L?:"]);
            LCOUNT := MC;     RESULT := RUNFORM([:@AMT1: = ":@AMOUNT:L?:" * ":@FIN_BS_FACTOR:L?:"]);
            IF((EXISTS([:@AMT0]) + (EXISTS([:@AMT1])) + (ATOI([:@AMT1]) != 0))==3)
            {
                RESULT := RUNFORM([:@CHG:  = (:@AMT0 - :@AMT1)/(ABS(:@AMT1)) * "100"]);
                RESULT := RUNFORM([:@TEMP: = (ABS(:@CHG:) <=999)*(ABS(:@CHG:) + 0.005) + "0"]);
                IF(ATOI([:@TEMP]) > 0)
                {
                    IF(STRCMP([:@CHG:S1],"-")==0) {RESULT := RUNFORM([:@CHG: = ":@TEMP:" * "-1"]); }
                    ELSE                          {RESULT := RUNFORM([:@CHG: = ":@TEMP:"]);        }
                    LCOUNT := MC-1; RESULT := RUNFORM([:@CHANGE:L?:  = ":@CHG"]);
                }
            }
            FLOAT_FORMAT "1,000";
        }
        // END Calculate %Change
        // -----------------------

        INC MC;
    }
    IF(HASDATA)
    {
        `
        <TR>
            <TD class="dbRow` IF(OUTPUT_SUBTOTAL){`G`}ELSE{`C`} `label" width="4">&nbsp;</TD>
            <TD class="dbRow` IF(OUTPUT_SUBTOTAL){`G`}ELSE{`C`} `label" vAlign="bottom">投资活动产生的净现金流</TD>
            `

        // Right-Justify column data
        // --------------------------
        MC := 0; WHILE(MC < PAD_MCOUNT) { FILL; `<TD class="dbRow` IF(OUTPUT_SUBTOTAL){`G`}ELSE{`C`} `results" vAlign="bottom" align="right">&nbsp;</TD>` IF(MC < MAX_SECTIONS-1) { FILL; `<TD class="dbRow` IF(OUTPUT_SUBTOTAL){`G`}ELSE{`C`} `results" vAlign="bottom" align="right">&nbsp;</TD>` } INC MC; }

        MC := 0;
        WHILE(MC < TOTAL_MC)
        {
            `
            <TD class="dbRow` IF(OUTPUT_SUBTOTAL){`G`}ELSE{`C`} `results" vAlign="bottom" align="right">`

            LCOUNT:=MC; RESULT := RUNFORM([:@AMT: = ":@AMOUNT:L?:"]);
            IF     (FLOATCMP([:@AMT],"0.0")== 0) {`-`}
            ELSE IF(STRCMP([:@AMT:S1],"-")==0)
            {
                RESULT := RUNFORM([:@TEMP: = ":@AMT:FREMOVE-"]);
                `([:@TEMP:FFLOAT])`
            }
            ELSE {`[:@AMT:FFLOAT]`}
            `</TD>`

            IF(MC < TOTAL_MC-1)
            {
                `
                <TD class="dbRow` IF(OUTPUT_SUBTOTAL){`G`}ELSE{`C`} `results" vAlign="bottom" align="right">`
                    FLOAT_FORMAT "1,000.00"; IF(FLOATCMP([:@CHANGE:FFLOAT],"0")==0) {`-`} ELSE {`[:@CHANGE:L?:FFLOAT]%`} FLOAT_FORMAT "1,000";
                `</TD>`
            }
            INC MC;
        }
        `
            <TD class="dbRow` IF(OUTPUT_SUBTOTAL){`G`}ELSE{`C`} `results" width="4">&nbsp;</TD>
        </TR>
        `
        OUTPUT_SUBTOTAL := 0;
    }

    // Net Cash Flow from Finance Activities
    // ----------------------------------------
    HASDATA := 0; MC := 0;
    WHILE(MC < TOTAL_MC)
    {
        LCOUNT:=MC; MCOUNT:=ATOI([:@FIN_BS_MCOUNT:L?]); RESULT := RUNFORM([:@AMOUNT:L?: = ""]);
        RESULT := RUNFORM([:@AMT: = :al:M?]);
        IF(EXISTS([:@AMT:FSTRIP])) { HASDATA := 1; RESULT := RUNFORM([:@AMOUNT:L?: = ":@AMT:"]);}

        // START Calculate %Change
        // ------------------------
        LCOUNT := MC; RESULT := RUNFORM([:@CHANGE:L?: = "0"]);
        IF(MC > 0)
        {
            FLOAT_FORMAT "1000";
            LCOUNT := MC-1;   RESULT := RUNFORM([:@AMT0: = ":@AMOUNT:L?:" * ":@FIN_BS_FACTOR:L?:"]);
            LCOUNT := MC;     RESULT := RUNFORM([:@AMT1: = ":@AMOUNT:L?:" * ":@FIN_BS_FACTOR:L?:"]);
            IF((EXISTS([:@AMT0]) + (EXISTS([:@AMT1])) + (ATOI([:@AMT1]) != 0))==3)
            {
                RESULT := RUNFORM([:@CHG:  = (:@AMT0 - :@AMT1)/(ABS(:@AMT1)) * "100"]);
                RESULT := RUNFORM([:@TEMP: = (ABS(:@CHG:) <=999)*(ABS(:@CHG:) + 0.005) + "0"]);
                IF(ATOI([:@TEMP]) > 0)
                {
                    IF(STRCMP([:@CHG:S1],"-")==0) {RESULT := RUNFORM([:@CHG: = ":@TEMP:" * "-1"]); }
                    ELSE                          {RESULT := RUNFORM([:@CHG: = ":@TEMP:"]);        }
                    LCOUNT := MC-1; RESULT := RUNFORM([:@CHANGE:L?:  = ":@CHG"]);
                }
            }
            FLOAT_FORMAT "1,000";
        }
        // END Calculate %Change
        // -----------------------

        INC MC;
    }
    IF(HASDATA)
    {
        `
        <TR>
            <TD class="dbRow` IF(OUTPUT_SUBTOTAL){`G`}ELSE{`C`} `label" width="4">&nbsp;</TD>
            <TD class="dbRow` IF(OUTPUT_SUBTOTAL){`G`}ELSE{`C`} `label" vAlign="bottom">融资活动产生的净现金流</TD>`

        // Right-Justify column data
        // --------------------------
        MC := 0; WHILE(MC < PAD_MCOUNT) { FILL; `<TD class="dbRow` IF(OUTPUT_SUBTOTAL){`G`}ELSE{`C`} `results" vAlign="bottom" align="right">&nbsp;</TD>` IF(MC < MAX_SECTIONS-1) { FILL; `<TD class="dbRow` IF(OUTPUT_SUBTOTAL){`G`}ELSE{`C`} `results" vAlign="bottom" align="right">&nbsp;</TD>` } INC MC; }

        MC := 0;
        WHILE(MC < TOTAL_MC)
        {
            `
            <TD class="dbRow` IF(OUTPUT_SUBTOTAL){`G`}ELSE{`C`} `results" vAlign="bottom" align="right">`

            LCOUNT:=MC; RESULT := RUNFORM([:@AMT: = ":@AMOUNT:L?:"]);
            IF     (FLOATCMP([:@AMT],"0.0")== 0) {`-`}
            ELSE IF(STRCMP([:@AMT:S1],"-")==0)
            {
                RESULT := RUNFORM([:@TEMP: = ":@AMT:FREMOVE-"]);
                `([:@TEMP:FFLOAT])`
            }
            ELSE {`[:@AMT:FFLOAT]`}
            `</TD>`

            IF(MC < TOTAL_MC-1)
            {
                `
                <TD class="dbRow` IF(OUTPUT_SUBTOTAL){`G`}ELSE{`C`} `results" vAlign="bottom" align="right">`
                    FLOAT_FORMAT "1,000.00"; IF(FLOATCMP([:@CHANGE:FFLOAT],"0")==0) {`-`} ELSE {`[:@CHANGE:L?:FFLOAT]%`} FLOAT_FORMAT "1,000";
                `</TD>`
            }
            INC MC;
        }
        `
            <TD class="dbRow` IF(OUTPUT_SUBTOTAL){`G`}ELSE{`C`} `results" width="4">&nbsp;</TD>
        </TR>
        `
        OUTPUT_SUBTOTAL := 0;
    }
    
    // Foreign Exchange Adjustment
    // -----------------------------------------
    HASDATA := 0; MC := 0;
    WHILE(MC < TOTAL_MC)
    {
        LCOUNT:=MC; MCOUNT:=ATOI([:@FIN_BS_MCOUNT:L?]); RESULT := RUNFORM([:@AMOUNT:L?: = ""]);
        RESULT := RUNFORM([:@AMT: = :an:M?]);
        IF(EXISTS([:@AMT:FSTRIP])) { HASDATA := 1; RESULT := RUNFORM([:@AMOUNT:L?: = ":@AMT:"]);}

        // START Calculate %Change
        // ------------------------
        LCOUNT := MC; RESULT := RUNFORM([:@CHANGE:L?: = "0"]);
        IF(MC > 0)
        {
            FLOAT_FORMAT "1000";
            LCOUNT := MC-1;   RESULT := RUNFORM([:@AMT0: = ":@AMOUNT:L?:" * ":@FIN_BS_FACTOR:L?:"]);
            LCOUNT := MC;     RESULT := RUNFORM([:@AMT1: = ":@AMOUNT:L?:" * ":@FIN_BS_FACTOR:L?:"]);
            IF((EXISTS([:@AMT0]) + (EXISTS([:@AMT1])) + (ATOI([:@AMT1]) != 0))==3)
            {
                RESULT := RUNFORM([:@CHG:  = (:@AMT0 - :@AMT1)/(ABS(:@AMT1)) * "100"]);
                RESULT := RUNFORM([:@TEMP: = (ABS(:@CHG:) <=999)*(ABS(:@CHG:) + 0.005) + "0"]);
                IF(ATOI([:@TEMP]) > 0)
                {
                    IF(STRCMP([:@CHG:S1],"-")==0) {RESULT := RUNFORM([:@CHG: = ":@TEMP:" * "-1"]); }
                    ELSE                          {RESULT := RUNFORM([:@CHG: = ":@TEMP:"]);        }
                    LCOUNT := MC-1; RESULT := RUNFORM([:@CHANGE:L?:  = ":@CHG"]);
                }
            }
            FLOAT_FORMAT "1,000";
        }
        // END Calculate %Change
        // -----------------------

        INC MC;
    }
    IF(HASDATA)
    {
        `
        <TR>
            <TD class="dbRowClabel" width="4">&nbsp;</TD>
            <TD class="dbRowClabel" vAlign="bottom">汇率变动影响</TD>`

        // Right-Justify column data
        // --------------------------
        MC := 0; WHILE(MC < PAD_MCOUNT) { FILL; `<TD class="dbRowCresults" vAlign="bottom" align="right">&nbsp;</TD>` IF(MC < MAX_SECTIONS-1) { FILL; `<TD class="dbRowCresults" vAlign="bottom" align="right">&nbsp;</TD>` } INC MC; }

        MC := 0;
        WHILE(MC < TOTAL_MC)
        {
            `
            <TD class="dbRowCresults" vAlign="bottom" align="right">`

            LCOUNT:=MC; RESULT := RUNFORM([:@AMT: = ":@AMOUNT:L?:"]);
            IF     (FLOATCMP([:@AMT],"0.0")== 0) {`-`}
            ELSE IF(STRCMP([:@AMT:S1],"-")==0)
            {
                RESULT := RUNFORM([:@TEMP: = ":@AMT:FREMOVE-"]);
                `([:@TEMP:FFLOAT])`
            }
            ELSE {`[:@AMT:FFLOAT]`}
            `</TD>`


            IF(MC < TOTAL_MC-1)
            {
                `
                <TD class="dbRowCresults" vAlign="bottom" align="right">`
                    FLOAT_FORMAT "1,000.00"; IF(FLOATCMP([:@CHANGE:FFLOAT],"0")==0) {`-`} ELSE {`[:@CHANGE:L?:FFLOAT]%`} FLOAT_FORMAT "1,000";
                `</TD>`
            }
            INC MC;
        }
        `
            <TD class="dbRowCresults" width="4">&nbsp;</TD>
        </TR>
        `
        OUTPUT_SUBTOTAL := 0;
    }
    

    // Total Net Cash Flow
    // --------------------
    HASDATA := 0; MC := 0;
    WHILE(MC < TOTAL_MC)
    {
        LCOUNT:=MC; MCOUNT:=ATOI([:@FIN_BS_MCOUNT:L?]); RESULT := RUNFORM([:@AMOUNT:L?: = ""]);
        RESULT := RUNFORM([:@AMT: = :am:M?]);
        IF(EXISTS([:@AMT:FSTRIP])) { HASDATA := 1; RESULT := RUNFORM([:@AMOUNT:L?: = ":@AMT:"]);}

        // START Calculate %Change
        // ------------------------
        LCOUNT := MC; RESULT := RUNFORM([:@CHANGE:L?: = "0"]);
        IF(MC > 0)
        {
            FLOAT_FORMAT "1000";
            LCOUNT := MC-1;   RESULT := RUNFORM([:@AMT0: = ":@AMOUNT:L?:" * ":@FIN_BS_FACTOR:L?:"]);
            LCOUNT := MC;     RESULT := RUNFORM([:@AMT1: = ":@AMOUNT:L?:" * ":@FIN_BS_FACTOR:L?:"]);
            IF((EXISTS([:@AMT0]) + (EXISTS([:@AMT1])) + (ATOI([:@AMT1]) != 0))==3)
            {
                RESULT := RUNFORM([:@CHG:  = (:@AMT0 - :@AMT1)/(ABS(:@AMT1)) * "100"]);
                RESULT := RUNFORM([:@TEMP: = (ABS(:@CHG:) <=999)*(ABS(:@CHG:) + 0.005) + "0"]);
                IF(ATOI([:@TEMP]) > 0)
                {
                    IF(STRCMP([:@CHG:S1],"-")==0) {RESULT := RUNFORM([:@CHG: = ":@TEMP:" * "-1"]); }
                    ELSE                          {RESULT := RUNFORM([:@CHG: = ":@TEMP:"]);        }
                    LCOUNT := MC-1; RESULT := RUNFORM([:@CHANGE:L?:  = ":@CHG"]);
                }
            }
            FLOAT_FORMAT "1,000";
        }
        // END Calculate %Change
        // -----------------------

        INC MC;
    }
    IF(HASDATA)
    {
        `
        <TR>
            <TD class="dbRowFlabel" width="4">&nbsp;</TD>
            <TD class="dbRowFlabel" vAlign="bottom"><B>净现金流量合计</B></TD>`

        // Right-Justify column data
        // --------------------------
        MC := 0; WHILE(MC < PAD_MCOUNT) { FILL; `<TD class="dbRowFresults" vAlign="bottom" align="right">&nbsp;</TD>` IF(MC < MAX_SECTIONS-1) { FILL; `<TD class="dbRowFresults" vAlign="bottom" align="right">&nbsp;</TD>` } INC MC; }

        MC := 0;
        WHILE(MC < TOTAL_MC)
        {
            `
            <TD class="dbRowFresults" vAlign="bottom" align="right">`

            LCOUNT:=MC; RESULT := RUNFORM([:@AMT: = ":@AMOUNT:L?:"]);
            IF     (FLOATCMP([:@AMT],"0.0")== 0) {`-`}
            ELSE IF(STRCMP([:@AMT:S1],"-")==0)
            {
                RESULT := RUNFORM([:@TEMP: = ":@AMT:FREMOVE-"]);
                `([:@TEMP:FFLOAT])`
            }
            ELSE {`[:@AMT:FFLOAT]`}
            `</TD>`

            IF(MC < TOTAL_MC-1)
            {
                `
                <TD class="dbRowFresults" vAlign="bottom" align="right">`
                    FLOAT_FORMAT "1,000.00"; IF(FLOATCMP([:@CHANGE:FFLOAT],"0")==0) {`-`} ELSE {`[:@CHANGE:L?:FFLOAT]%`} FLOAT_FORMAT "1,000";
                `</TD>`
            }
            INC MC;
        }
        `
            <TD class="dbRowFresults" width="4">&nbsp;</TD>
        </TR>
        `
        OUTPUT_SUBTOTAL := 1;
    }
    `
    </TABLE>
    `

}


