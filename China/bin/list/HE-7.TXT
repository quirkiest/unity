6     6    60  2  ENG   CHI
1     EN   a state-owned enterprise
1     CH   国有企业/国有独资有限公司
2     EN   a jointly owned enterprise
2     CH   联营企业
3     EN   a representative office
3     CH   办事处
4     EN   a collectively owned enterprise
4     CH   集体企业
5     EN   a limited company
5     CH   有限责任公司
6     EN   a private owned enterprise
6     CH   私营企业
7     EN   a Chinese foreign joint venture
7     CH   中外合资经营企业
8     EN   a Chinese foreign cooperative venture
8     CH   中外合作经营企业
9     EN   a government dept
9     CH   国家机关
11    EN   an enterprise processing provided samples
11    CH   三来一补企业
12    EN   a wholly foreign-owned enterprise
12    CH   外商独资企业
13    EN   a holding limited company
13    CH   控股公司
14    EN   a joint stock
14    CH   股份有限公司
15    EN   an administrative organization
15    CH   管理部门
16    EN   not registered
16    CH   未注册
18    EN   State owned institution
18    CH   事业单位/社团法人
