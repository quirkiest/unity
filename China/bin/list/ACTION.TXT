6     6    60  2  ENG   CHI
1     EN   Breach of Contract
1     CH   毁约
2     EN   Refund of Deposit
2     CH   订金退还
3     EN   Breach of Trust
3     CH   毁信
4     EN   Overtime Payment
4     CH   加班费
5     EN   Overdue Income Tax
5     CH   逾期所得税
6     EN   Infringement of Trademark
6     CH   侵犯商标
7     EN   Infringement of Patent Rights
7     CH   侵犯专利权
8     EN   Mortgage Interest
8     CH   房地产利息
9     EN   Breach of Warranty
9     CH   违反保证条款
10    EN   Misrepresentation
10    CH   误导
11    EN   Negligence
11    CH   疏忽
12    EN   Environmental Offense
12    CH   违反环保法
13    EN   Encroachment of Property
13    CH   Encroachment of Property
14    EN   Damages
14    CH   Damages
15    EN   Overdue Payment
15    CH   Overdue Payment
