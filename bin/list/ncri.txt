6  6  64  1  ENG
name  1 NCRI
Scr1  1 1/1 NCRI
re    1 NCRI adjust reason
ad    1 NCRI adjustment
ac    1 Update Date
ri    1 NCRI
st    1 Total (Score)
fa    1 Legal Structure
va    1 Legal Structure Score  
wa    1 Weight
sa    1 Weighted Score
fb    1 Industry
vb    1 Industry Score  
wb    1 Weight
sb    1 Weighted Score
fc    1 Employees
vc    1 Employees Score  
wc    1 Weight
sc    1 Weighted Score
fd    1 Import/Export
vd    1 Import/Export Score  
wd    1 Weight
sd    1 Weighted Score
fe    1 Registered Capital(RMB) & Years in Business
ve    1 Registered Capital(RMB) & Years in Business Score  
we    1 Weight
se    1 Weighted Score
ff    1 Region
vf    1 Region Score  
wf    1 Weight
sf    1 Weighted Score
fg    1 Registed Capital(RMB)
vg    1 Registed Capital(RMB) Score  
wg    1 Weight
sg    1 Weighted Score
fh    1 History
vh    1 History Score  
wh    1 Weight
sh    1 Weighted Score
fi    1 Ligitations  
vi    1 Ligitations Score  
wi    1 Weight
si    1 Weighted Score
fj    1 Slow Payment
vj    1 Slow Payment Score  
wj    1 Weight
sj    1 Weighted Score
fk    1 Family Support
vk    1 Family Support Score  
wk    1 Weight
sk    1 Weighted Score
fl    1 Current Ratio
vl    1 Current Ratio Score  
wl    1 Weight
sl    1 Weighted Score
fm    1 Sales(RMB) to Employees
vm    1 Sales(RMB) to Employees Score  
wm    1 Weight
sm    1 Weighted Score
fn    1 Return on Assets
vn    1 Return on Assets Score  
wn    1 Weight
sn    1 Weighted Score

