9 6 60 1 CHI
Name  1 Antecedents
Scr1  1 1/6  Principal Information
Scr2  1 2/6  Education
Scr3  1 3/6  Previous Employment
Scr4  1 4/6  Current Employment
Scr5  1 5/6  Affiliations
Scr6  1 6/6  Comments
ac    1 Update Date
in    1 Print Comments Only?
jg    1 CEO
is    1 Individual Shareholder
cm    1	Council Members
mc    1 Surname
mb    1 Given Name
MC    1 Chinese surname
MB    1 Chinese given name
mm    1 ID Number
ja    1 Title
js    1 Secondary Position
po    1	Position (Eng)
PO    1	Position (Chi)
af    1 Date of Birth
at    1	Nation
na    1 Nationality
ag    1 Marital Status
gd    1 Gender
Y1    1 Fact Title         
Y2    1 Department
Y3    1 Type DNB
Y4    1 Contact Email	
Y5    1 Contact AreaCode	
Y6    1 Contact Phone 	
Y7    1	Contact Phone extension 
Y8    1	Contact Fax 
Y9    1	Contact Fax extension 
Y0    1	Contact mobile 	
YY    1	Type Project

bb    1 Educational Background
bc    1 School
BC    1 Chinese school
bj    1 University Department
bx    1 Manual Enter Major (eng)
BX    1 Manual Enter Major (chi)
bi    1 Graduation Year
ka    1 Qualifications
KA    1 Qualification (c)
kb    1 Qualified as
KB    1 Qualified as (c)
qa    1 Qualified as list
yi    1 Excelling at (English)
YI    1 Excelling at (Chinese)
yj    1 Promotion way
yk    1 Negative Record(English)
YK    1	Negative Record (Chinese)

ck    1 From (year)
cl    1 to
cn    1 Current Position
cp    1 Firm
CP    1 Firm (chi)
cv    1 Title
ci    1 Conflict of Interest(eng)
CI    1 Conflict of Interest(chi)
rs    1 Related working exper. since :
mf    1 Military service from :
mt    1 Military service to :
jf    1 Comments
JF    1 comments(chi)
jx    1 Comments
JX    1 comments(chi)
db    1 Year started with subject
fo    1 Founder?
jb    1 Relationship with Founder
jc    1 Owner?
ji    1 Date promoted
jj    1 Title
dd    1 Current position from (year)
ra    1 Active in Day to Day Operations
sm    1 Sole Manager
eh    1 Affiliation type
ej    1 Affiliation Duns
ei    1 Firm Name
EI    1 Firm name (chi)
EC    1 Comments (chi)
ec    1 comments (eng)
ED    1 Comments (chi)
ed    1 comments (eng)
EE    1 Comments (chi)
ee    1 comments (eng)
EF    1 Comments (chi)
ef    1 comments (eng)
jk    1 Title
jm    1 Hobby
rp    1 Responsibilities
RP    1 Responsibilities (chi)
re    1 Resposibilities list	
fd    1 Function departments include
fc    1	Function Departments Comments(eng)
FC    1	Function Departments Comments(chi)
cc    1 Conflict Company DUNS
ek    1	Administrative Penalties(eng)
EK    1	Administrative Penalties(chi)




z1    1 1/6  Principal Information
z2    1 2/6  Education
z3    1 3/6  Previous Employment
z4    1 4/6  Current Employment
z5    1 5/6  Affiliations
z6    1 6/6  Comments
