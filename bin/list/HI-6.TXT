6       6       64      4       ENG     ESP     POR     JAP
1       EN      succeeded
1       SP      sucede a
1       PO      sucede a
1       JA      継承
2       EN      succeeded sole proprietorship of
2       SP      sucede la firma personal de
2       PO      sucede la firma personal de
2       JA      個人事業 ・・・ を継承
3       EN      absorbed activities of
3       SP      absorbi｢ las actividades de
3       PO      absorbi｢ las actividades de
3       JA      ・・・ の事業を吸収
4       EN      assumed activities of branch of
4       SP      asumi｢ las actividades de una sucursal de
4       PO      asumi｢ las actividades de una sucursal de
4       JA      ・・・ の子会社の事業を継承
5       EN      was formed as a result of merger of
5       SP      es el resultado de la fusi｢n de
5       PO      es el resultado de la fusi｢n de
5       JA      ・・・ の合併の結果誕生
6       EN      was formed as a new venture
6       SP      fu� originalmente organizada
6       PO      fu� originalmente organizada
6       JA      新規事業として誕生
r1      EN      was formed to continue operations of
r1      SP      was formed to continue operations of
r1      PO      was formed to continue operations of
r1      JA      ・・・ の業務を継続する為に設立された。
r2      EN      was formed to acquire operations of the business trading as
r2      SP      was formed to acquire operations of the business trading as
r2      PO      was formed to acquire operations of the business trading as
r2      JA      ・・・ として業務を獲得運営して行く為に設立された。
r3      EN      was formed to acquire operations of the business trading under same style
r3      SP      was formed to acquire operations of the business trading under same style
r3      PO      was formed to acquire operations of the business trading under same style
r3      JA      同形態で業務を獲得運営して行く為に設立された。
