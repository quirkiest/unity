6     6     64  2  ENG  CHI
1     EN    founder
1     CH    公司创办人
2     EN    son of founder
2     CH    公司创办人的儿子
3     EN    descendant of founder
3     CH    公司创办人的後裔
4     EN    family member of former President
4     CH    卸任董事长的家庭成员
5     EN    family member of President
5     CH    董事长的家庭成员
6     EN    family member of Chairman
6     CH    负责人的家庭成员
7     EN    relative of founder
7     CH    负责人的亲戚
8     EN    daughter of founder
8     CH    创办人的女儿
9     EN    son of President
9     CH    董事长的儿子
10    EN    daughter of President
10    CH    董事长的女儿
11    EN    Spouse of President
11    CH    董事长的太太
