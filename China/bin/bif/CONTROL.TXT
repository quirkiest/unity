9 6 32 1 ENG
General      1       Header information
Riskguid     1       Risk Guide
History      1       History
Manager      1       Principals
Antece1      30      Antecedents
Operatio     1       Operations
Risk         1       Risk Score
NCRI         30      NCRI
Terri        1       Territory & Supplies
Instala      1       Location
Subsid       1       Subsidiaries
Affil        1       Affiliates
Parent       1       Parent
Current      1       Current Investigation
Update       20      Update
News         20      News
Finstmtb     10      Financial statement
Fincom       4       Financial statement
Banks        20      Banks
::*Bonds     1       Bonds
Trend        1       Sales & Profit Trend
Court        1       Court Action
Collect      1       Collections
Insur        1       Insurance
Payment      1       Trade Payments
Conclu       1       Conclusion
Utl          1       UTL - Free Format
Oob          1       Oob - Free Format
Company      1       Search of company
Text_nar     1       Highlights - M/F
Text_pay     1       Payments - MF
Text_upd     1       Report Update - M/F
Comp         3       Comparatives - M/F
Text_fin     1       Financials - M/F
Text_bal     1       Balance Sheet - M/F
Text_ban     1       Banks - M/F
Text_bnk     1       Banking - MF
Text_his     1       History - M/F
Text_dir     1       Directors - M/F
Text_ops     1       Operations - M/F
Text_loc     1       Location - M/F
