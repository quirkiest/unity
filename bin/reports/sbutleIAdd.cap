//*********************************************************************
//  sbeIAdd.cap - SuperBIR Eng. HTML Identification - Address
//*********************************************************************

DBSECTION "general";
LANGUAGE "ENG";
LINE_LENGTH 500;
NOTFILL;
DECLARE NUMLOOPS;
DECLARE RESULT;

DECLARE HAVE_DATA; HAVE_DATA := 0;

// :@PRODUCT variables
// -----------------------
DECLARE PRODUCT_HYBRIDLC; PRODUCT_HYBRIDLC := 3;


DECLARE DISPLAYLINECOUNT;   DISPLAYLINECOUNT    := 0;
DECLARE DLLCOUNT;           DLLCOUNT            := 0;
DECLARE NUMDISPLAYLINE;     NUMDISPLAYLINE      := 0;
DECLARE LOAD_LINE;          LOAD_LINE           := 0;

RESULT := RUNFORM([:@TEMP: = ""]);
DLLCOUNT         := 0;
DISPLAYLINECOUNT := 0;
WHILE(DISPLAYLINECOUNT < 6)
{
    RESULT := RUNFORM([:@TEMP: = ""]);

    // Set Up the Address element
    // ----------------------------
    IF(DISPLAYLINECOUNT==0) { RESULT := RUNFORM([:@TEMP: = "::general:dd:FFIRSTUPPERCASE:"]);   } // Street Address 1
    IF(DISPLAYLINECOUNT==1) { RESULT := RUNFORM([:@TEMP: = "::general:d9:FFIRSTUPPERCASE:"]);   } // Street Address 2

    IF(EXISTS([:di:B*]))     // HAVE County
    {
        IF(DISPLAYLINECOUNT==2)
        {
            IF(EXISTS([:ci:B*])==0)  //NO City (New)
            {
                RESULT := RUNFORM([:@TEMP: = "::general:df:FFIRSTUPPERCASE:"]);  // City (!!!This is old field!!!)
            }
        }
        IF(DISPLAYLINECOUNT==3) { RESULT := RUNFORM([:@TEMP: = "::general:di:Bsf:FFIRSTUPPERCASE:"]);  }  // County
    }
    ELSE                    // NO County
    {
        IF(DISPLAYLINECOUNT==2) { RESULT := RUNFORM([:@TEMP: = "::general:dk:FFIRSTUPPERCASE:"]);            } // District
        IF(DISPLAYLINECOUNT==3) { RESULT := RUNFORM([:@TEMP: = "::general:ci:Bcity:FFIRSTUPPERCASE:"]);      } // City (New)
        IF(DISPLAYLINECOUNT==4) { RESULT := RUNFORM([:@TEMP: = "::general:pv:Bprovince::FFIRSTUPPERCASE:"]); } // Province
    }
    IF(DISPLAYLINECOUNT==5)
    {
        IF(EXISTS([:dg:B*])==0)                                                 // Postal Code
        {
            RESULT := RUNFORM([:@TEMP: = "::general:dj:Bcf:FFIRSTUPPERCASE:"]);  // Country
        }
    }

    // Load the Address element
    // ----------------------------
    IF(EXISTS([:@TEMP:FSTRIP]))
    {
        LOAD_LINE := 1;
        IF(DISPLAYLINECOUNT > 0)
        {
            // DO NOT LOAD duplicate lines
            // -----------------------------
            LCOUNT := DLLCOUNT-1;
            LOAD_LINE := (STRCMPI([:@TEMP:FSTRIP],[:@DISPLAYLINE:L?])!=0);
        }
        IF(LOAD_LINE)
        {
            LCOUNT := DLLCOUNT; INC DLLCOUNT; RESULT := RUNFORM([:@DISPLAYLINE:L?: = ":@TEMP:"]);
        }
    }
    INC DISPLAYLINECOUNT;
}

// Output Address Elements
// ------------------------
NUMDISPLAYLINE := DLLCOUNT;
DLLCOUNT       := 0;
WHILE(DLLCOUNT < NUMDISPLAYLINE)
{
    LCOUNT := DLLCOUNT;
    `
    <TR>
    `
    IF(DLLCOUNT==0)
    {
        `<TD width="110" class="idLabels">Address</TD>`
    }
    ELSE
    {
        `<TD width="110" class="idLabels"> </TD>`
    }

    `
        <TD width="160" class="idRows">[:@DISPLAYLINE:L?:FSTRIP]</TD>
    </TR>
    `
    HAVE_DATA := 1;
    INC DLLCOUNT;
}


// Postal Code
// --------------
IF(EXISTS([:dg:B*]))
{
    `
    <TR>
        <TD width="110" class="idLabels">Postal Code</TD>
        <TD width="160" class="idRows">[:dg]</TD>
    </TR>
    `
}

LCOUNT := PRODUCT_HYBRIDLC;
IF(ATOI([:@PRODUCT:L?])==1)
{
    DISPLAYLINECOUNT    := 0;
    DLLCOUNT            := 0;
    NUMDISPLAYLINE      := 0;
    LOAD_LINE           := 0;

    RESULT := RUNFORM([:@TEMP: = ""]);
    DLLCOUNT         := 0;
    DISPLAYLINECOUNT := 0;
    WHILE(DISPLAYLINECOUNT < 6)
    {
        RESULT := RUNFORM([:@TEMP: = ""]);

        // Set Up the Address element
        // ----------------------------
        IF(DISPLAYLINECOUNT==0) { RESULT := RUNFORM([:@TEMP: = "::general:DD"]);   } // Street Address 1
        IF(DISPLAYLINECOUNT==1) { RESULT := RUNFORM([:@TEMP: = "::general:D9"]);   } // Street Address 2

        IF(EXISTS([:di:B*]))     // HAVE County
        {
            IF(DISPLAYLINECOUNT==2)
            {
                IF(EXISTS([:ci:B*])==0)  //NO City (New)
                {
                    RESULT := RUNFORM([:@TEMP: = "::general:DF"]);  // City (!!!This is old field!!!)
                }
            }
            IF(DISPLAYLINECOUNT==3) { RESULT := RUNFORM([:@TEMP: = "::general:di:Bsf:FFIRSTUPPERCASE:"]);  }  // County
        }
        ELSE                    // NO County
        {
            IF(DISPLAYLINECOUNT==2) { RESULT := RUNFORM([:@TEMP: = "::general:DK"]);            } // District
            IF(DISPLAYLINECOUNT==3) { RESULT := RUNFORM([:@TEMP: = "::general:ci:Bcity:CCHI"]);      } // City (New)
            IF(DISPLAYLINECOUNT==4) { RESULT := RUNFORM([:@TEMP: = "::general:pv:Bprovince:CCHI"]); } // Province
        }
        IF(DISPLAYLINECOUNT==5)
        {
            IF(EXISTS([:dg:B*])==0)  // NO Postal Code
            {
                RESULT := RUNFORM([:@TEMP: = "::general:dj:Bcf:CCHI"]);  // Country
            }
        }

        // Load the Address element
        // ----------------------------
        IF(EXISTS([:@TEMP:FSTRIP]))
        {
            LOAD_LINE := 1;
            IF(DISPLAYLINECOUNT > 0)
            {
                // DO NOT LOAD duplicate lines
                // -----------------------------
                LCOUNT := DLLCOUNT-1;
                LOAD_LINE := (STRCMPI([:@TEMP:FSTRIP],[:@DISPLAYLINE:L?])!=0);
            }
            IF(LOAD_LINE)
            {
                LCOUNT := DLLCOUNT; INC DLLCOUNT; RESULT := RUNFORM([:@DISPLAYLINE:L?: = ":@TEMP:"]);
            }
        }
        INC DISPLAYLINECOUNT;
    }

    // Output Address Elements
    // ------------------------
    NUMDISPLAYLINE := DLLCOUNT;
    DLLCOUNT       := 0;
    WHILE(DLLCOUNT < NUMDISPLAYLINE)
    {
        LCOUNT := DLLCOUNT;
        `
        <TR>
        `
        IF(DLLCOUNT==0)
        {
            `<TD width="110" class="idLabels">Address (Chinese)</TD>`
        }
        ELSE
        {
            `<TD width="110" class="idLabels"> </TD>`
        }

        `
            <TD width="160" class="idRows">[:@DISPLAYLINE:L?:FSTRIP]</TD>
        </TR>
        `
        HAVE_DATA := 1;
        INC DLLCOUNT;
    }
}


// Telephone
// ------------------------
LCOUNT := 0;
NUMLOOPS := LOOPS([:gb]);
WHILE(LCOUNT < NUMLOOPS)
{
    `
    <TR>
    `
    IF(LCOUNT == 0)
    {
        `<TD width="110" class="idLabels">Telephone</TD>`
    }
    ELSE
    {
        `<TD width="110" class="idLabels"> </TD>`
    }
    IF(EXISTS([:gb:L?]))
    {
        `
        <TD width="160" class="idRows">[:ga:L?] [:gb:L?]`
        IF (EXISTS([:ge:L?])) { `-[:ge:L?]` }
        `</TD>
        `
    }
    `
    </TR>
    `
    HAVE_DATA := 1;
    IF(LCOUNT > 0) { LCOUNT := NUMLOOPS;}
    INC LCOUNT;
}

// Facsimile
// ------------------------
LCOUNT := 0;
NUMLOOPS := LOOPS([:gd]);
WHILE(LCOUNT < NUMLOOPS)
{
    `
    <TR>
    `
    IF(LCOUNT == 0)
    {
        `<TD width="110" class="idLabels">Facsimile</TD>`
    }
    ELSE
    {
        `<TD width="110" class="idLabels"> </TD>`
    }
    IF(EXISTS([:gd:L?]))
    {
        `
        <TD width="160" class="idRows">[:gc:L?] [:gd:L?]`
        IF (EXISTS([:gf:L?])) { `-[:gf:L?]` }
        `</TD>
        `
    }
    `
    </TR>
    `
    HAVE_DATA := 1;
    IF(LCOUNT > 0) { LCOUNT := NUMLOOPS;}
    INC LCOUNT;
}

// Web Site
// ------------------------
IF(EXISTS([:we]))
{
    `
    <TR>
        <TD width="110" class="idLabels">Website</TD>
        <TD width="160" class="idRows" style="word-wrap: break-word;">
            <A class="summaryLinksNew" href="http://[:we]">[:we]</A>
        </TD>
    </TR>
    `
    HAVE_DATA := 1;
}

IF(HAVE_DATA)
{
    `
    <TR>
        <TD width="110" class="idRowsLast"><IMG src="[:@WEBSERVER]images/spacer.gif" width="1" height=1></TD>
        <TD width="160" class="idRowsLast"><IMG src="[:@WEBSERVER]images/spacer.gif" width="1" height=1></TD>
    </TR>
    `
}

