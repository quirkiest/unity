//*************************************************************************************************
//  tpcPOPBT.cap - TradePaymentReport CHI. HTML Payment Overview - Payment Behaviour Over Time
//*************************************************************************************************
DBSECTION "payment";
LANGUAGE "CHI";
LINE_LENGTH 800;
NOTFILL;
DECLARE RESULT;

// :@TRADEPAYMENT:L?  variables
// ---------------------------
DECLARE NUMEXPLC;               NUMEXPLC                := 3;
DECLARE INDUSTRYGROUP_DESCLC;   INDUSTRYGROUP_DESCLC    := 4;

// :@FIELD:L?  variables
// ---------------------------
DECLARE COMPNAMELC;             COMPNAMELC          := 0;
DECLARE COMPNAME_CHINESELC;     COMPNAME_CHINESELC  := 2;

DECLARE TPBRS_PRV_YCOUNT;   TPBRS_PRV_YCOUNT  := -1;
DECLARE TPBRS_PRV_NUMRECS;  TPBRS_PRV_NUMRECS := 0;
DECLARE TPBRS_PRV_LCOUNT;   TPBRS_PRV_LCOUNT  := 0;

DECLARE TPBRS_DUNS_YCOUNT;  TPBRS_DUNS_YCOUNT  := -1;
DECLARE TPBRS_DUNS_NUMRECS; TPBRS_DUNS_NUMRECS := 0;
DECLARE TPBRS_DUNS_LCOUNT;  TPBRS_DUNS_LCOUNT  := 0;

DECLARE TPBRS_SIC2_YCOUNT;  TPBRS_SIC2_YCOUNT  := -1;
DECLARE TPBRS_SIC2_NUMRECS; TPBRS_SIC2_NUMRECS := 0;
DECLARE TPBRS_SIC2_LCOUNT;  TPBRS_SIC2_LCOUNT  := 0;

DECLARE TPBRS_SIC4_YCOUNT;  TPBRS_SIC4_YCOUNT  := -1;
DECLARE TPBRS_SIC4_NUMRECS; TPBRS_SIC4_NUMRECS := 0;
DECLARE TPBRS_SIC4_LCOUNT;  TPBRS_SIC4_LCOUNT  := 0;

DECLARE INDNAME_LCOUNT;     INDNAME_LCOUNT     := 0;
DECLARE SDP_LCOUNT;         SDP_LCOUNT         := 0;
DECLARE SDPVALUE_LCOUNT;    SDPVALUE_LCOUNT    := 0;
DECLARE SDPVALUE_NUMRECS;   SDPVALUE_NUMRECS   := 0;

DECLARE DBGYCOUNT;          DBGYCOUNT   := -1;
DECLARE DBGNUMRECS;         DBGNUMRECS  := 0;
DECLARE CONTINUE;           CONTINUE    := 0;
DECLARE ERROR;              ERROR       := 0;
DECLARE THRESHHOLD;         THRESHHOLD  := 10;
DECLARE MAXSICCOUNT;        MAXSICCOUNT  := 0;
DECLARE MAXSIC2COUNT;       MAXSIC2COUNT := 0;
DECLARE MAXSIC4COUNT;       MAXSIC4COUNT := 0;

DECLARE XAXISVALUE_YCOUNT;  XAXISVALUE_YCOUNT  := -1;
DECLARE XAXISVALUE_NUMRECS; XAXISVALUE_NUMRECS := 0;
DECLARE XAXISVALUE_LCOUNT;  XAXISVALUE_LCOUNT  := 0;

RESULT := RUNFORM([:@SIC2NDesc: = ""]);RESULT := RUNFORM([:@SIC2EDesc: = ""]);
RESULT := RUNFORM([:@SIC4NDesc: = ""]);RESULT := RUNFORM([:@SIC4EDesc: = ""]);
RESULT := RUNFORM([:@SICNDesc:  = ""]);RESULT := RUNFORM([:@SICEDesc: = ""]);

LCOUNT   := NUMEXPLC;
CONTINUE := (ATOI([:@TRADEPAYMENT:L?]) > 0);

// Get Trade Payment Behaviour YAxis values
// --------------------------------------------
IF(CONTINUE)
{
    CONTINUE := 0;

    // Trade Payment Behaviour YAxisValues
    // ------------------------------------
    RESULT := RUNFORM([:@OPTION:    = "PAYREC_VALUES"]);
    TPBRS_PRV_YCOUNT := LOADREL([:+DPUB>SP>usp_TradePaymentBehaviourReportSummary:K~Option=:@OPTION]);
    YCOUNT := TPBRS_PRV_YCOUNT; TPBRS_PRV_NUMRECS := COUNTREL([:Y?]);

    IF(TPBRS_PRV_NUMRECS > 0)
    {
        CONTINUE := 1;
    }
    ELSE
    {
        YCOUNT := TPBRS_PRV_YCOUNT; TPBRS_PRV_YCOUNT := -1; YCOUNT := UNLOADREL([:Y?]);
    }
}

// Get Trade Payment Behaviour DUNS Data Points
// --------------------------------------------
IF(CONTINUE)
{
    CONTINUE := 0;

    // Trade Payment Behaviour Data Points
    // ------------------------------------
    RESULT := RUNFORM([:@OPTION:    = "DUNS"]);
    RESULT := RUNFORM([:@DUNS:      = ::general:aa:FSTRIP]);
    RESULT := RUNFORM([:@StartDate: = ""]);
    RESULT := RUNFORM([:@MonthsAgo: = "12"]);
    TPBRS_DUNS_YCOUNT := LOADREL([:+DPUB>SP>usp_TradePaymentBehaviourReportSummary:K~Option=:@OPTION:~:K~DUNS=:@DUNS:~:K~StartDate=:@StartDate:~:K~MonthsAgo=:@MonthsAgo:~:K~Language="CHI"]);

    //*********************************************************************************************************************************************
    //Sample
    //*********************************************************************************************************************************************
    //Duns_no   Country_no Type SIC  Exp_date_MMM_YY Exp_date_MMM_YY_Duns_Count Mean_Pay_Record Mean_Pay_Record_Ordinal Update_date
    //--------- ---------- ---- ---- --------------- -------------------------- --------------- ----------------------- ------------------- --------------------------- -----------------------
    //526800701 156        DUNS 8062 Feb 09          1                          PPT             10.0                    2010-02-15 19:11:57.090
    //526800701 156        DUNS 8062 Mar 09          1                          PPT             10.0                    2010-02-15 19:11:57.090
    //526800701 156        DUNS 8062 Apr 09          1                          PPT             10.0                    2010-02-15 19:11:57.090
    //526800701 156        DUNS 8062 May 09          1                          PPT/SLW 30      9.0                     2010-02-15 19:11:57.090
    //526800701 156        DUNS 8062 Jun 09          1                          PPT/SLW 30      9.0                     2010-02-15 19:11:57.090
    //526800701 156        DUNS 8062 Jul 09          1                          PPT             10.0                    2010-02-15 19:11:57.090
    //526800701 156        DUNS 8062 Aug 09          1                          PPT             10.0                    2010-02-15 19:11:57.090
    //526800701 156        DUNS 8062 Sep 09          1                          PPT             10.0                    2010-02-15 19:11:57.090
    //526800701 156        DUNS 8062 Oct 09          1                          PPT             10.0                    2010-02-15 19:11:57.090
    //526800701 156        DUNS 8062 Nov 09          1                          PPT             10.0                    2010-02-15 19:11:57.090
    //526800701 156        DUNS 8062 Dec 09          1                          PPT             10.0                    2010-02-15 19:11:57.090
    //526800701 156        DUNS 8062 Jan 10          1                          PPT             10.0                    2010-02-15 19:11:57.090
    //526800701 156        DUNS 8062 Feb 10          1                          PPT             10.0                    2010-02-15 19:11:57.090
    //*********************************************************************************************************************************************

    YCOUNT := TPBRS_DUNS_YCOUNT; TPBRS_DUNS_NUMRECS := COUNTREL([:Y?]);
    IF(TPBRS_DUNS_NUMRECS == 0)
    {
        YCOUNT := TPBRS_DUNS_YCOUNT; TPBRS_DUNS_YCOUNT := -1; YCOUNT := UNLOADREL([:Y?]);
    }
    CONTINUE := 1;
}

// Get Trade Payment Behaviour SIC2 Data Points
// --------------------------------------------
IF(CONTINUE)
{
    CONTINUE := 0;

    // Trade Payment Behaviour Data Points
    // ------------------------------------
    RESULT := RUNFORM([:@OPTION:    = "SIC2"]);
    RESULT := RUNFORM([:@DUNS:      = ::general:aa:FSTRIP]);
    RESULT := RUNFORM([:@StartDate: = ""]);
    RESULT := RUNFORM([:@MonthsAgo: = "12"]);
    TPBRS_SIC2_YCOUNT := LOADREL([:+DPUB>SP>usp_TradePaymentBehaviourReportSummary:K~Option=:@OPTION:~:K~DUNS=:@DUNS:~:K~StartDate=:@StartDate:~:K~MonthsAgo=:@MonthsAgo:~:K~Language="CHI"]);

    MAXSICCOUNT := 0;
    RESULT := RUNFORM([:@SIC2NDesc: = ""]);RESULT := RUNFORM([:@SIC2EDesc: = ""]);
    YCOUNT := TPBRS_SIC2_YCOUNT; TPBRS_SIC2_NUMRECS := COUNTREL([:Y?]);
    IF(TPBRS_SIC2_NUMRECS > 0)
    {
        TPBRS_SIC2_LCOUNT := 0;
        WHILE(TPBRS_SIC2_LCOUNT < TPBRS_SIC2_NUMRECS)
        {
            YCOUNT := TPBRS_SIC2_YCOUNT; LCOUNT := TPBRS_SIC2_LCOUNT;
            IF(ATOI([:+CExp_date_MMM_YY_Duns_Count:Y?:L?]) > MAXSICCOUNT)
            {
                MAXSICCOUNT := ATOI([:+CExp_date_MMM_YY_Duns_Count:Y?:L?]);
            }
            IF(EXISTS([:@SIC2NDesc])==0)
            {
                RESULT := RUNFORM([:@TEMP:L0:  =  ":+CSIC:Y?:L?:FSTRIP"]);
                RESULT := RUNFORM([:@SIC2NDesc: = ":@TEMP:L0: | :+DGEN>SIC_Master:Ksic_code:CSpa_description"]);
            }
            IF(EXISTS([:@SIC2EDesc])==0)
            {
                RESULT := RUNFORM([:@TEMP:L0:  =  ":+CSIC:Y?:L?:FSTRIP"]);
                RESULT := RUNFORM([:@SIC2EDesc: = ":@TEMP:L0: | :+DGEN>SIC_Master:Ksic_code:CEng_description"]);
            }
            INC TPBRS_SIC2_LCOUNT;
        }
        CONTINUE := 1;
    }
    ELSE
    {
        YCOUNT := TPBRS_SIC2_YCOUNT; TPBRS_SIC2_YCOUNT := -1; YCOUNT := UNLOADREL([:Y?]);
    }
    MAXSIC2COUNT := MAXSICCOUNT;
    CONTINUE := 1;
}

// Get Trade Payment Behaviour SIC4 Data Points
// --------------------------------------------
IF(CONTINUE)
{
    CONTINUE := 0;

    // Trade Payment Behaviour Data Points
    // ------------------------------------
    RESULT := RUNFORM([:@OPTION:    = "SIC4"]);
    RESULT := RUNFORM([:@DUNS:      = ::general:aa:FSTRIP]);
    RESULT := RUNFORM([:@StartDate: = ""]);
    RESULT := RUNFORM([:@MonthsAgo: = "12"]);
    TPBRS_SIC4_YCOUNT := LOADREL([:+DPUB>SP>usp_TradePaymentBehaviourReportSummary:K~Option=:@OPTION:~:K~DUNS=:@DUNS:~:K~StartDate=:@StartDate:~:K~MonthsAgo=:@MonthsAgo:~:K~Language="CHI"]);

    MAXSICCOUNT := 0;
    RESULT := RUNFORM([:@SIC4NDesc: = ""]);RESULT := RUNFORM([:@SIC4EDesc: = ""]);
    YCOUNT := TPBRS_SIC4_YCOUNT; TPBRS_SIC4_NUMRECS := COUNTREL([:Y?]);
    IF(TPBRS_SIC4_NUMRECS > 0)
    {
        TPBRS_SIC4_LCOUNT := 0;
        WHILE(TPBRS_SIC4_LCOUNT < TPBRS_SIC4_NUMRECS)
        {
            YCOUNT := TPBRS_SIC4_YCOUNT; LCOUNT := TPBRS_SIC4_LCOUNT;
            IF(ATOI([:+CExp_date_MMM_YY_Duns_Count:Y?:L?]) > MAXSICCOUNT)
            {
                MAXSICCOUNT := ATOI([:+CExp_date_MMM_YY_Duns_Count:Y?:L?]);
            }
            IF(EXISTS([:@SIC4NDesc])==0)
            {
                RESULT := RUNFORM([:@TEMP:L0:  =  ":+CSIC:Y?:L?:FSTRIP"]);
                RESULT := RUNFORM([:@SIC4NDesc: = ":@TEMP:L0: | :+DGEN>SIC_Master:Ksic_code:CSpa_description"]);
            }
            IF(EXISTS([:@SIC4EDesc])==0)
            {
                RESULT := RUNFORM([:@TEMP:L0:  =  ":+CSIC:Y?:L?:FSTRIP"]);
                RESULT := RUNFORM([:@SIC4EDesc: = ":@TEMP:L0: | :+DGEN>SIC_Master:Ksic_code:CEng_description"]);
            }
            INC TPBRS_SIC4_LCOUNT;
        }
        CONTINUE := 1;
    }
    ELSE
    {
        YCOUNT := TPBRS_SIC4_YCOUNT; TPBRS_SIC4_YCOUNT := -1; YCOUNT := UNLOADREL([:Y?]);
    }
    MAXSIC4COUNT := MAXSICCOUNT;
    CONTINUE := 1;
}

IF(CONTINUE)
{
    IF(MAXSIC4COUNT >= THRESHHOLD)
    {
        RESULT := RUNFORM([:@SICNDesc: = ":@SIC4NDesc:FSTRIP"]);
        RESULT := RUNFORM([:@SICEDesc: = ":@SIC4EDesc:FSTRIP"]);
    }
    ELSE IF(MAXSIC2COUNT >= THRESHHOLD)
    {
        RESULT := RUNFORM([:@SICNDesc: = ":@SIC2NDesc:FSTRIP"]);
        RESULT := RUNFORM([:@SICEDesc: = ":@SIC4EDesc:FSTRIP"]);
    }
    ELSE
    {
        RESULT := RUNFORM([:@SICNDesc: = "N/A"]);
        RESULT := RUNFORM([:@SICEDesc: = "N/A"]);
    }
    LCOUNT := INDUSTRYGROUP_DESCLC;
    RESULT := RUNFORM([:@TRADEPAYMENT:L?: = :@SICNDesc:FSTRIP]);
}

IF(CONTINUE)
{
    // Trade Payment Industry Comparison
    // ----------------------------------
    RESULT := RUNFORM([:@OPERATION: = "GETUID"]);
    RESULT := RUNFORM([:@UID:       = ""]);
    RESULT := RUNFORM([:@ParamName: = ""]);
    RESULT := RUNFORM([:@ParamValue:= ""]);
    DBGYCOUNT := LOADREL([:+DPUB>SP>uspDnBGrapher:K~Operation=:@OPERATION:~:K~UID=:@UID:~:K~ParamName=:@ParamName:~:K~ParamValue=:@ParamValue]);

    CONTINUE := 1;
    YCOUNT := DBGYCOUNT; DBGNUMRECS := COUNTREL([:Y?]);
    IF(DBGNUMRECS > 0)
    {
        YCOUNT := DBGYCOUNT; LCOUNT := 0;
        RESULT := RUNFORM([:@UID: = :+CUID:Y?:L?:FSTRIP]);
        CONTINUE := (EXISTS([:@UID])==1);
    }
    IF(DBGYCOUNT > -1){YCOUNT := DBGYCOUNT; DBGYCOUNT := -1; YCOUNT := UNLOADREL([:Y?]);}
    IF(CONTINUE)
    {
        RESULT := RUNFORM([:@OPERATION: = "ADDPARAM"]);
        RESULT := RUNFORM([:@ParamName: = "GraphType"]);        RESULT := RUNFORM([:@ParamValue:= "DnbTradePaymentAnalysis"]);       DBGYCOUNT := LOADREL([:+DPUB>SP>uspDnBGrapher:K~Operation=:@OPERATION:~:K~UID=:@UID:~:K~ParamName=:@ParamName:~:K~ParamValue=:@ParamValue]);     IF(DBGYCOUNT > -1){YCOUNT := DBGYCOUNT; DBGYCOUNT := -1; YCOUNT := UNLOADREL([:Y?]);}
        RESULT := RUNFORM([:@ParamName: = "Version"]);          RESULT := RUNFORM([:@ParamValue:= "002"]);                           DBGYCOUNT := LOADREL([:+DPUB>SP>uspDnBGrapher:K~Operation=:@OPERATION:~:K~UID=:@UID:~:K~ParamName=:@ParamName:~:K~ParamValue=:@ParamValue]);     IF(DBGYCOUNT > -1){YCOUNT := DBGYCOUNT; DBGYCOUNT := -1; YCOUNT := UNLOADREL([:Y?]);}

        RESULT := RUNFORM([:@ParamName: = "ImagePath"]);
        IF(EXISTS([:@TicketKey:FSTRIP])){ RESULT := RUNFORM([:@ParamValue: = "CHN/Ticket/" & :@TicketKey:FSTRIP & "/ "]);}
        ELSE                            { RESULT := RUNFORM([:@ParamValue: = "CHN/Generic/"]);}

        DBGYCOUNT := LOADREL([:+DPUB>SP>uspDnBGrapher:K~Operation=:@OPERATION:~:K~UID=:@UID:~:K~ParamName=:@ParamName:~:K~ParamValue=:@ParamValue]);     IF(DBGYCOUNT > -1){YCOUNT := DBGYCOUNT; DBGYCOUNT := -1; YCOUNT := UNLOADREL([:Y?]);}

        RESULT := RUNFORM([:@ParamName: = "Language"]);         RESULT := RUNFORM([:@ParamValue:= "CHI"]);                           DBGYCOUNT := LOADREL([:+DPUB>SP>uspDnBGrapher:K~Operation=:@OPERATION:~:K~UID=:@UID:~:K~ParamName=:@ParamName:~:K~ParamValue=:@ParamValue]);     IF(DBGYCOUNT > -1){YCOUNT := DBGYCOUNT; DBGYCOUNT := -1; YCOUNT := UNLOADREL([:Y?]);}
        RESULT := RUNFORM([:@ParamName: = "Title"]);            RESULT := RUNFORM([:@ParamValue:= ""]);                              DBGYCOUNT := LOADREL([:+DPUB>SP>uspDnBGrapher:K~Operation=:@OPERATION:~:K~UID=:@UID:~:K~ParamName=:@ParamName:~:K~ParamValue=:@ParamValue]);     IF(DBGYCOUNT > -1){YCOUNT := DBGYCOUNT; DBGYCOUNT := -1; YCOUNT := UNLOADREL([:Y?]);}
        RESULT := RUNFORM([:@ParamName: = "Height"]);           RESULT := RUNFORM([:@ParamValue:= "320"]);                           DBGYCOUNT := LOADREL([:+DPUB>SP>uspDnBGrapher:K~Operation=:@OPERATION:~:K~UID=:@UID:~:K~ParamName=:@ParamName:~:K~ParamValue=:@ParamValue]);     IF(DBGYCOUNT > -1){YCOUNT := DBGYCOUNT; DBGYCOUNT := -1; YCOUNT := UNLOADREL([:Y?]);}
        RESULT := RUNFORM([:@ParamName: = "Width"]);            RESULT := RUNFORM([:@ParamValue:= "610"]);                           DBGYCOUNT := LOADREL([:+DPUB>SP>uspDnBGrapher:K~Operation=:@OPERATION:~:K~UID=:@UID:~:K~ParamName=:@ParamName:~:K~ParamValue=:@ParamValue]);     IF(DBGYCOUNT > -1){YCOUNT := DBGYCOUNT; DBGYCOUNT := -1; YCOUNT := UNLOADREL([:Y?]);}
        RESULT := RUNFORM([:@ParamName: = "XTitle"]);           RESULT := RUNFORM([:@ParamValue:= ""]);                              DBGYCOUNT := LOADREL([:+DPUB>SP>uspDnBGrapher:K~Operation=:@OPERATION:~:K~UID=:@UID:~:K~ParamName=:@ParamName:~:K~ParamValue=:@ParamValue]);     IF(DBGYCOUNT > -1){YCOUNT := DBGYCOUNT; DBGYCOUNT := -1; YCOUNT := UNLOADREL([:Y?]);}
        RESULT := RUNFORM([:@ParamName: = "YTitle"]);           RESULT := RUNFORM([:@ParamValue:= ""]);                              DBGYCOUNT := LOADREL([:+DPUB>SP>uspDnBGrapher:K~Operation=:@OPERATION:~:K~UID=:@UID:~:K~ParamName=:@ParamName:~:K~ParamValue=:@ParamValue]);     IF(DBGYCOUNT > -1){YCOUNT := DBGYCOUNT; DBGYCOUNT := -1; YCOUNT := UNLOADREL([:Y?]);}
        RESULT := RUNFORM([:@ParamName: = "TextColor"]);        RESULT := RUNFORM([:@ParamValue:= "#000066"]);                       DBGYCOUNT := LOADREL([:+DPUB>SP>uspDnBGrapher:K~Operation=:@OPERATION:~:K~UID=:@UID:~:K~ParamName=:@ParamName:~:K~ParamValue=:@ParamValue]);     IF(DBGYCOUNT > -1){YCOUNT := DBGYCOUNT; DBGYCOUNT := -1; YCOUNT := UNLOADREL([:Y?]);}
        RESULT := RUNFORM([:@ParamName: = "BackgroundColor"]);  RESULT := RUNFORM([:@ParamValue:= "#eff3f7"]);                       DBGYCOUNT := LOADREL([:+DPUB>SP>uspDnBGrapher:K~Operation=:@OPERATION:~:K~UID=:@UID:~:K~ParamName=:@ParamName:~:K~ParamValue=:@ParamValue]);     IF(DBGYCOUNT > -1){YCOUNT := DBGYCOUNT; DBGYCOUNT := -1; YCOUNT := UNLOADREL([:Y?]);}
        RESULT := RUNFORM([:@ParamName: = "ShowBorder"]);       RESULT := RUNFORM([:@ParamValue:= "False"]);                         DBGYCOUNT := LOADREL([:+DPUB>SP>uspDnBGrapher:K~Operation=:@OPERATION:~:K~UID=:@UID:~:K~ParamName=:@ParamName:~:K~ParamValue=:@ParamValue]);     IF(DBGYCOUNT > -1){YCOUNT := DBGYCOUNT; DBGYCOUNT := -1; YCOUNT := UNLOADREL([:Y?]);}
        RESULT := RUNFORM([:@ParamName: = "ShowLegend"]);       RESULT := RUNFORM([:@ParamValue:= "True"]);                          DBGYCOUNT := LOADREL([:+DPUB>SP>uspDnBGrapher:K~Operation=:@OPERATION:~:K~UID=:@UID:~:K~ParamName=:@ParamName:~:K~ParamValue=:@ParamValue]);     IF(DBGYCOUNT > -1){YCOUNT := DBGYCOUNT; DBGYCOUNT := -1; YCOUNT := UNLOADREL([:Y?]);}
        RESULT := RUNFORM([:@ParamName: = "Margin"]);           RESULT := RUNFORM([:@ParamValue:= "0"]);                             DBGYCOUNT := LOADREL([:+DPUB>SP>uspDnBGrapher:K~Operation=:@OPERATION:~:K~UID=:@UID:~:K~ParamName=:@ParamName:~:K~ParamValue=:@ParamValue]);     IF(DBGYCOUNT > -1){YCOUNT := DBGYCOUNT; DBGYCOUNT := -1; YCOUNT := UNLOADREL([:Y?]);}
        RESULT := RUNFORM([:@ParamName: = "Orientation"]);      RESULT := RUNFORM([:@ParamValue:= "1"]);                             DBGYCOUNT := LOADREL([:+DPUB>SP>uspDnBGrapher:K~Operation=:@OPERATION:~:K~UID=:@UID:~:K~ParamName=:@ParamName:~:K~ParamValue=:@ParamValue]);     IF(DBGYCOUNT > -1){YCOUNT := DBGYCOUNT; DBGYCOUNT := -1; YCOUNT := UNLOADREL([:Y?]);}
        RESULT := RUNFORM([:@ParamName: = "Resolution"]);       RESULT := RUNFORM([:@ParamValue:= "0"]);                             DBGYCOUNT := LOADREL([:+DPUB>SP>uspDnBGrapher:K~Operation=:@OPERATION:~:K~UID=:@UID:~:K~ParamName=:@ParamName:~:K~ParamValue=:@ParamValue]);     IF(DBGYCOUNT > -1){YCOUNT := DBGYCOUNT; DBGYCOUNT := -1; YCOUNT := UNLOADREL([:Y?]);}
        RESULT := RUNFORM([:@ParamName: = "ShowSample"]);       RESULT := RUNFORM([:@ParamValue:= "False"]);                         DBGYCOUNT := LOADREL([:+DPUB>SP>uspDnBGrapher:K~Operation=:@OPERATION:~:K~UID=:@UID:~:K~ParamName=:@ParamName:~:K~ParamValue=:@ParamValue]);     IF(DBGYCOUNT > -1){YCOUNT := DBGYCOUNT; DBGYCOUNT := -1; YCOUNT := UNLOADREL([:Y?]);}

        RESULT := RUNFORM([:@ParamName: = "XAxisValue"]);

        XAXISVALUE_YCOUNT   := -1;
        XAXISVALUE_NUMRECS  := 0;
        IF(TPBRS_DUNS_NUMRECS > 0)
        {
            XAXISVALUE_NUMRECS  := TPBRS_DUNS_NUMRECS;
            XAXISVALUE_YCOUNT   := TPBRS_DUNS_YCOUNT;
        }
        ELSE IF(TPBRS_SIC2_NUMRECS > 0)
        {
            XAXISVALUE_NUMRECS  := TPBRS_SIC2_NUMRECS;
            XAXISVALUE_YCOUNT   := TPBRS_SIC2_YCOUNT;
        }
        ELSE IF(TPBRS_SIC4_NUMRECS > 0)
        {
            XAXISVALUE_NUMRECS  := TPBRS_SIC4_NUMRECS;
            XAXISVALUE_YCOUNT   := TPBRS_SIC4_YCOUNT;
        }

        XAXISVALUE_LCOUNT := 0;
        WHILE(XAXISVALUE_LCOUNT < XAXISVALUE_NUMRECS)
        {
            YCOUNT := XAXISVALUE_YCOUNT; LCOUNT := XAXISVALUE_LCOUNT;
            RESULT := RUNFORM([:@ParamValue:= :+CExp_date_MMM_YY:Y?:L?:FSTRIP]);
            DBGYCOUNT := LOADREL([:+DPUB>SP>uspDnBGrapher:K~Operation=:@OPERATION:~:K~UID=:@UID:~:K~ParamName=:@ParamName:~:K~ParamValue=:@ParamValue]);
            IF(DBGYCOUNT > -1){YCOUNT := DBGYCOUNT; DBGYCOUNT := -1; YCOUNT := UNLOADREL([:Y?]);}

            INC XAXISVALUE_LCOUNT;
        }

        RESULT := RUNFORM([:@ParamName: = "YAxisValue"]);
        TPBRS_PRV_LCOUNT := 0;
        WHILE(TPBRS_PRV_LCOUNT <= TPBRS_PRV_NUMRECS)
        {
            RESULT := RUNFORM([:@ParamValue:= ""]);
             IF(TPBRS_PRV_LCOUNT < TPBRS_PRV_NUMRECS)
            {
                YCOUNT := TPBRS_PRV_YCOUNT; LCOUNT := TPBRS_PRV_LCOUNT;
                //RESULT := RUNFORM([:@ParamValue:= :+CPaying_Record_Value:Y?:L?:FSTRIP]);
                IF      (STRCMPI([:+CPaying_Record_Value:Y?:L?:FSTRIP],"PPT"       )==0){RESULT := RUNFORM([:@ParamValue:= "��ʱ"         ]);}
                ELSE IF (STRCMPI([:+CPaying_Record_Value:Y?:L?:FSTRIP],"PPT/SLW 30")==0){RESULT := RUNFORM([:@ParamValue:= "��ʱ/�ӳ� 30" ]);}
                ELSE IF (STRCMPI([:+CPaying_Record_Value:Y?:L?:FSTRIP],"PPT/SLW 60")==0){RESULT := RUNFORM([:@ParamValue:= "��ʱ/�ӳ� 60" ]);}
                ELSE IF (STRCMPI([:+CPaying_Record_Value:Y?:L?:FSTRIP],"PPT/SLW 90")==0){RESULT := RUNFORM([:@ParamValue:= "��ʱ/�ӳ� 90" ]);}
                ELSE IF (STRCMPI([:+CPaying_Record_Value:Y?:L?:FSTRIP],"SLW 30"    )==0){RESULT := RUNFORM([:@ParamValue:= "�ӳ� 30"        ]);}
                ELSE IF (STRCMPI([:+CPaying_Record_Value:Y?:L?:FSTRIP],"SLW 30/60" )==0){RESULT := RUNFORM([:@ParamValue:= "�ӳ� 30/60"     ]);}
                ELSE IF (STRCMPI([:+CPaying_Record_Value:Y?:L?:FSTRIP],"SLW 30/90" )==0){RESULT := RUNFORM([:@ParamValue:= "�ӳ� 30/90"     ]);}
                ELSE IF (STRCMPI([:+CPaying_Record_Value:Y?:L?:FSTRIP],"SLW 60"    )==0){RESULT := RUNFORM([:@ParamValue:= "�ӳ� 60"        ]);}
                ELSE IF (STRCMPI([:+CPaying_Record_Value:Y?:L?:FSTRIP],"SLW 60/90" )==0){RESULT := RUNFORM([:@ParamValue:= "�ӳ� 60/90"     ]);}
                ELSE IF (STRCMPI([:+CPaying_Record_Value:Y?:L?:FSTRIP],"SLW 90"    )==0){RESULT := RUNFORM([:@ParamValue:= "�ӳ� 90"        ]);}
            }
            DBGYCOUNT := LOADREL([:+DPUB>SP>uspDnBGrapher:K~Operation=:@OPERATION:~:K~UID=:@UID:~:K~ParamName=:@ParamName:~:K~ParamValue=:@ParamValue]);
            IF(DBGYCOUNT > -1){YCOUNT := DBGYCOUNT; DBGYCOUNT := -1; YCOUNT := UNLOADREL([:Y?]);}

            INC TPBRS_PRV_LCOUNT;
        }
        IF(TPBRS_PRV_YCOUNT > -1){YCOUNT := TPBRS_PRV_YCOUNT; TPBRS_PRV_YCOUNT := -1; YCOUNT := UNLOADREL([:Y?]);}

        RESULT := RUNFORM([:@IND_ENAME:L0:  = ""]);
        RESULT := RUNFORM([:@IND_NNAME:L0:  = ""]);
        RESULT := RUNFORM([:@IND_COLOR:L0:  = ""]);
        IF(TPBRS_DUNS_NUMRECS > 0)
        {
            LCOUNT := COMPNAMELC; RESULT := RUNFORM([:@IND_ENAME:L0: = :@FIELD:L?:FSTRIP]);
            LCOUNT := COMPNAME_CHINESELC; RESULT := RUNFORM([:@IND_NNAME:L0: = :@FIELD:L?:FSTRIP]);
            RESULT := RUNFORM([:@IND_COLOR:L0: = "#ffcc00"]);
        }

        RESULT := RUNFORM([:@IND_ENAME:L1:  = ""]);
        RESULT := RUNFORM([:@IND_NNAME:L1:  = ""]);
        RESULT := RUNFORM([:@IND_COLOR:L1:  = ""]);
        IF(STRCMPI([:@SICNDesc:FSTRIP],"N/A")!=0)
        {
            RESULT := RUNFORM([:@IND_ENAME:L1: = :@SICEDesc:FSTRIP]);
            RESULT := RUNFORM([:@IND_NNAME:L1: = :@SICNDesc:FSTRIP]);
            RESULT := RUNFORM([:@IND_COLOR:L1: = "#9999FF"]);
        }

        INDNAME_LCOUNT     := 0;
        WHILE(INDNAME_LCOUNT < 2)
        {
            LCOUNT := INDNAME_LCOUNT;
            IF((EXISTS([:@IND_NNAME:L?:FSTRIP]))+(EXISTS([:@IND_ENAME:L?:FSTRIP]))==2)
            {
                RESULT := RUNFORM([:@ParamName: = "Series/NName"]);
                RESULT := RUNFORM([:@ParamValue:= :@IND_NNAME:L?:FSTRIP]);
                DBGYCOUNT := LOADREL([:+DPUB>SP>uspDnBGrapher:K~Operation=:@OPERATION:~:K~UID=:@UID:~:K~ParamName=:@ParamName:~:K~ParamValue=:@ParamValue]);
                IF(DBGYCOUNT > -1){YCOUNT := DBGYCOUNT; DBGYCOUNT := -1; YCOUNT := UNLOADREL([:Y?]);}

                RESULT := RUNFORM([:@ParamName: = "Series/EName"]);
                RESULT := RUNFORM([:@ParamValue:= :@IND_ENAME:L?:FSTRIP]);
                DBGYCOUNT := LOADREL([:+DPUB>SP>uspDnBGrapher:K~Operation=:@OPERATION:~:K~UID=:@UID:~:K~ParamName=:@ParamName:~:K~ParamValue=:@ParamValue]);
                IF(DBGYCOUNT > -1){YCOUNT := DBGYCOUNT; DBGYCOUNT := -1; YCOUNT := UNLOADREL([:Y?]);}

                RESULT := RUNFORM([:@ParamName: = "Series/Color"]);
                RESULT := RUNFORM([:@ParamValue:= :@IND_COLOR:L?:FSTRIP]);
                DBGYCOUNT := LOADREL([:+DPUB>SP>uspDnBGrapher:K~Operation=:@OPERATION:~:K~UID=:@UID:~:K~ParamName=:@ParamName:~:K~ParamValue=:@ParamValue]);
                IF(DBGYCOUNT > -1){YCOUNT := DBGYCOUNT; DBGYCOUNT := -1; YCOUNT := UNLOADREL([:Y?]);}
            }
            INC INDNAME_LCOUNT;
        }

        RESULT := RUNFORM([:@SDP_NAME:L0:FSTRIP = ""]);
        RESULT := RUNFORM([:@SDP_NAME:L1:FSTRIP = ""]);

        IF(EXISTS([:@IND_ENAME:L0:FSTRIP]))
        {
            RESULT := RUNFORM([:@SDP_NAME:L0:FSTRIP = "Series/EName='" & :@IND_ENAME:L0:FSTRIP & "'/DataPoint"]);
        }
        VCOUNT := TPBRS_DUNS_YCOUNT;  RESULT := RUNFORM([:@SDP_YCOUNT:L0:FSTRIP  = ":#:VAR:V?:FSTRIP"]);
        VCOUNT := TPBRS_DUNS_NUMRECS; RESULT := RUNFORM([:@SDP_NUMRECS:L0:FSTRIP = ":#:VAR:V?:FSTRIP"]);

        IF(EXISTS([:@IND_ENAME:L1:FSTRIP]))
        {
            RESULT := RUNFORM([:@SDP_NAME:L1:FSTRIP = "Series/EName='" & :@IND_ENAME:L1:FSTRIP & "'/DataPoint"]);
        }

        RESULT := RUNFORM([:@SDP_YCOUNT:L1:FSTRIP  = "-1"]);
        RESULT := RUNFORM([:@SDP_NUMRECS:L1:FSTRIP = "0"]);
        IF(MAXSIC4COUNT >= THRESHHOLD)
        {
            VCOUNT := TPBRS_SIC4_YCOUNT;  RESULT := RUNFORM([:@SDP_YCOUNT:L1:FSTRIP  = ":#:VAR:V?:FSTRIP"]);
            VCOUNT := TPBRS_SIC4_NUMRECS; RESULT := RUNFORM([:@SDP_NUMRECS:L1:FSTRIP = ":#:VAR:V?:FSTRIP"]);
        }
        ELSE IF(MAXSIC2COUNT >= THRESHHOLD)
        {
            VCOUNT := TPBRS_SIC2_YCOUNT;  RESULT := RUNFORM([:@SDP_YCOUNT:L1:FSTRIP  = ":#:VAR:V?:FSTRIP"]);
            VCOUNT := TPBRS_SIC2_NUMRECS; RESULT := RUNFORM([:@SDP_NUMRECS:L1:FSTRIP = ":#:VAR:V?:FSTRIP"]);
        }
        SDP_LCOUNT := 0;
        WHILE(SDP_LCOUNT < 2)
        {
            LCOUNT := SDP_LCOUNT;
            RESULT := RUNFORM([:@ParamName: = :@SDP_NAME:L?:FSTRIP]);

            SDPVALUE_LCOUNT  := 0;
            LCOUNT := SDP_LCOUNT; SDPVALUE_NUMRECS := ATOI([:@SDP_NUMRECS:L?:FSTRIP]);
            WHILE(SDPVALUE_LCOUNT < SDPVALUE_NUMRECS)
            {
                LCOUNT := SDP_LCOUNT;
                YCOUNT := ATOI([:@SDP_YCOUNT:L?:FSTRIP]); LCOUNT := SDPVALUE_LCOUNT;
                RESULT := RUNFORM([:@ParamValue:= :+CMean_Pay_Record_Ordinal:Y?:L?:FSTRIP]);
                DBGYCOUNT := LOADREL([:+DPUB>SP>uspDnBGrapher:K~Operation=:@OPERATION:~:K~UID=:@UID:~:K~ParamName=:@ParamName:~:K~ParamValue=:@ParamValue]);
                IF(DBGYCOUNT > -1){YCOUNT := DBGYCOUNT; DBGYCOUNT := -1; YCOUNT := UNLOADREL([:Y?]);}

                INC SDPVALUE_LCOUNT;
            }
            INC SDP_LCOUNT;
        }

        RESULT := RUNFORM([:@OPERATION: = "RUNGRAPHER"]);
        RESULT := RUNFORM([:@ParamName: = ""]);
        RESULT := RUNFORM([:@ParamValue:= ""]);
        DBGYCOUNT := LOADREL([:+DPUB>SP>uspDnBGrapher:K~Operation=:@OPERATION:~:K~UID=:@UID:~:K~ParamName=:@ParamName:~:K~ParamValue=:@ParamValue]);

        CONTINUE := 0;
        ERROR    := 0;
        YCOUNT := DBGYCOUNT; DBGNUMRECS := COUNTREL([:Y?]);
        IF(DBGNUMRECS > 0)
        {
            YCOUNT := DBGYCOUNT; LCOUNT := 0;
            CONTINUE := ((ATOI([:+CRetCode:Y?:L?])==0) + (EXISTS([:+CURL:Y?:L?])==1)==2);
            ERROR    := ((ATOI([:+CRetCode:Y?:L?])!=0) + (EXISTS([:+CRetMessage:Y?:L?])==1)==2);
        }
    }
    IF((ERROR)+(CONTINUE))
    {
        YCOUNT := DBGYCOUNT; LCOUNT := 0;
        `
        <!--<DIV class="pagebreakhere"></DIV>-->
        <H3 id="TradePaymentIndustryComparison" class="h3border">����������Ϊ(12 ����)</H3>
        <TABLE class="graphTable" cellSpacing="0" cellPadding="0" width="560" border="0">
        <TR>
            <TD vAlign="bottom">`

            IF(ERROR)   {`<P style='font-size:8px;color:#ff0000'>[:+CRetMessage:Y?:L?]<P> ` }
            IF(CONTINUE){`<IMG src="[:+CURL:Y?:L?]" alt="[:+CURL:Y?:L?]>">`                 }

        `
            </TD>
        </TR>
        </TABLE>
        `
    }
    IF(DBGYCOUNT > -1){YCOUNT := DBGYCOUNT; DBGYCOUNT := -1; YCOUNT := UNLOADREL([:Y?]);}
}

IF(DBGYCOUNT        > -1)   { YCOUNT := DBGYCOUNT; DBGYCOUNT := -1; YCOUNT := UNLOADREL([:Y?]);}
IF(TPBRS_PRV_YCOUNT > -1)   { YCOUNT := TPBRS_PRV_YCOUNT;  TPBRS_PRV_YCOUNT  := -1; YCOUNT := UNLOADREL([:Y?]); }
IF(TPBRS_DUNS_YCOUNT> -1)   { YCOUNT := TPBRS_DUNS_YCOUNT; TPBRS_DUNS_YCOUNT := -1; YCOUNT := UNLOADREL([:Y?]); }
IF(TPBRS_SIC2_YCOUNT> -1)   { YCOUNT := TPBRS_SIC2_YCOUNT; TPBRS_SIC2_YCOUNT := -1; YCOUNT := UNLOADREL([:Y?]); }
IF(TPBRS_SIC4_YCOUNT> -1)   { YCOUNT := TPBRS_SIC4_YCOUNT; TPBRS_SIC4_YCOUNT := -1; YCOUNT := UNLOADREL([:Y?]); }

