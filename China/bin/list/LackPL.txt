9        6      80  3   CHI ENG JAP
0        CH     处于筹备阶段
0        EN     Under preparation
0        JA     未だ操業していない
1        CH     刚投产
1        EN     started production
1        JA     稼動したばかり
2        CH     未获得
2        EN     N/A
2        JA     を入手することが出来ませんでした
3        CH     无原因
3        EN     No Reason
3        JA     原因不明
4        CH     未归档
4        EN     Not filed
4        JA     登録されていません
5        CH     数据模糊
5        EN     Data fuzzy
5        JA     データがはっきりしていなくて
6        CH     无原因&拒绝
6        EN     No Reason & Decline
6        JA     原因不明、拒否された