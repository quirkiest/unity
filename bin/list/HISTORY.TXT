9 6 46 1 ENG
name  1 History
Scr1  1 1/21   Incorporation
ac    1 Update Date
ka    1 Start year
lt    1	Legal Type
ir    1	Investor Type
ha    1 Type of Business
ng    1	Non-governmental Organization
tb    1 Input type of business(ENG)
TB    1 Input type of business(CHI)
ke    1 Registration Date
oo    1	OOB Date
aa    1	Activity Area(eng)
AA    1	Activity Area(chi)
wb    1 Which Bureau
WB    1 Which Bureau(chi)
bl    1	Bureau list
c1    1 Business Registration No.
C1    1 Business Registration No (CHI).
lv    1 License Expiry Date
le    1 License Expiry Date List
nf    1 No Fixed Expiry
nx    1 No Expiry detail
cx    1 ISFMD Serial No.
ts    1 NOC
tx    1 Tax Number
ty    1 Local tax Number
tz    1 State tax Number
jo    1 Control (year)
jj    1 Founder
JJ    1 Founder (chi)
lr    1 Legal Representative (English)
LR    1 Legal Representative (Chinese)
c2    1 Principal?
ji    1 History condition
dd    1 Business Scope (English)
DD    1 Business Scope (Chinese)
ya    1 Management Deadline (From)

jf    1 Listed
jm    1 Effective Date
li    1 List Code
in    1 Foreign Invested Company
co    1 Country

ba    1	Registration Status
bb    1	Registration Status Comments(eng)
BB    1	Registration Status Comments(chi)
bc    1	Management Status
bd    1	Management Status Comments(eng)
BD    1	Management Status Comments(chi)
be    1	United Social Credit Code Certificate No.


Scr2  1 2/21   Capital Investment
d7    1 Initial Capital Investment
e7    1 Registered Capital
f7    1 Total Investment
cr    1 Currency
c3    1 Euro?
jg    1 No. of Shares (Ordinary)
jh    1 Par Value (Ordinary)
jk    1 No. of Shares (Preference)
jl    1 Per Value (Preference)
d5    1 Paid Up Capital
br    1 Br.Op.Capital
jx    1 No. of Shares (Ordinary)
jy    1 No. of Shares (Preference)
je    1 As Of Date
io    1 Investment of Equipment
pr    1 Processing Service Charge


Scr3  1 3/21   Shareholders
fc    1 Name
FC    1 Name (chi)
is    1 Individual Shareholder
lt    1 Literal Translation
sc    1 Country

o1    1 No. of Ordinary Shares
fl    1 Percent
p1    1 No. of Preference Shares
p2    1 Percent
im    1 Investment Mode
p3    1 Paid-up Rate (Percent)

ys    1 Approval Payment Caption
ot    1 Total Ordinary Shares
ft    1 Total Ordinary Percent
c4    1 Total Subscription
pt    1 Total Preference Shares
ps    1 Total Preference Percent
fm    1 No other shareholder ?
ja    1 Controlling Interest held by
JA    1 Controlling Interest held by (chi)
fv    1 Total Number of Shareholders
os    1 Others holding less than 10% of equity
jc    1 Shareholder Information Unknown?
jn    1 Reason of Unknown
jp    1 Business Size
su    1 Shareholder updated on
jq    1 Business Type 1
jr    1 Business Type 2
js    1 Business Type 3
yb    1 Import & Export Power
yc    1 Authorized Time
yd    1 Checkup Organization (English)
YD    1 Checkup Organization (Chinese)
ye    1 Import & Export Permit No.

si    1 Shareholders Incomplete
s1    1 Comment1 (chi)
s2    1 Comment2 (chi)
s3    1 Comment3 (chi)
s4    1 Comment4 (chi)
j1    1 Comment (eng)
J1    1 Comment (chi)
j2    1 Comment (eng)
J2    1 Comment (chi)
j3    1 Comment (eng)
J3    1 Comment (chi)
j4    1 Comment (eng)
J4    1 Comment (chi)
cl    1 Comment List    
ma    1 Date of Search
mb    1 Orgnazation
c4    1 Total Subscription

Scr4  1 4/21   Name Changes
ca    1 From
CA    1 From (chi)
cb    1 Date
ce    1 To
CE    1 To (chi)
c5    1 name add

Scr5  1 5/21   Address Changes
kj    1 From Address - line 1
ko    1 - Address line 2
KJ    1 Chinese- Address line 1
KO    1 - Address line 2
kk    1 Print on Header?
kl    1 Date
km    1 To Address - line 1
kp    1 - Address line 2
KM    1 Chinese- Address line 1
KP    1 - Address line 2
kn    1 Print on Header?
c6    1 address add

Scr6  1 6/21   Registered Address Changes
yj    1 From Address - line 1
yo    1 - Address line 2
YJ    1 Chinese- Address line 1
YO    1 - Address line 2
yl    1 Date
ym    1 To Address - line 1
yp    1 - Address line 2
YM    1 Chinese- Address line 1
YP    1 - Address line 2
yn    1 Data Unknown
c7    1 Registered address add

Scr7  1 7/21   Key Events
ib    1 Date
ia    1 Event
jb    1 Event
IA    1 Event (chi)
IB    1 Event (chi)
IC    1 Event (chi)
ID    1 Event (chi)
IE    1 Event (chi)
bf    1	Listed in abnormal operation directory or not
bg    1	Listed Date
bh    1	Abnormal operation listed reason(eng)
BH    1	Abnormal operation listed reason(chi)
bi    1	Abnormal directory removed
bj    1	Removed Date
bk    1	Abnormal operation removed reason(eng)
BK    1	Abnormal operation removed reason(chi)
bm    1	Comments (eng)
BM    1	Comments (chi)

bn    1	Listed in Serious Illegal Enterprises or not
bo    1	Listed Date of Serious Illegal Enterprises
bp    1	SIE Listed Reason(eng)
BP    1	SIE Listed Reason(chi)
bq    1	Serious Illegal Enterprises Removed
bu    1	Removed Date of Serious Illegal Enterprises
bs    1	SIE Removed Reason(eng)
BS    1	SIE Removed Reason(chi)
bt    1	Serious Illegal Enterprises Comments(eng)
BT    1	Serious Illegal Enterprises Comments(chi)

da    1 Date
db    1 History Summary (English)
DB    1 History Summary (Chinese)
jd    1 Company Overview (eng)
JD    1 Company Overview (chi)
uo    1	On-site Audit Records(eng)
UO    1	On-site Audit Records(chi)
cf    1	Performing Obligation Start Time
cg    1	Performing Obligation Deadline
ch    1	Chattel Mortgage Registration No.(eng)
CH    1	Chattel Mortgage Registration No.(chi)
ci    1	Chattel Mortgage Registration Authority(en)
CI    1	Chattel Mortgage Registration Authority(cn)
cj    1	Amount of Secured Creditor's Rights(eng)
CJ    1	Amount of Secured Creditor's Rights(chi)
ck    1	Chattel Mortgage Type(eng)
CK    1	Chattel Mortgage Type(chi)
cm    1	Guarantee Scope(eng)
CM    1	Guarantee Scope(chi)
cp    1	Collateral Name(eng)
CP    1	Collateral Name(chi)
cq    1	Chattel Mortgage Registration Status
cs    1	Chattel Mortgage Comments(eng)
CS    1	Chattel Mortgage Comments(chi)


Scr8  1 8/21   Shareholder Changes
oh    1 From
OH    1 From (chi)
sd    1 Date
oi    1 To
OI    1 To (chi)
c8    1 Shareholder add

Scr9  1 9/21   Capital Changes
ou    1 Currency
oc    1 From
cd    1 Date
ov    1 Currency
od    1 To
yg    1 Examines The Capital The Unit (English)	
YG    1	Examines The Capital The Unit (Chinese)	
c9    1 Euro?
qa    1 Euro?

Scr10 1 10/21   Legal Status Changes
ol    1 From
ld    1 Date
om    1 To

Scr11 1 11/21   Previous Total Investment
oy    1 Currency
oa    1 From
hd    1 Date
oz    1 Currency
ob    1 To
qb    1 Euro?
qc    1 Euro?

Scr12 1 12/21   Registration Agency Changes
qi    1 From
QI    1 From (chi)
qo    1 Date
qk    1 To
QK    1 To (chi)
ql    1 Registration Agency add

Scr13 1 13/21   Previous Legal Person
pa    1 From
PA    1 From (chi)
pb    1 Date
pe    1 To
PE    1 To (chi)
t1    1 Legal Person add

Scr14 1 14/21   Principal change
ph    1 From
PH    1 From (chi)
pi    1 Date
pl    1 To
PL    1 To (chi)
cz    1 Principal add

Scr15 1 15/21   Previous General Manager
ga    1 From
GA    1 From (chi)
gb    1 Date
ge    1 To
GE    1 To (chi)
t2    1 General Manager add

Scr16 1 16/21   Previous Chairman
ra    1 From
RA    1 From (chi)
rb    1 Date
re    1 To
RE    1 To (chi)
t3    1 Chairman add

Scr17 1 17/21   Previous Registration Number
na    1 From
NA    1 From (chi)
nb    1 Date
ne    1 To
NE    1 To (chi)
ta    1 Registration Number add

Scr18 1 18/21   Previous Investment of Equipement
eu    1 Currency
ec    1 From
ed    1 Date
ev    1 Currency
ep    1 To

Scr19 1 19/21   Previous Processing Service Charge
pu    1 Currency
pc    1 From
pd    1 Date
pv    1 Currency
pp    1 To


Scr20 1 20/21   Supervisor Changes
ua    1	From(eng)
UA    1	From(chi)
ub    1	Date
uc    1	Date Unknown
ud    1	To(eng)
UD    1	To(chi)
ue    1	Supervisor add
uf    1	Non Details

Scr21 1 21/21   Branches Changes
ug    1	From(eng)
UG    1	From(chi)
uh    1	Date
ui    1	Date Unknown
uj    1	To(eng)
UJ    1	To(chi)
uk    1	Branches add
um    1	Non Details


z1    1 1/21   Incorporation
z2    1 2/21   Capital Investment
z3    1 3/21   Shareholders
z4    1 4/21   Name Changes
z5    1 5/21   Address Changes
z6    1 6/21   Registered Address Changes
z7    1 7/21   Key Events
z8    1 8/21   Shareholder Changes
z9    1 9/21   Capital Changes
za    1 10/21   Legal Status Changes
zb    1 11/21   Previous Total Investment
zc    1 12/21   AIC Change
zd    1 13/21   Previous Legal Person
ze    1 14/21   Previous Principal
zf    1 15/21   Previous General Manager
zg    1 16/21   Previous Chairman
zh    1 17/21   Previous Registration Number
zj    1 18/21   Previous Investment of Equipement
zk    1 19/21   Previous Processing Service Charge
zi    1 20/21   Supervisor Changes
zl    1 21/21   Branches Changes

ro         1 Rounded?
no         1 Non Reg Cap
sv         1 Supervisor(Chi)
sg         1 Supervisor(Eng)
rs	   1 Supervisor DUNS
pg         1 Predecessor(Eng)
po         1 Predecessor(Chi)
he	   1 Headquarter(Eng)	
HE	   1 Headquarter(Chi)	
dp	   1 Display
ul         1 Unable to Obtain
ai         1 Annual Inspection?
ay         1 Annual Report Year
nd         1 Annual Report Published Date
fq         1 Shd Comments
du         1 Date Unknown
dn         1 Date Unknown
as         1 Non Details
ky         1 Key Events
dk         1 Date Unknown
sh         1 Non Details
do         1 Date Unknown
rc         1 Non Details
dw         1 Date Unknown
lc         1 Non Details
dt         1 Date Unknown
ic         1 Non Details
de         1 Date Unknown
lp         1 Non Details
dc         1 Date Unknown
gm         1 Non Details
df         1 Date Unknown
cn         1 Non Details
dg         1 Date Unknown
rg         1 Non Details
di         1 Date Unknown
ie         1 Non Details
dh         1 Date Unknown
cc         1 No Details
yu         1 No Details
hn         1 Hotlist Name
hp         1 Hotlist property
qu         1 Date Unknown
pk         1 Date Unknown
lo         1 Non Details
ry         1 No Details
g7	   1 Euro Registered Capital
g8	   1 Euro Total Investment
nn	   1 Certificate Type
nm	   1 No.
tc	   1 Type of business comments
un         1 Paid-up Rate Comments