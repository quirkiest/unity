6       6       35   2   ENG  CHI
2       EN      Retailers
2       CH      零售商
8       EN      Manufacturers
8       CH      制造商
11      EN      Wholesalers
11      CH      批发商
12      EN      Individuals
12      CH      个人
13      EN      Parent Company
13      CH      母公司
14      EN      Affiliate(s)
14      CH      关系企业
16      EN      Subsidiary
16      CH      子公司
18      EN      Government
18      CH      政府及相关机构
r1      EN      Departmental Stores
r1      CH      百货业者
r2      EN      Buying Agents
r2      CH      采购商
j1      EN      Trading Firms
j1      CH      贸易商
j2      EN      Others
j2      CH      其他
j3      EN      Local Foreign Capitalized Co.
j3      CH      零售商
j4      EN      Hotels and/or Restaurants
j4      CH      旅馆餐饮业者
j5      EN      Private sector
j5      CH      私营承包商
j6      EN      Designers
j6      CH      设计师
J7      EN      Construction Companies
J7      CH      建设公司
J8      EN      Hospitals
J8      CH      医院
J9      EN      Supermarkets
J9      CH      超级市场
J10     EN      Corporations
J10     CH      公司
J11     EN      Importers
J11     CH      进口商
J12     EN      Chain Stores
J12     CH      连锁商店
J13     EN      Financial Organizations
J13     CH      金融机构
J14     EN      Related Companies
J14     CH      相关公司
J15     EN      Research Institutes
J15     CH      研究机构
J16     EN      Industrial Sector
J16     CH      工业区
J17     EN      Commercial Sector
J17     CH      商业区
J18     EN      Airlines
J18     CH      航空公司
J19     EN      Banks
J19     CH      银行
J20     EN      Corporations
J20     CH
J21     EN      Securities companies
J21     CH      证券公司
J22     EN      Marine sector
J22     CH      海军区
J23     EN      Original Equipment Manufacturers
J23     CH      OEM 代工厂
J24     EN      Electronics Manufacturers
J24     CH      电子工厂
J25     EN      Distributors
J25     CH      经销商
J26     EN      Drugstores
J26     CH      药店
J27     EN      Pharmacies
J27     CH      药房
J28     EN      Clinics
J28     CH      诊所
J29     EN      Government employees
J29     CH      政府官员
J30     EN      Military veterans
J30     CH      军人
J31     EN      Laboratories
J31     CH      科学实验室
J32     EN      Restaurants
J32     CH      餐厅
J33     EN      Advertising companies
J33     CH      广告公司
J34     EN      Universities
J34     CH      大学
J35     EN      Motor dealers
J35     CH      汽车零售商
J36     EN      Schools
J36     CH      学校
J37     EN      Publishers
J37     CH      出版商
J38     EN      Beauty salons
J38     CH      美容院
J39     EN      Government & Military
J39     CH      政府及军方
J40     EN      Air Force
J40     CH      空军
J41     EN      Army
J41     CH      军队
J42     EN      Building contractors
J42     CH      营建商
J43     EN      Academic Institutions
J43     CH
J44     EN      Aerospace Industry
J44     CH
J45     EN      Wholesalers & Retailers
J45     CH      批发商及代理商
J46     EN      Bookstores
J46     CH      书店
J47     EN      Hair Salons
J47     CH      发廊
J48     EN      Financial Institutions
J48     CH
J49     EN      Warehousing Stores
J49     CH      量贩店
J50     EN      Directorate General of Telecommunications
J50     CH      电信局
J51     EN      Garages
J51     CH      汽车修护厂
J52     EN      Farms
J52     CH      农场
J53     EN      Company
J53     CH      一般公司
J54     EN      Constructors
J54     CH      建筑商
J55     EN      Hog Farm
J55     CH      养猪场
J56     EN      Waterworks
J56     CH      自来水厂
J57     EN      Shipping Companies
J57     CH      船运公司
J58     EN      Shipowner
J58     CH      船主
J59     EN      Construction companies
J59     CH      建设公司
