//*********************************************************************************
//  sbeARskI.cap - SuperBIR Eng. HTML Appendix - Risk Index
//*********************************************************************************
DBSECTION "general";
LANGUAGE "ENG";
MULTIBYTE;
FLOAT_FORMAT "1,000";
LINE_LENGTH 1200;
NOTFILL;
DECLARE RESULT;
DECLARE NUMSECTIONS;
DECLARE NUMRECS;
DECLARE NUMLOOPS;

MULTIBYTE;

LEFT_MARGIN 9;

IF(EXISTS([:ac:B*]))
{
    // Appendix
    // -----------

    // Risk Index
    // -------------
    `
    <A id="AppendixRiskIndex"></A>

    <TABLE width="100%" cellSpacing="0" cellPadding="0" border="0">
    <TR>
        <TD width="100%" align="left"><H3 class="h3border">付録</H3></TD>
        <TD class="dbh3Align">
            <DIV class="NOPRINT">
                <A class="h2nav" href="javascript:doExpand('AppRiskIndex','[:@WEBSERVER]');">
                    <IMG id="butAppRiskIndex" onclick="" height="11" alt="Summarise" src="[:@WEBSERVER]images/negative-single-box-11px.gif" width="11" border="0">
                </A>
            </DIV>
        </TD>
    </TR>
    </TABLE>

    <DIV id="AppRiskIndex" style="DISPLAY: block">

        <P>
	D&B EMMAスコア（D&B EMMA Score）はD&Bが新興市場の国に向けて開発したリスク評価システムです。D&B EMMA
	スコアはデータプロファイリングおよび統計分析に基づいて開発された、企業が、例えば財務状態がよくない、遅延の返済、
	在庫の長期化、正常な事業運営が困難など不安定または信用できないリスク状態になる可能性を予測するものです。EMMA
	スコアは、対象企業の信用リスクを示したものとして、その他の情報と併用することでより最適なリスク判定を行うことができます。
	</P>

        <P>EMMAスコアは1から10までであり、1がより低い企業信用リスクを表し、10がより高い企業信用リスクを表しています。</P>

	<P>
	中国EMMAスコアは2009年5月に初めて起用された約400万の企業データの指標開発に基づくものです。個々の企業の信用
	判断に同スコアを使用する場合、HDBCは EMMAスコアをお客様の実際の取引状況に合せて分析、カスタマイズされた、よ
	り精度の高いリスクインデックス基準を確立することをお勧めします。比較的高いEMMAスコアが付与されている企業と取引を行う場合でも、必ずしも同リスクスコアに相当する信用上のリスクを負うとは限りません。
	</P>

        <P>
	中国のビジネス環境の特殊性に応じて、EMMAスコアを中国企業のリスク要素を適切に分析し、示すために、HDBCは2012
	年上半期に、EMMAスコアを最適化しました。支払指数を重要なリスク評価ディメンションとして評価モデルに組み入れて、
	対象企業のリスクの内在要素をより深く示しています。最適化後のEMMAスコア評点は現在の中国の新興市場としての特徴をより科学的に示し、中国企業のリスクレベルをより客観的により深く示すことができます。
	</P>

	<P>中国EMMAスコアは、マクロ経済の変動及び特定出来事による中国商業環境変化の要因をタイムリーに精確に把握するために、D&Bワールドモデル開発方法論の枠組において定例チェックを行われています。</P>

        <H5>EMMAスコアの説明</H5>

        <TABLE cellspacing="0" cellpadding="2" width="630" bgcolor="#eff3f7" border="0">
        <TR>
            <TD class="dbRowB" width="120" bgcolor="#c8d7f0"><B>リスク区間</B></TD>
            <TD class="dbRowB" width="90" bgcolor="#c8d7f0"><B>EMMAスコア</B></TD>
            <TD class="dbRowB" align="center" width="120" bgcolor="#c8d7f0"><B>HDBCデータベースに占める割合</B></TD>
            <TD class="dbRowB" align="left" width="4" bgcolor="#c8d7f0">&nbsp;</TD>
            <TD class="dbRowB" width="100" bgcolor="#c8d7f0" align="center"><B>不良ケースの可能性</B></TD>
            <TD class="dbRowB" align="left" width="4" bgcolor="#c8d7f0">&nbsp;</TD>
            <TD class="dbRowB" width="160" bgcolor="#c8d7f0"><B>EMMAスコアの定義</B></TD>
            <TD class="dbRowB" align="left" width="4" bgcolor="#c8d7f0">&nbsp;</TD>
        </TR>
        <TR>
            <TD class="dbRowCresults">低位のEMMAスコア</TD>
            <TD class="dbRowClabel" align="center" width="60">1-3</TD>
            <TD class="dbRowClabel" align="center">28.9%</TD>
            <TD class="dbRowCresults" valign="top" width="4">&nbsp;</TD>
            <TD class="dbRowClabel" align="center">5.7%</TD>
            <TD class="dbRowCresults" valign="top" width="4">&nbsp;</TD>
            <TD class="dbRowCresults" align="left">低リスク</TD>
            <TD class="dbRowCresults" valign="top" width="4">&nbsp;</TD>
        </TR>
       
        <TR>
            <TD class="dbRowCresults">中位のEMMAスコア</TD>
            <TD class="dbRowClabel" align="center" width="60">4-7</TD>
            <TD class="dbRowClabel" align="center">56.7%</TD>
            <TD class="dbRowCresults" valign="top" width="4">&nbsp;</TD>
            <TD class="dbRowClabel" align="center">9.1%</TD>
            <TD class="dbRowCresults" valign="top" width="4">&nbsp;</TD>
            <TD class="dbRowCresults" align="left">平均リスク</TD>
            <TD class="dbRowCresults" valign="top" width="4">&nbsp;</TD>
        </TR>
            
        <TR>
            <TD class="dbRowCresults">高位のEMMAスコア</TD>
            <TD class="dbRowClabel" align="center" width="60">8-10</TD>
            <TD class="dbRowClabel" align="center">14.4%</TD>
            <TD class="dbRowCresults" valign="top" width="4">&nbsp;</TD>
            <TD class="dbRowClabel" align="center">36.4%</TD>
            <TD class="dbRowCresults" valign="top" width="4">&nbsp;</TD>
            <TD class="dbRowCresults" align="left">高リスク</TD>
            <TD class="dbRowCresults" valign="top" width="4">&nbsp;</TD>
        </TR>        
       
        </TABLE>
        
       
        <p><br></p>
        <P>EMMAスコア算出の基本項目：</P>

        <TABLE cellspacing="0" cellpadding="0" width="630" bgcolor="#eff3f7" border="0">
        <TR>
            <TD valign="top">
                <P><B>企業情報</B></P>
                <UL>
                    <LI>企業形態
                    <LI>業種 
                    <LI>従業員数
                    <LI>取引地域
                    <LI>登録資本金 
                    <LI>事業年数
                    <LI>社歴
                    <LI>資産所有権
                </UL>                     
            </TD>
            <TD valign="top">        
                <P><B>地理的条件</B></P>
                <UL>
                    <LI>所属地域や省 
                </UL>
		 <P><B>公的情報</B></P>
                <UL>
                    <LI>不利記録数
                    <BR>
                </UL>       
                <P><B>支払情報</B></P>
                <UL>
                    <LI>支払遅延金額比率
                    <LI>企業/業界PAYDEX指数
                </UL>
               </TD>
        </TR>
        </TABLE>
        <H5>EMMAスコアの応用</H5>
        <UL>
            <LI><I>低位のEMMAスコア</I>
                    -   最低限の検証後、または特に検証なしに取引を開始してよいです。
            <LI><I>中位のEMMAスコア</I>
                    -  取引先の再検証を推奨します。または、取引先の支払能力、もしくは自社の信用供与方針およびリスク許容度に応じて、信用供与の条件を調整することを推奨します。
            <LI><I>高位のEMMAスコア</I>
                    -  取引先の支払能力、もしくは自社の信用供与方針およびリスク許容度に基づき、取引拒否とするか、
		    または前払い現金による取引なら承認するかについて、徹底的な自社検証を行う必要があります。
        </UL>
       

					
					<A id="AppendixRiskIndex"></A>

    <TABLE width="630" cellSpacing="0" cellPadding="0" border="0">
    <TR>
        <TD width="619" align="left"><H3 class="h3border">リスク総合評価マトリックスのデータベースサンプルデータ分析</H3></TD>
        <TD class="dbh3Align">
            <DIV class="NOPRINT">
                <A class="h2nav" href="javascript:doExpand('AppRiskIndex','[:@WEBSERVER]');">
                    <IMG id="butAppRiskIndex" onclick="" height="11" alt="Summarise" src="[:@WEBSERVER]images/negative-single-box-11px.gif" width="11" border="0">
                </A>
            </DIV>
        </TD>
    </TR>
    </TABLE>


</br>
<h5 class="tableHead" id="RAnCompanyRiskIndex1">リスクマトリックスサンプルの分布</h5>


					<style type="text/css">
		.tab_1{border-left:1px solid #000;border-top:1px solid #000;line-height:140%;}
    	.tab_1 td{border-bottom:1px solid #000;border-right:1px solid #000;}
    </style>
	<table cellpadding="0" cellspacing="0" border="0"  class="tab_1" width="630">
    	<tr>
        	<td rowspan="2" bgcolor="#BFBFBF" width="180" align=center><B>支払指数</B></td>
            <td colspan="3" bgcolor="#BFBFBF" width="450" align=center><B>リスク</B></td>
        </tr>
        <tr>
            <td bgcolor="#339966" align=center width="150">低リスク</td>
            <td bgcolor="#E5D71B" align=center width="150">中等リスク</td>
            <td bgcolor="#FF5D37" align=center width="150">高リスク</td>
        </tr>
        <tr>
        	<td bgcolor="#339966" width="180">PAYDEX>=80</td>
            <td bgcolor="#339966" align=center width="150">13.6%</td>
            <td bgcolor="#E5D71B" align=center width="150">12.6%</td>
            <td bgcolor="#FF9900" align=center width="150">0.5%</td>
        </tr>
        <tr>
        	<td bgcolor="#E5D71B" width="180">80>PAYDEX>=60</td>
            <td bgcolor="#339966" align=center width="150">15.2%</td>
            <td bgcolor="#E5D71B" align=center width="150">28.7%</td>
            <td bgcolor="#FF9900" align=center width="150">1.7%</td>
        </tr>
        <tr>
        	<td bgcolor="#FF5D37" width="180">PAYDEX<60</td>
            <td bgcolor="#E5D71B" align=center width="150">0.1%</td>
            <td bgcolor="#FF9900"" align=center width="150">15.4%</td>
            <td bgcolor="#FF5D37" align=center width="150">12.2%</td>
        </tr>
    </table>

    </br>
<h5 class="tableHead" id="RAnCompanyRiskIndex1">不良ケースの可能性</h5>


					
	<table cellpadding="0" cellspacing="0" border="0"  class="tab_1" width="630">
    	<tr>
        	<td rowspan="2" bgcolor="#BFBFBF" width="180" align=center><B>支払指数</B></td>
            <td colspan="3" bgcolor="#BFBFBF" width="450" align=center><B>リスク</B></td>
        </tr>
        <tr>
            <td bgcolor="#339966" align=center width="150">低リスク</td>
            <td bgcolor="#E5D71B" align=center width="150">中等リスク</td>
            <td bgcolor="#FF5D37" align=center width="150">高リスク</td>
        </tr>
        <tr>
        	<td bgcolor="#339966" width="180">PAYDEX>=80</td>
            <td bgcolor="#339966" align=center width="150">5.8%</td>
            <td bgcolor="#E5D71B" align=center width="150">8.4%</td>
            <td bgcolor="#FF9900" align=center width="150">9.1%</td>
        </tr>
        <tr>
        	<td bgcolor="#E5D71B" width="180">80>PAYDEX>=60</td>
            <td bgcolor="#339966" align=center width="150">5.6%</td>
            <td bgcolor="#E5D71B" align=center width="150">8.4%</td>
            <td bgcolor="#FF9900" align=center width="150">14.5%</td>
        </tr>
        <tr>
        	<td bgcolor="#FF5D37" width="180">PAYDEX<60</td>
            <td bgcolor="#E5D71B" align=center width="150">5.6%</td>
            <td bgcolor="#FF9900"" align=center width="150">11.1%</td>
            <td bgcolor="#FF5D37" align=center width="150">40.4%</td>
        </tr>
    </table>





</br>

<h5 class="tableHead" id="RAnCompanyRiskIndex1">
					リスク総合評価マトリックス説明</h5>
					<table cellpadding="0" cellspacing="0" border="0"  class="tab_1" width="630">
    	<tr>
        	<td bgcolor="#BFBFBF" width="100" align=center><B>企業群</B></td>
            <td bgcolor="#BFBFBF" width="100" align=center><B>評価分類</B></td>
            <td bgcolor="#BFBFBF" width="240" align=center><B>説明</B></td>
	    <td bgcolor="#BFBFBF" width="185" align=center><B>信用政策</B></td>
        </tr>
        <tr>
            <td bgcolor="#339966" align=center width="100">品質が優良な企業群</td>
            <td bgcolor="#339966" align=center width="100">AA->AB</td>
            <td style="text-align: left;" width="240">事業経営は健全であり、支払は迅速で信用は高い。</td>
	    <td style="text-align: left;" width="185">自動承認により有利な与信方針を提供することが可能である。</td>
        </tr>
	<tr>
            <td bgcolor="#E5D71B" align=center width="100">リスクが比較的低い企業群</td>
            <td bgcolor="#E5D71B" align=center width="100">BA->BB->AC</td>
            <td style="text-align: left;" width="240">事業経営におけるリスクは低い。一部支払遅延が存在する</td>
	    <td style="text-align: left;" width="185">適切な与信方針を推奨する。ただし一部手動審査が必要。</td>
        </tr>
	<tr>
            <td bgcolor="#FF9900" align=center width="100">リスクが比較的高い企業群</td>
            <td bgcolor="#FF9900" align=center width="100">CA->BC->CB</td>
            <td style="text-align: left;" width="240">事業経営におけるリスクは高い。一部深刻な支払遅延が存在する。</td>
	    <td style="text-align: left;" width="185">さらに手動審査を用いた比較的厳格な与信方針を推奨。</td>
        </tr>
	<tr>
            <td bgcolor="#FF5D37" align=center width="100">モニタリング対象となる企業群</td>
            <td bgcolor="#FF5D37" align=center width="100">CC</td>
            <td style="text-align: left;" width="240">事業経営におけるリスク或いは倒産リスクは高い。期日内の支払は不可能である。</td>
	    <td style="text-align: left;" width="185">信用取引を回避し、現金決済を推奨。</td>
        </tr>

       
    </table>			
			<p>*評価分類は<span lang="zh-cn">小</span>さなリスクから大きなリスクまでの順序で排列されています。</p>
	<p>リスク総合評価マトリックスはEMMAスコアと支払指数とを結合して対象企業を深く分析するツールです。EMMAスコアによ
	り、対象企業のリスクの大きさを評価できます。一方、支払指数を通じて、対象企業の支払行為により、対象企業のリスクをさらに分析し、対象企業のリスク
	発生の内在要素を把握することができます。まとめてみれば、リスク総合評価マトリックスはEMMAスコアが近似している対象企業をさらに深く分析することができます。</p>
					<p>
EMMAスコア及びリスク総合評価マトリックスをご利用の際、社内の審査規定、D&Bの提供するその他の情報、リスク検討の
対象と直接に接触したもののフィードバックなど、その他の情報も参考にすることを推奨します。</p>


        
    </DIV>
    `
}

